package homeworks.homework11;

import java.util.List;
import java.util.stream.Collectors;

/*5.	На вход подается список непустых строк. Необходимо привести все символы строк к верхнему регистру и вывести их,
        разделяя запятой.
        Например, для List.of("abc", "def", "qqq") результат будет ABC, DEF, QQQ.*/
public class Task5 {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "def", "qqq");
        String result = list.stream().map(String::toUpperCase).collect(Collectors.joining(","));
        System.out.println(result);

        //то же самое без StreamAPI
        for (String s :
                list) {
            if (list.indexOf(s) < list.size() - 1) {
                System.out.print(s.toUpperCase() + ",");
            } else {
                System.out.print(s.toUpperCase());
            }
        }
    }
}
