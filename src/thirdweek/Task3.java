package thirdweek;

import java.util.*;

//Даны числа m < 13, n < 7. Вывести все степени чесла m от 0 до n включительно с помощью цикла.
public class Task3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите число m");
        int m = scan.nextInt();
        System.out.println("Введите число n");
        int n = scan.nextInt();

        for (int i = 0; i <= n; i++) {
            System.out.println((int) Math.pow(m, i));
        }
    }
}
