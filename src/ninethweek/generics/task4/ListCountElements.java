package ninethweek.generics.task4;

import java.util.List;

/*Реализовать метод, который считает кол-во элементов в списке*/
public class ListCountElements {
    public static <T> int countIf(List<T> from, T element) {
        int counter = 0;
        for (T elem:
             from) {
            if(elem.equals(element)){
                counter++;
            }
        }
        return counter;
    }
}
