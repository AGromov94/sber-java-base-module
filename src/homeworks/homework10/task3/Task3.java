package homeworks.homework10.task3;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Task3 {
    public static void main(String[] args) {
        //создаем экземпляр через new, вызываем метод с помощью рефлексии
        try {
            Method printMethod = APrinter.class.getMethod("print", int.class);
            printMethod.invoke(new APrinter(), 20);
        } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            e.printStackTrace();
        }

        //создаем экземпляр через рефлексию и вызываем метод с помощью рефлексии
        try {
            Constructor <APrinter> constructor = APrinter.class.getConstructor();
            APrinter printer = constructor.newInstance();
            Method printMethod = printer.getClass().getMethod("print", int.class);
            printMethod.invoke(printer, 30);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
