package homeworks.homework8.task2;

public class Main {
    public static void main(String[] args) {
        BestCarpenterEver factory = new BestCarpenterEver();
        Furniture table = new Table();
        Furniture stool = new Stool();

        table.getNameOfFurniture();
        System.out.println(factory.checkToRepair(table)); //столы не чиним - false
        stool.getNameOfFurniture();
        System.out.println(factory.checkToRepair(stool)); //табуретки чиним - true
    }
}
