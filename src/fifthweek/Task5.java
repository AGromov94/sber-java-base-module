package fifthweek;

import java.util.*;

/*
На вход передается N — высота двумерного массива и M — его ширина.
Затем передается сам массив.
Необходимо сохранить в одномерном массиве суммы чисел каждого столбца и вывести их на экран.
Пример:
Входные данные
2 2
10 20
5 7
Выходные данные
15 27
Входные данные
3 1
30
42
15
Выходные данные
87 */
public class Task5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int m = scan.nextInt();
        int[][] arr = new int[n][m];
        int[] answerArr = new int[m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                arr[i][j] = scan.nextInt();
            }
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                answerArr[i] += arr[j][i];
            }
        }
        System.out.println(Arrays.toString(answerArr));
    }
}
