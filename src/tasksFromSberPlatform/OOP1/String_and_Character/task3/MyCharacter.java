package tasksFromSberPlatform.OOP1.String_and_Character.task3;

/*public MyCharacter(char value);
public char charValue();
public int compareTo(MyCharacter anotherCharacter);
public boolean equals(MyCharacter anotherCharacter);
public boolean isDigit();
public static boolean isDigit(char ch);
public static boolean isLetter(char ch);
public static boolean isLetterOrDigit(char ch);
public static boolean isLowerCase(char ch);
public static boolean isUpperCase(char ch);
public static char toUpperCase(char ch);
public static char toLowerCase(char ch);*/
public class MyCharacter {
    private char character;

    public MyCharacter(char character) {
        this.character = character;
    }

    public char charValue() {
        return character;
    }

    public int compareTo(MyCharacter anotherCharacter) {
        return character - anotherCharacter.charValue();
    }

    public boolean equals(MyCharacter anotherCharacter) {
        return character == anotherCharacter.charValue();
    }

    public boolean isDigit() {
        return isDigit(character);
    }

    public static boolean isDigit(char ch) {
        return '0' <= ch && ch <= '9';
    }

    public static boolean isLetter(char ch) {
        return ('a' <= ch && ch <= 'z') || ('A' <= ch && ch <= 'Z');
    }

    public static boolean isLetterOrDigit(char ch) {
        return isLetter(ch) || isDigit(ch);
    }

    public static boolean isLowerCase(char ch) {
        return 'a' <= ch && ch <= 'z';
    }

    public static boolean isUpperCase(char ch) {
        return 'A' <= ch && ch <= 'Z';
    }

    public static char toUpperCase(char ch) {
        if (isLowerCase(ch)) {
            return (char) (ch - 32);
        } else {
            return ch;
        }
    }

    public static char toLowerCase(char ch) {
        if (isUpperCase(ch)) {
            return (char) (ch + 32);
        } else {
            return ch;
        }
    }
}
