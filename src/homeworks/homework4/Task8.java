package homeworks.homework4;

import java.util.*;
/*8.	(1 балл) На вход подается число N — длина массива. Затем передается массив целых чисел (ai) из N элементов.
        После этого передается число M.

        Необходимо найти в массиве число, максимально близкое к M (т.е. такое число, для которого |ai - M| минимальное).
        Если их несколько, то вывести максимальное число.

        Ограничения:
        ●	0 < N < 100
        ●	-1000 < ai < 1000
        ●	-1000 < M < 1000*/

public class Task8 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int m;
        int value;
        int resultIndex = 0;
        int n = scan.nextInt();
        int[] arr = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = scan.nextInt();
        }
        m = scan.nextInt();
        value = Math.abs(arr[0] - m);
        for (int i = 1; i < n; i++) {
            if (value >= Math.abs(arr[i] - m)) {
                resultIndex = i;
                value = Math.abs(arr[i] - m);
            } else {
                continue;
            }
        }
        System.out.println(arr[resultIndex]);
    }
}
