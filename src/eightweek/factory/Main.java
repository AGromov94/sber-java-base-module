package eightweek.factory;

public class Main {
    public static void main(String[] args) {
        IceCream chocolateIceCream = new ChocolateIceCream();//полиморфизм
        chocolateIceCream.printIngredients();

        IceCreamFactory iceCreamFactory = new IceCreamFactory();
        IceCream myCherryIceCream = iceCreamFactory.getIceCream(IceCreamType.CHERRY);
    }
}
