package tasksFromSberPlatform.OOP1.task8;

import tasksFromSberPlatform.OOP1.task7.MyTime;

public class MyIntegerClient {
    public static void main(String[] args) {
        MyInteger integer1 = new MyInteger(16);
        MyInteger integer2 = new MyInteger(1);
        MyInteger integer3 = new MyInteger(9);

        System.out.println(integer1.isEven());
        System.out.println(integer1.isOdd());
        System.out.println(integer1.isPrime());

        System.out.println(integer2.isEven());
        System.out.println(integer2.isOdd());
        System.out.println(integer2.isPrime());

        System.out.println(integer3.isEven());
        System.out.println(integer3.isOdd());
        System.out.println(integer3.isPrime());

        System.out.println(MyInteger.parseInt(new char[]{'2', '0', '2', '4'}));
        System.out.println(MyInteger.parseInt("2024"));

        System.out.println(MyInteger.isEven(8));
        System.out.println(MyInteger.isPrime(integer1));

        System.out.println(integer1.equals(16));
        System.out.println(integer1.equals(integer2));
    }
}
