package homeworks.homework4;

import java.util.*;
/*9.	(1 балл) На вход подается число N — длина массива. Затем передается массив строк из N элементов (разделение через перевод строки).
        Каждая строка содержит только строчные символы латинского алфавита.

        Необходимо найти и вывести дубликат на экран. Гарантируется что он есть и только один.

        Ограничения:
        ●	0 < N < 100
        ●	0 < ai.length() < 1000*/

public class Task9 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        scan.nextLine();
        String[] arr = new String[n];
        int flag = 0;
        String answer = " ";

        for (int i = 0; i < n; i++) {
            arr[i] = scan.nextLine();
        }

        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (arr[i].equals(arr[j])) {
                    answer = arr[i];
                    flag++;
                    break;
                }
            }
            if (flag == 1) {
                break;
            }
        }
        System.out.println(answer);
    }
}
