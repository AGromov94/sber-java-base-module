package tasksFromSberPlatform.OOP2.Generics.task22;

public class NewGenericStackTest {
    public static void main(String[] args) {
        NewGenericStack<String> stack = new NewGenericStack<String>();
        stack.push("One");
        stack.push("Two");
        stack.push("Three");
        stack.push("Four");
        stack.push("Five");

        System.out.println(stack.toString());

        while (!stack.isEmpty()) {
            System.out.println(stack.pop());
        }
    }
}
