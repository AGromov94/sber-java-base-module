package homeworks.homework9.task4;

public class MyEvenNumberTest {
    public static void main(String[] args) {
        try {
            MyEvenNumber number1 = new MyEvenNumber(2);
            MyEvenNumber number2 = new MyEvenNumber(6);
            MyEvenNumber number3 = new MyEvenNumber(3);
        } catch (IllegalCreateEvenNumberException e) {
            System.out.println(e.getNumber() + " - НЕ четное число");
            e.printStackTrace();
        }
        System.out.println(MyEvenNumber.getCounterOfElements()); //проверяем, что создалось 2 экземпляра нашего класса и прога не упала
    }
}
