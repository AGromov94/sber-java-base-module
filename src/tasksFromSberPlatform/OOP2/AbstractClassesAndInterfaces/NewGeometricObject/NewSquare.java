package tasksFromSberPlatform.OOP2.AbstractClassesAndInterfaces.NewGeometricObject;

public class NewSquare extends NewGeometricObject implements Colorable {
    private double side;

    public NewSquare(String color, boolean filled, double side) {
        super(color, filled);
        this.side = side;
    }

    public NewSquare() {
        this.side = 0;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    @Override
    public double getArea() {
        return side * side;
    }

    @Override
    public double getPerimeter() {
        return side * 4;
    }

    @Override
    public void howToColor() {
        System.out.println("Раскрасьте все четыре стороны");
    }

    @Override
    public String toString() {
        return super.toString() +
                "сторона квадрата равна " + side;
    }
}
