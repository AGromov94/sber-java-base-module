package homeworks.homework5;

import java.util.*;
/*3.	На вход подается число N — количество строк и столбцов матрицы. Затем передаются координаты X и Y расположения коня на шахматной доске.

        Необходимо заполнить матрицу размера NxN нулями, местоположение коня отметить символом K, а позиции, которые он может бить, символом X.

        Ограничения:
        ●	4 < N < 100
        ●	0 <= X, Y < N*/

public class Task3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        char[][] matrix = new char[n][n];
        final int X = scan.nextInt();
        final int Y = scan.nextInt();
        final int[][] COORDINATES_OF_HORSE = {{-2, -1}, {-2, 1}, {-1, -2}, {-1, 2}, {1, -2}, {1, 2}, {2, -1}, {2, 1}};

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = '0';
            }
        }

        matrix[Y][X] = 'K';

        for (int i = 0; i < COORDINATES_OF_HORSE.length; i++) {
            if (Y + COORDINATES_OF_HORSE[i][0] >= 0 && Y + COORDINATES_OF_HORSE[i][0] < n && X + COORDINATES_OF_HORSE[i][1] >= 0 && X + COORDINATES_OF_HORSE[i][1] < n) {
                matrix[Y + COORDINATES_OF_HORSE[i][0]][X + COORDINATES_OF_HORSE[i][1]] = 'X';
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j != n - 1) {
                    System.out.print(matrix[i][j] + " ");
                } else {
                    System.out.print(matrix[i][j]);
                }
            }
            System.out.println();
        }
    }
}
