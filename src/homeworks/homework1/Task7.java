package homeworks.homework1;

import java.util.*;
/*На вход подается двузначное число n. Выведите число, полученное перестановкой цифр в исходном числе n. Если после перестановки получается ведущий 0, его также надо вывести.

        Ограничения:
        9 < count < 100*/

public class Task7 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int inputNumber = scan.nextInt();
        int firstNumber = inputNumber / 10;
        int secondNumber = inputNumber % 10;

        System.out.println(secondNumber + "" + firstNumber);
    }
}
