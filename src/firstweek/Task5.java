package firstweek;

import java.util.*;

/*
 Напишите аналог функции swap, которая меняет значения двух параметров местами (без вспомогательной переменной)
 Входные данные
 a = 8; b = 10
     */
public class Task5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();

        a = a + b;
        b = a - b;
        a = a - b;

        System.out.println("a = " + a);
        System.out.println("b = " + b);
    }
}
