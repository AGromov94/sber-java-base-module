package homeworks.homework3;

import java.util.*;

/*4. Дано натуральное число n. Вывести его цифры в “столбик”.
        Ограничения:
        0 < n < 1000000*/
public class Task4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        String str = Integer.toString(n);

        for (int i = 0; i < str.length(); i++) {
            System.out.println(str.charAt(i));
        }
    }
}
