package tasksFromSberPlatform.OOP1.String_and_Character.task1;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        MyString1 testString1 = new MyString1(new char[]{'W', 'e', 'l', 'c', 'o', 'm', 'e'});
        MyString1 testString2 = new MyString1(new char[]{'W', 'e', 'l', 'c', 'o'});

        System.out.println(Arrays.toString(testString1.substring(1, 6).toChars()));
        System.out.println(testString1.equals(testString2));
        System.out.println(Arrays.toString(MyString1.valueOf(Integer.MAX_VALUE).toChars()));
    }
}
