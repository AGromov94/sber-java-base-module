package homeworks.homework1;

import java.util.*;
/*На вход подается два целых числа a и b. Вычислите и выведите среднее квадратическое a и b.

        Подсказка:
        Среднее квадратическое: https://en.wikipedia.org/wiki/Root_mean_square
        Для вычисления квадратного корня воспользуйтесь функцией Math.sqrt(x)

        Ограничения:
        0 < a, b < 100*/

public class Task2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();

        double averageSquare = Math.pow(((Math.pow(a, 2) + Math.pow(b, 2)) / 2), 1.0 / 2);
        System.out.println(averageSquare);
    }
}
