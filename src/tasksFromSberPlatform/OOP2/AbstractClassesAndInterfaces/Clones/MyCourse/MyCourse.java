package tasksFromSberPlatform.OOP2.AbstractClassesAndInterfaces.Clones.MyCourse;

import java.util.*;

/*      Перепишите класс Course из программы TestCourse в разделе «Объектно-ориентированное мышление»,
        чтобы добавить метод clone() для выполнения глубокой копии поля students.*/
public class MyCourse implements Cloneable {
    private String courseName;
    private String[] students = new String[100];
    private int numberOfStudents;

    public MyCourse(String courseName) {
        this.courseName = courseName;
    }

    public void addStudent(String student) {
        students[numberOfStudents] = student;
        numberOfStudents++;
    }

    public String[] getStudents() {
        return students;
    }

    public int getNumberOfStudents() {
        return numberOfStudents;
    }

    public String getCourseName() {
        return courseName;
    }

    public void dropStudent(String student) {
        int deleteIndex = 0;
        for (int i = 0; i < getNumberOfStudents(); i++) {
            if (student.equalsIgnoreCase(students[i])) {
                deleteIndex = i;
            }
        }
        String[] newArr = new String[student.length() - 1];
        System.arraycopy(students, 0, newArr, 0, deleteIndex);
        System.arraycopy(students, deleteIndex + 1, newArr, deleteIndex, student.length() - deleteIndex - 1);
        students = newArr;
        numberOfStudents--;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        MyCourse MyCourseClone = (MyCourse) super.clone(); //поверхностная копия
        //так как есть поле students ссылочного типа, требуется глубокое копирование
        MyCourseClone.students = students.clone(); //глубокая копия
        return MyCourseClone;
    }
}
