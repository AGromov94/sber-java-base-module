package tasksFromSberPlatform.OOP1.bigInteger;

import java.math.BigInteger;

/*      Простое число называется простым числом Мерсенна, если его можно записать в виде 2^p - 1,
        где р — положительное целое число. Напишите программу, которая находит все простые числа Мерсенна для p <= 100
        и отображает следующие выходные данные:
        p   2^p - 1
        ---------------------
        2      3

        3      7

        5      31*/
public class Task4 {
    public static void main(String[] args) {
        BigInteger j = new BigInteger("2");

        for (int i = 2; i <= 1000; i++) {
            j = j.multiply(BigInteger.TWO);
            if (j.subtract(BigInteger.ONE).isProbablePrime(1)) {
                System.out.println(i + "\t" + j.subtract(BigInteger.ONE));
            }
        }
    }
}
