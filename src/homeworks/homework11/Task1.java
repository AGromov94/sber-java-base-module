package homeworks.homework11;

import java.util.Arrays;
import java.util.List;

// 1.	Посчитать сумму четных чисел в промежутке от 1 до 100 включительно и вывести ее на экран.
//      Использовать stream API.
public class Task1 {
    public static void main(String[] args) {
        int[] arr = new int[100];
        for (int i = 0; i < 100; i++) {
            arr[i] = i + 1;
        }
        int sum = Arrays.stream(arr).filter(i -> i % 2 == 0).sum();
        System.out.println(sum);
        int sum1 = 0;
        for (int i : arr) {
            if (i % 2 == 0)
                sum1 += i;
        }
        System.out.println(sum1);
    }
}
