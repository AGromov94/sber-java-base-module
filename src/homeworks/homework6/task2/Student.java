package homeworks.homework6.task2;
/*2.	Необходимо реализовать класс Student.
        У класса должны быть следующие приватные поля:
        ●	String name — имя студента
        ●	String surname — фамилия студента
        ●	int[] grades — последние 10 оценок студента. Их может быть меньше, но не может быть больше 10.
        И следующие публичные методы:
        ●	геттер/сеттер для name
        ●	геттер/сеттер для surname
        ●	геттер/сеттер для grades
        ●	метод, добавляющий новую оценку в grades. Самая первая оценка должна быть удалена, новая должна сохраниться в конце массива
        (т.е. массив должен сдвинуться на 1 влево).
        ●	метод, возвращающий средний балл студента (рассчитывается как среднее арифметическое от всех оценок в массиве grades)*/

public class Student {
    private String name;
    private String surname;
    private int[] grades = new int[10];

    public Student(String name, String surname, int[] grades) {
        this.name = name;
        this.surname = surname;
        this.grades = grades;
    }

    public Student() {
    }

    //region getters
    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int[] getGrades() {
        return grades;
    }
    //endregions

    //region setters
    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }
    //endregion

    public void setNewMarkIntoGradesArray(int mark, int[] grades) {
        for (int i = 0; i < 9; i++) {
            grades[i] = grades[i+1];
        }
        grades[9] = mark;
    }

    public double returnAverageOfMarks(int[] grades) {
        int sum = 0;
        for (int i = 0; i < 10; i++) {
            sum += grades[i];
        }
        return sum / 10.0;
    }
}
