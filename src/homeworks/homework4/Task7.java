package homeworks.homework4;

import java.util.*;
/*7.	(1 балл) На вход подается число N — длина массива. Затем передается массив целых чисел (ai) из N элементов, отсортированный по возрастанию.

        Необходимо создать массив, полученный из исходного возведением в квадрат каждого элемента,
        упорядочить элементы по возрастанию и вывести их на экран.

        Ограничения:
        ●	0 < N < 100
        ●	-1000 < ai < 1000*/

public class Task7 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[] arr = new int[n];
        int[] resultArr = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = scan.nextInt();
        }
        for (int i = 0; i < n; i++) {
            resultArr[i] = (int) Math.pow(arr[i], 2);
        }
        Arrays.sort(resultArr);
        for (int i = 0; i < n; i++) {
            System.out.print(resultArr[i] + " ");
        }
    }
}
