package thirdweek;

import java.util.*;

//Дана строка s. Вычислить кол-во символов в ней не считая пробелов.
public class Task4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str = scan.nextLine();
        int counter = 0;

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) != ' ') {
                counter++;
            }
        }
        System.out.println(counter);
    }
}
