package tasksFromSberPlatform.OOP1.task10;

import java.util.*;

/*      Напишите клиент этих классов — программу, которая запрашивает у пользователя ввести три стороны треугольника,
        цвет и логическое значение для указания закрашен ли треугольник. Программа должна создавать объект типа Triangle с указанными
        сторонами и присваивать значения свойствам цвет (color) и заливка (isFilled) с помощью этих входных данных.
        Программа должна отображать площадь (area), периметр (perimeter), цвет, а также true или false для указания,
        закрашен треугольник или нет.*/
public class TriangleTest {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Привет, пользователь!" + "\n" + "Введи длины сторон треугольника (3 числа)");
        double side1 = scan.nextDouble();
        double side2 = scan.nextDouble();
        double side3 = scan.nextDouble();
        scan.nextLine();
        System.out.println("Введи цвет треугольника");
        String color = scan.nextLine();
        System.out.println("Введи логическое значение (true/false)");
        boolean filled = scan.nextBoolean();

        try {
            Triangle triangle = new Triangle(side1, side2, side3);
            triangle.setColor(color);
            triangle.setFilled(filled);

            System.out.println("Площадь нашего треугольника равна: " + triangle.getArea() + "\n" +
                    "Периметр нашего треугольника равен: " + triangle.getPerimeter() + "\n" +
                    "Цвет нашего треугольника: " + triangle.getColor() + "\n" +
                    "Наш треугольник закрашен? " + triangle.isFilled());
        } catch (IllegalTriangleException e) {

            System.out.println(e.getClass());
            System.out.println(e.getMessage());
            System.out.println("Стороны:");
            System.out.println(e.getSide1());
            System.out.println(e.getSide2());
            System.out.println(e.getSide3());
        }
    }
}
