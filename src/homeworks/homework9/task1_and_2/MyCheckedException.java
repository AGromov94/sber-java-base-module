package homeworks.homework9.task1_and_2;

//1.	Создать собственное исключение MyCheckedException, являющееся проверяемым.
public class MyCheckedException extends Exception {
    public MyCheckedException() {
    }

    public MyCheckedException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return "This is my checked Exception";
    }
}
