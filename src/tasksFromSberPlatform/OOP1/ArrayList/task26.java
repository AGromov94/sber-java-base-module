package tasksFromSberPlatform.OOP1.ArrayList;

import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;

/*      Напишите программу, которая случайным образом заполняет матрицу n x n значениями 0 и 1,
        отображает эту матрицу в консоли и находит строчки и столбцы с наибольшим количеством 1.
        (Подсказка: используйте два списка типа ArrayList для хранения индексов строчек и столбцов с наибольшим количеством 1.)
        Далее приведен пример выполнения этой программы:

        Введите размер матрицы: 4
        Матрица со случайными значениями равна
        0011
        0011
        1101
        1010
        Индекс строчки с наибольшим кол-вом единиц: 2
        Индекс столбца с наибольшим кол-вом единиц: 2, 3*/
public class task26 {
    public static void main(String[] args) {
        ArrayList<Integer> counterForOneInRows = new ArrayList<>();
        ArrayList<Integer> counterForOneInColumns = new ArrayList<>();
        int maxIndexInRows = 0;
        int maxIndexInColumns = 0;
        int n = 4;
        int[][] matrix = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = (int) (Math.random() * 2);
            }
        }

        System.out.println("Матрица со случайными значениями равна:");
        for (int[] columns : matrix) {
            for (int number : columns) {
                System.out.print(number + " ");
            }
            System.out.println();
        }

        for (int i = 0; i < n; i++) {
            int sum = 0;
            for (int j = 0; j < n; j++) {
                sum += matrix[i][j];
            }
            counterForOneInRows.add(sum);
        }

        for (int i = 0; i < n; i++) {
            int sum = 0;
            for (int j = 0; j < n; j++) {
                sum += matrix[j][i];
            }
            counterForOneInColumns.add(sum);
        }

        for (int i = 1; i < counterForOneInRows.size(); i++) {
            if (counterForOneInRows.get(0) < counterForOneInRows.get(i)) {
                maxIndexInRows = i;
            }
        }

        for (int i = 1; i < counterForOneInColumns.size(); i++) {
            if (counterForOneInColumns.get(0) < counterForOneInColumns.get(i)) {
                maxIndexInColumns = i;
            }
        }

        System.out.println("Индекс строчки с наибольшим кол-вом единиц: " + maxIndexInRows);
        System.out.println("Индекс столбца с наибольшим кол-вом единиц: " + maxIndexInColumns);
    }
}
