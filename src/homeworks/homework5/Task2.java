package homeworks.homework5;

import java.util.*;
/*2.	На вход подается число N — количество строк и столбцов матрицы.
        Затем в последующих двух строках подаются координаты X (номер столбца) и Y (номер строки) точек, которые задают прямоугольник.

        Необходимо отобразить прямоугольник с помощью символа 1 в матрице, заполненной нулями (см. пример) и вывести всю матрицу на экран.

        Ограничения:
        ●	0 < N < 100
        ●	0 <= X1, Y1, X2, Y2 < N
        ●	X1 < X2
        ●	Y1 < Y2*/

public class Task2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[][] matrix = new int[n][n];
        int x1 = scan.nextInt();
        int y1 = scan.nextInt();
        int x2 = scan.nextInt();
        int y2 = scan.nextInt();

        for (int i = y1; i <= y2; i++) {
            for (int j = x1; j <= x2; j++) {
                matrix[i][j] = 1;
            }
        }

        for (int i = y1 + 1; i < y2; i++) {
            for (int j = x1 + 1; j < x2; j++) {
                matrix[i][j] = 0;
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i != n - 1) {
                    System.out.print(matrix[i][j] + " ");
                } else {
                    System.out.print(matrix[i][j]);
                }
            }
            System.out.println();
        }
    }
}
