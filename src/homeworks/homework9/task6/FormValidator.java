package homeworks.homework9.task6;
/*6.	Фронт со своей стороны не сделал обработку входных данных анкеты! Петя очень зол и ему придется написать свои проверки,
        а также кидать исключения, если проверка провалилась. Помогите Пете написать класс FormValidator со статическими методами проверки.
        На вход всем методам подается String str.
        a.	public void checkName(String str) — длина имени должна быть от 2 до 20 символов, первая буква заглавная.
        b.	public void checkBirthdate(String str) — дата рождения должна быть не раньше 01.01.1900 и не позже текущей даты.
        c.	public void checkGender(String str) — пол должен корректно матчится в enum Gender, хранящий Male и Female значения.
        d.	public void checkHeight(String str) — рост должен быть положительным числом и корректно конвертироваться в double.*/

import java.text.SimpleDateFormat;
import java.util.Date;

public class FormValidator {
    public static void checkName(String str) throws CheckException {
        if ((str.length() < 2 || str.length() > 20) || Character.isLowerCase(str.charAt(0))) {
            throw new CheckException("Wrong name");
        }
    }

    public static void checkBirthdate(String str) throws CheckException {
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            Date startDate = format.parse("01.01.1900");
            Date inputDate = format.parse(str);
            Date currentDate = new Date();
            if (inputDate.before(startDate) || inputDate.after(currentDate)) {
                throw new CheckException("Date is not valid");
            }
        } catch (Exception ex) {
            throw new CheckException("Invalid birthdate");
        }
    }

    public void checkGender(String str) throws CheckException {
        try {
            Gender.valueOf(str.toUpperCase());
        } catch (IllegalArgumentException ex) {
            throw new CheckException("Wrong gender");
        }
    }

    public static void checkHeight(String str) throws CheckException {
        try {
            if (Double.parseDouble(str) <= 0) {
                throw new IllegalArgumentException();
            }
        } catch (RuntimeException ex) {
            throw new CheckException("Invalid height");
        }
    }
}
