package tasksFromSberPlatform.OOP1.project;

/*      Напишите клиент этого класса — программу, которая создает объект типа Account с ID счета, равным 1122, балансом,
        равным 20 000 руб., и годовой процентной ставкой, равной 4.5%. Используйте метод withdraw(), чтобы снять 2 500 руб.,
        метод deposit(), чтобы положить 3 000 руб. и отобразите в консоли баланс, ежемесячные проценты и дату создания этого счета.*/
public class AccountTest {
    public static void main(String[] args) {
        Account testAccount = new Account("Ivan", 1, 20000);
        Account.setAnnualInterestRate(4.5);
        testAccount.withdraw(2500);
        testAccount.deposit(3000);

        testAccount.printHistoryOfOperations();
    }
}
