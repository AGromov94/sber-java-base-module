package homeworks.homework10.task4;
/*4.	Написать метод, который с помощью рефлексии получит все интерфейсы класса,
        включая интерфейсы от классов-родителей и интерфейсов-родителей.*/

import tasksFromSberPlatform.OOP2.AbstractClassesAndInterfaces.NewGeometricObject.NewSquare;

import java.util.*;

public class Task4 {
    public static void main(String[] args) {
        Class<NewSquare> squareClass = NewSquare.class;
        List<Class<?>> list = getAllInterfaces(squareClass);

        for (Class<?> cls : list
        ) {
            System.out.println(cls.getName());
        }
    }

    public static List<Class<?>> getAllInterfaces(Class<?> cls) {
        List<Class<?>> interfaces = new ArrayList<>();
        while (cls != Object.class) {
            interfaces.addAll(Arrays.asList(cls.getInterfaces()));
            cls = cls.getSuperclass();
        }

        for (int i = 0; i < interfaces.size(); i++) {
            List<Class<?>> superInterfaces = new ArrayList<>(Arrays.asList(interfaces.get(i).getInterfaces()));
            interfaces.addAll(superInterfaces);
        }
        return interfaces;
    }
}