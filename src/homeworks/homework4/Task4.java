package homeworks.homework4;

import java.util.*;
/*4.	(1 балл) На вход подается число N — длина массива. Затем передается массив целых чисел (ai) из N элементов, отсортированный по возрастанию.

        Необходимо вывести на экран построчно сколько встретилось различных элементов.
        Каждая строка должна содержать количество элементов и сам элемент через пробел.

        Ограничения:
        ●	0 < N < 100
        ●	-1000 < ai < 1000*/

public class Task4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[] arr = new int[n];
        int counter = 0;

        for (int i = 0; i < n; i++) {
            arr[i] = scan.nextInt();
        }

        for (int i = 0; i < n; i++) {
            if ((i != 0) && arr[i] == arr[i - 1]) {
                continue;
            }
            for (int j = 0; j < n; j++) {
                if (arr[i] == arr[j]) {
                    counter++;
                }
            }
            System.out.println(counter + " " + arr[i]);
            counter = 0;
        }
    }
}
