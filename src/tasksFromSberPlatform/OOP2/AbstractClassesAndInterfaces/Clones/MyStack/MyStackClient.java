package tasksFromSberPlatform.OOP2.AbstractClassesAndInterfaces.Clones.MyStack;

public class MyStackClient {
    public static void main(String[] args) {
        MyStack stack1 = new MyStack();
        stack1.push(1);
        stack1.push(2);
        stack1.push(3);
        stack1.push(4);
        try {
            MyStack stack2 = (MyStack) stack1.clone();
            stack1.push(5);
            stack1.push(6);
            System.out.println(stack1.toString());
            System.out.println(stack2.toString());
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}
