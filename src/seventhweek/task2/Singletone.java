package seventhweek.task2;

//Синглтон (одиночка) - паттерн, дающий гарантию, что:
//экземпляр класса будет только один
//предоставляет глобальную точку доступа к экземпляру
/*
Самая простая реализация:
+
1. Простота и прозрачность кода
2. Высокая производительность в многопоточной среде
3. Потокобезопасность
-
1. Не ленивая инициализация*/
public class Singletone {
    private static final Singletone INSTANCE = new Singletone();

    private Singletone() {

    }

    public static Singletone getInstance() {
        return INSTANCE;
    }
}
