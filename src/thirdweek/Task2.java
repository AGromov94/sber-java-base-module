package thirdweek;

import java.util.*;

//Найти произведение всех чисел в диапазоне между m и n включительно. m < n.
public class Task2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите число m");
        int m = scan.nextInt();
        System.out.println("Введите число n");
        int n = scan.nextInt();
        int result = 1;

        while (m <= n){
            result *= m;
            m++;
        }
        System.out.println(result);
    }
}
