package tasksFromSberPlatform.OOP1.task3;

import java.util.Random;

/*        Напишите программу, которая создает объект типа Random с начальным (случайным) значением 1000
          и отображает первые 50 случайных целых чисел между 0 и 100 с помощью метода nextInt(100).*/
public class Task3_2 {
    public static void main(String[] args) {
        Random randomizer = new Random(1000);
        for (int i = 0; i < 50; i++) {
            System.out.println(1 + randomizer.nextInt(99)); //от 0 до 100 НЕ включая границы!
            // System.out.println(randomizer.nextInt(101)); //от 0 до 100 включая границы!
        }
    }
}
