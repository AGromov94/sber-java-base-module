package homeworks.homework3;

import java.util.*;

/*7. Дана строка s. Вычислить количество символов в ней, не считая пробелов
        (необходимо использовать цикл).
        Ограничения:
        0 < s.length() < 1000*/
public class Task7 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str = scan.nextLine();
        int counter = 0;

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) != ' ') {
                counter++;
            }
        }
        System.out.println(counter);
    }
}
