package tasksFromSberPlatform.OOP2.Exceptions.Practicum;

public class NullDenominatorException extends Exception {
    private int numerator;
    private int denominator;

    public NullDenominatorException() {
    }

    public NullDenominatorException(int numerator, int denominator, String message) {
        super(message);
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
