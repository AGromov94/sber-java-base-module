package homeworks.homework9.task4;

/*4.	Создать класс MyEvenNumber, который хранит четное число int n. Используя исключения,
        запретить создание инстанса MyEvenNumber с нечетным числом.*/
public class MyEvenNumber {
    private int evenNumber;
    private static int counterOfElements = 0;

    public MyEvenNumber() {
        this.evenNumber = 0;
    }

    public MyEvenNumber(int evenNumber) throws IllegalCreateEvenNumberException {
        if (evenNumber % 2 == 0) {
            this.evenNumber = evenNumber;
            counterOfElements++;
        } else {
            throw new IllegalCreateEvenNumberException("Данное число НЕ четное", evenNumber);
        }
    }

    public int getEvenNumber() {
        return evenNumber;
    }

    public static int getCounterOfElements() {
        return counterOfElements;
    }
}
