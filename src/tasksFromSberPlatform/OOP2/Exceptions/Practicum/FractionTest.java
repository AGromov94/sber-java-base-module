package tasksFromSberPlatform.OOP2.Exceptions.Practicum;

public class FractionTest {
    public static void main(String[] args) {
        try {
            Fraction fraction1 = new Fraction(1, 2);
            System.out.println(fraction1);
            Fraction fraction2 = new Fraction(1, 0);
            System.out.println(fraction2);
        } catch (NullDenominatorException e){
            System.out.println(e.getMessage());
            System.out.println(e.getNumerator() + "/" + e.getDenominator());
        }
    }
}
