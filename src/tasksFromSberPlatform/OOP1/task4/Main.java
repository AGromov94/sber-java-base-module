package tasksFromSberPlatform.OOP1.task4;

public class Main {
    public static void main(String[] args) {
        //1000000 элементов - 555456 мс
        //100000 элементов - 5873 мс
        //10000 элементов - 72 мс
        StopWatch myStopWatch = new StopWatch();
        int[] array = new int[10000];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 100000);
        }
        myStopWatch.start();//старт миллисекундомера
        selectionSort(array);
        myStopWatch.stop();//стоп миллисекундомера
        System.out.println(myStopWatch.getElapsedTime());
    }

    public static void selectionSort(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            // Найти наименьшее значение в arr[0..arr.length-1]
            int currentMin = arr[i];
            int currentMinIndex = i;

            for (int j = i + 1; j < arr.length; j++) {
                if (currentMin > arr[j]) {
                    currentMin = arr[j];
                    currentMinIndex = j;
                }
            }

            // Переставить arr[i] и arr[currentMinIndex], если необходимо
            if (currentMinIndex != i) {
                arr[currentMinIndex] = arr[i];
                arr[i] = currentMin;
            }
        }
    }
}
