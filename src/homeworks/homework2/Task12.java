package homeworks.homework2;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/*У Марата был взломан пароль. Он решил написать программу, которая проверяет его пароль на сложность. В интернете он узнал,
        что пароль должен отвечать следующим требованиям:
        ●	пароль должен состоять из хотя бы 8 символов;
        ●	в пароле должны быть:
        ○	заглавные буквы
        ○	строчные символы
        ○	числа
        ○	специальные знаки(_*-)
        Если пароль прошел проверку, то программа должна вывести в консоль строку пароль надежный, иначе строку: пароль не прошел проверку*/

public class Task12 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String password = scan.nextLine();
        Pattern pattern1 = Pattern.compile("[A-Z]");
        Pattern pattern2 = Pattern.compile("[a-z]");
        Pattern pattern3 = Pattern.compile("[\\d]");
        Pattern pattern4 = Pattern.compile("_|-|/*");
        Matcher matcher1 = pattern1.matcher(password);
        Matcher matcher2 = pattern2.matcher(password);
        Matcher matcher3 = pattern3.matcher(password);
        Matcher matcher4 = pattern4.matcher(password);

        if (password.length() >= 8) {
            if (matcher1.find() && matcher2.find() && matcher3.find() && matcher4.find()) {
                System.out.println("пароль надежный");
            } else {
                System.out.println("пароль не прошел проверку");
            }
        } else {
            System.out.println("пароль не прошел проверку");
        }
    }
}
