package tasksFromSberPlatform.OOP2.AbstractClassesAndInterfaces.NewGeometricObject;

/*      Спроектируйте интерфейс с именем Colorable с помощью метода howToColor() типа void.
        Каждый класс раскрашиваемого объекта должен реализовывать интерфейс Colorable.
        Спроектируйте класс Square, который наследуется от GeometricObject и реализует Colorable.
        Реализуйте метод howToColor() для отображения сообщения Раскрасьте все четыре стороны.
        Класс Square содержит поле данных side с getter- и setter-методами, а также конструктор для создания Square с указанной стороной.
        У класса Square есть скрытое поле данных типа double с именем side и getter- и setter-методами.
        У него есть безаргументный конструктор, который создает объект типа Square со стороной, равной 0,
        и еще один конструктор, который создает объект типа Square с указанной стороной.

        Нарисуйте UML-диаграмму, которая включает в себя Colorable, Square и GeometricObject. Напишите тестовую программу,
        которая создает массив из пяти объектов типа GeometricObjects.
        Для каждого объекта в массиве отобразите его площадь и вызовите метод howToColor(), если его можно раскрасить.*/
public interface Colorable extends OneMore {
    void howToColor();
}
