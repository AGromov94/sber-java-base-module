package tasksFromSberPlatform.OOP2.AbstractClassesAndInterfaces.NewGeometricObject;

import tasksFromSberPlatform.OOP1.task10.IllegalTriangleException;

/*      Спроектируйте новый класс Triangle, который наследуется от абстрактного класса GeometricObject.
        Нарисуйте UML-диаграмму классов Triangle и GeometricObject, а затем реализуйте класс Triangle.
        Напишите тестовую программу, которая запрашивает у пользователя ввод трёх сторон треугольника,
        цвета и логического значения для указания заливки. Программа должна создать объект типа Triangle с этими сторонами
        и задать свойства color и filled, используя введенные пользователем данные. Программа должна отображать площадь,
        периметр, цвет и значение true или false для указания заливки.*/

/*      Измените класс GeometricObject, чтобы реализовать интерфейс Comparable и определить статический метод max() в классе GeometricObject
        для поиска наибольшего из двух объектов типа GeometricObject. Нарисуйте UML-диаграмму и реализуйте новый класс GeometricObject.
        Напишите тестовую программу, которая использует метод max() для поиска наибольшего из двух кругов и наибольшего из двух прямоугольников.*/
public class NewTriangle extends NewGeometricObject {
    private double side1 = 1.0;
    private double side2 = 1.0;
    private double side3 = 1.0;

    public NewTriangle(String color, boolean filled) {
        super(color, filled);
    }

    public NewTriangle(String color, boolean filled, double side1, double side2, double side3) throws IllegalTriangleException {
        super(color, filled);
        if (side1 + side2 > side3 && side2 + side3 > side2 && side1 + side3 > side2) {
            this.side1 = side1;
            this.side2 = side2;
            this.side3 = side3;
        } else {
            throw new IllegalTriangleException(side1, side2, side3, "Сумма двух сторон меньше третьей");
        }
    }

    //region getters
    public double getSide1() {
        return side1;
    }

    public double getSide2() {
        return side2;
    }

    public double getSide3() {
        return side3;
    }
    //endregion

    @Override
    public double getArea() {
        double halfOfPerimeter = getPerimeter() / 2;
        return Math.pow(halfOfPerimeter * (halfOfPerimeter - side1) * (halfOfPerimeter - side2) * (halfOfPerimeter - side3), 0.5);
    }

    @Override
    public double getPerimeter() {
        return side1 + side2 + side3;
    }

    @Override
    public String toString() {
        return super.toString() + "Треугольник: сторона1 = " + side1 + ", сторона2 = " + side2 +
                ", сторона3 = " + side3;
    }
}
