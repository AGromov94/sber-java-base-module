package tasksFromSberPlatform.OOP1.String_and_Character.task1;

import java.util.Arrays;

/*Java-библиотека предоставляет класс String. Предоставьте собственную реализацию следующих методов (назовите новый класс MyString1):

public MyString1(char[] chars);
public char charAt(int index);
public int length();
public MyString1 substring(int begin, int end);
public MyString1 toLowerCase();
public static MyString1 valueOf(int i);
public char[] toChars();
public boolean equals(MyString1 obj);*/
public class MyString1 {
    private char arr[];

    public MyString1(char[] chars) {
        this.arr = new char[chars.length];
        System.arraycopy(chars, 0, arr, 0, chars.length);
    }

    public char charAt(int index) {
        return arr[index];
    }

    public int length() {
        return arr.length;
    }

    public MyString1 substring(int begin, int end) {
        char[] newArr = new char[end - begin];
        for (int i = 0, j = 0; i < newArr.length; i++, j++) {
            newArr[i] = arr[begin + j];
        }
        return new MyString1(newArr);
    }

    public MyString1 toLowerCase() {
        char[] newArr = new char[arr.length];
        for (int i = 0; i < arr.length; i++) {
            newArr[i] = Character.toLowerCase(arr[i]);
        }
        return new MyString1(newArr);
    }

    public static MyString1 valueOf(int i) {
        int num = i;
        int counter = 0;
        while (num > 0) {
            counter++;
            num /= 10;
        }
        char[] newArr = new char[counter];
        for (int j = newArr.length - 1; j >= 0; j--) {
            newArr[j] = (char) (i % 10 + 48);
            i = i / 10;
        }
        return new MyString1(newArr);
    }

    public char[] toChars() {
        return arr;
    }

    public boolean equals(MyString1 obj) {
        return obj != null && Arrays.equals(arr, obj.toChars());
    }
}
