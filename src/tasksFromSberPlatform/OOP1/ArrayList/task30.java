package tasksFromSberPlatform.OOP1.ArrayList;

import java.util.*;

/*      Напишите метод, который возвращает объединение двух целочисленных списков типа ArrayList, используя следующий заголовок:

        public static ArrayList<Integer> union(ArrayList<Integer> list1, ArrayList<Integer> list2)
        Например, объединение двух списков {2, 3, 1, 5} и {3, 4, 6} равно {2, 3, 1, 5, 3, 4, 6}.
        Напишите тестовую программу, которая запрашивает у пользователя два списка, каждый с пятью целыми числами,
        и отображает результат их объединения. Числа должны быть отделены друг от друга только одним пробелом.
        Пример выполнения программы:

        Введите пять целых чисел для списка1: 3 5 45 4 3
        Введите пять целых чисел для списка2: 33 51 5 4 13
        Объединенный список равен 3 5 45 4 3 33 51 5 4 13*/
public class task30 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        ArrayList<Integer> list1 = new ArrayList<>();
        ArrayList<Integer> list2 = new ArrayList<>();

        System.out.println("Задаем первый список");
        for (int i = 0; i < 5; i++) {
            System.out.println("Введите чило номер " + (i + 1));
            list1.add(scan.nextInt());
        }
        System.out.println("Задаем второй список");
        for (int i = 0; i < 5; i++) {
            System.out.println("Введите чило номер " + (i + 1));
            list2.add(scan.nextInt());
        }

        System.out.println("Объединенный список равен" + "\n");
        print(union(list1, list2));
    }

    public static ArrayList<Integer> union(ArrayList<Integer> list1, ArrayList<Integer> list2) {
        ArrayList<Integer> list3 = new ArrayList<>();
        for (Integer integer : list1) {
            list3.add(integer);
        }
        for (Integer integer : list2) {
            list3.add(integer);
        }
        return list3;
    }

    public static void print(ArrayList<Integer> list) {
        for (Integer integer : list) {
            System.out.print(integer + " ");
        }
    }
}
