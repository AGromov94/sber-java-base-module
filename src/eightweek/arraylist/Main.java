package eightweek.arraylist;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Car> cars = new ArrayList<>();
        cars.add(new Car("BMW","1990"));
        cars.add(new Car("Audi","1991"));
        cars.add(new Car("Volvo","2000"));
        cars.add(new Car("Volvo","2000"));
        cars.add(new Car("Mazda","2005"));

        for (Car car: cars
             ) {
            System.out.println(car.toString());
        }

        //cars.forEach(car -> System.out.println(car)); то же самое что строчка ниже
        cars.forEach(System.out::println); //лямбда выражение

        //удаление с помощью итератора
        Iterator<Car> iterator = cars.iterator();
        while (iterator.hasNext()){
            Car car = iterator.next();
            if(car.getModel().equalsIgnoreCase("mazda")){
                iterator.remove();
            }
        }
    }
}
