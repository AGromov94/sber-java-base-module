package firstweek;

import java.util.Scanner;

//Дана последовательность символов,
//        конкатенировать их в одну строку и вывести эту строку, исключая цифры.
//        На вход подаются заглавные или строчные символы английского алфавита или цифры.
//        Scanner input = new Scanner(System.in);
//        String a1 = input.next();
//        String a2 = input.next();
//        String a3 = input.next();
//        String a4 = input.next();
//        String a5 = input.next();
//
//        Входные данные
//        H 1 9 i 4
//        Выходные данные
//        Hi
public class TestClass {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        replaceNumbers(n);
    }

    public static void replaceNumbers(int n) {
        if (n >= 10) {
            System.out.print(n%10);
            replaceNumbers(n / 10);
        } else {
            System.out.print(n);
        }
    }

//    public static long fact(long number) {
////        while (number > 1) {
////            result *= number;
////            number--;
////        }
////        return result;
//        if (number == 1) {
//            return 1;
//        } else {
//            return number * fact(number - 1);
//        }
}
