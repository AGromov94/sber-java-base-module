package tasksFromSberPlatform.OOP1.task9;

import java.util.Calendar;
import java.util.GregorianCalendar;

/*      Создайте класс с именем MyDate для представления даты. Класс MyDate должен содержать:

        Поля данных year, month и day, которые представляют дату. Поле month должно быть основано на числе, т.е. 0 — для января,
        1 — для февраля и т.д.
        Безаргументный конструктор, который создает объект типа MyDate для текущей даты.
        Конструктор, который создает объект типа MyDate с указанным временем в миллисекундах, прошедших с 00:00, 1 января 1970 г.
        Конструктор, который создает объект типа MyDate с указанными годом, месяцем и днем.
        Три getter-метода для полей данных year, month и day, соответственно.
        Метод с именем setDate(long elapsedTime), который присваивает новую дату объекту, используя прошедшее время.
        Нарисуйте UML-диаграмму класса MyDate, а затем реализуйте этот класс. Напишите клиент этого класса — программу,
        которая создает два объекта типа MyDate (с помощью new MyDate() и new MyDate(34355555133101L)) и отображает их год, месяц и день.

        (Подсказка: первые два конструктора извлекут год, месяц и день из прошедшего времени. Например,
        если прошедшее время составляет 561555550000 миллисекунд, то год равен 1987, месяц равен 9, а день равен 18.
        Для упрощения кодирования можно использовать класс GregorianCalendar, описанный в подразделе «Задания. Часть 2» этого курса.)*/
public class MyDate {
    private int year;
    private int month;
    private int day;

    public MyDate(long milliseconds) {
        setDate(milliseconds);
    }

    public MyDate() {
        setDate(System.currentTimeMillis());
    }

    public MyDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public void setDate(long elapsedTime) {
        //вспомогательный календарь для определения текущего года
        GregorianCalendar currentCalendar = new GregorianCalendar();
        currentCalendar.setTimeInMillis(System.currentTimeMillis());
        int currentYear = currentCalendar.get(GregorianCalendar.YEAR);

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(elapsedTime);
        this.year = calendar.get(GregorianCalendar.YEAR);
        this.month = (calendar.get(GregorianCalendar.YEAR) < currentYear ? calendar.get(GregorianCalendar.MONTH) : calendar.get(GregorianCalendar.MONTH) + 1);
        this.day = calendar.get(GregorianCalendar.DAY_OF_MONTH);
    }

    public void printDate() {
        System.out.println("Год: " + year + "\n" + "Месяц: " + (month < 10 ? "0" + month : month) + "\n" + "День: " + (day < 10 ? "0" + day : day));
        System.out.println();
    }
}
