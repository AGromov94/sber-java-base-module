package tasksFromSberPlatform.OOP1.task9;

public class MyCalendarClient {
    public static void main(String[] args) {
        MyDate firstDate = new MyDate();
        MyDate secondDate = new MyDate(34355555133101L);
        MyDate thirdDate = new MyDate(561555550000L);
        //firstDate.setDate(34355555133101L);

        firstDate.printDate();
        secondDate.printDate();
        thirdDate.printDate();
    }
}
