package homeworks.homework6.task7;

/*7.	Реализовать класс TriangleChecker, статический метод которого принимает три длины сторон треугольника и возвращает true,
        если возможно составить из них треугольник, иначе false. Входные длины сторон треугольника — числа типа double.
        Придумать и написать в методе main несколько тестов для проверки работоспособности класса
        (минимум один тест на результат true и один на результат false)*/
public class TriangleChecker {
    public static boolean checkingExistingOfTriangle(double a, double b, double c) {
        if (!(a + b > c && a + c > b && b + c > a && a > 0 && b > 0 && c > 0)) {
            return false;
        }
        return true;
    }

    private TriangleChecker() {
    }
}
