package secondweek;

import java.util.*;

/*
Даны три целых числа a, b, c.
Проверить есть ли среди них прямо противоположные.
( 5 и -5 прямо противоположные числа).
0 и 0 не считать прямо противоположными.
Входные данные
-1 1 0
Выходные данные
true
Входные данные
-2 1 0
Выходные данные
false
 */
public class Task5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();
        int c = scan.nextInt();

        System.out.println(a == -b && a != 0 || a == -c && c != 0 || b == -c && b != 0);
    }
}
