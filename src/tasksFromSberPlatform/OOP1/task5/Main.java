package tasksFromSberPlatform.OOP1.task5;

public class Main {
    public static void main(String[] args) {
        double[][] arr = {{23.5, 35, 2, 10}, {4.5, 3, 45, 3.5}, {35, 44, 5.5, 9.6}};
        Location location = Location.locateOfLargestElement(arr);

        System.out.println("Наибольший элемент массива, равный " + location.maxValue + ", находится в позиции (" + location.row + "," + location.column + ")");

        for (int i = 0; i < 100; i++) {
            location.value++;
        }
        System.out.println(location.value);
    }
}
