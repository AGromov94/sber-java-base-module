package tasksFromSberPlatform.OOP1.task3;

public class TestException1 {
    public static void main(String[] args) {
        try {
            int[] list = new int[10];
            System.out.println("list[10] равно " + list[10]);
        }
        catch (ArithmeticException ex) {
            System.out.println("ArithmeticException");
        }
        catch (RuntimeException ex) {
            System.out.println("RuntimeException");
        }
        catch (Exception ex) {
            System.out.println("Exception");
        }
    }
}
