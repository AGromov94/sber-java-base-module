package homeworks.homework8.task2;

/*2.	Цех по ремонту BestCarpenterEver умеет чинить некоторую Мебель. К сожалению, из Мебели он умеет чинить только Табуретки,
        а Столы, например, нет. Реализовать метод в цеху, позволяющий по переданной мебели определять, сможет ли ей починить или нет.
        Возвращать результат типа boolean. Протестировать метод.*/
public class BestCarpenterEver {
    public boolean checkToRepair(Furniture furniture) {
        if (furniture instanceof Stool) {
            return true;
        } else {
            return false;
        }
    }
}
