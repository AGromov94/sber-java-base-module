package tasksFromSberPlatform.OOP1.task1;

public class Main {
    public static void main(String[] args) {
        Rectangle firstRectangle = new Rectangle(4, 40);
        Rectangle secondRectangle = new Rectangle(3.5, 35.9);

        System.out.println("№ приямоугольника \t" + "Ширина \t" + "Высота \t" + "Площадь \t" + "Периметр \t");
        System.out.println("Первый прямоугольник: " + firstRectangle.width + " \t" + firstRectangle.height + " \t" + firstRectangle.getArea() + " \t\t" + firstRectangle.getPerimeter());
        System.out.println("Второй прямоугольник: " + secondRectangle.width + " \t" + secondRectangle.height + " \t" + secondRectangle.getArea() + " \t\t" + secondRectangle.getPerimeter());
    }
}
