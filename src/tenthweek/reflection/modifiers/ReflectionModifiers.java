package tenthweek.reflection.modifiers;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class ReflectionModifiers {
    public static void main(String[] args) {

    }

    public static void printAllFields(Class<?> clazz, Task4 task4) {
        for (Field field : clazz.getDeclaredFields()) {
            int mods = field.getModifiers();
            if (Modifier.isPublic(mods)) {
                System.out.println(" public ");
            }
            if (Modifier.isProtected(mods)) {
                System.out.println(" protected ");
            }
            if (Modifier.isPrivate(mods)) {
                System.out.println(" private ");
                field.setAccessible(true);
            }
            if (Modifier.isStatic(mods)) {
                System.out.println(" static ");
            }
            if (Modifier.isFinal(mods)) {
                System.out.println(" final ");
            }
        }
    }
}
