package homeworks.homework5;

import java.util.*;
/*10.	На вход подается число N. Необходимо вывести цифры числа справа налево. Решить задачу нужно через рекурсию.

        Ограничения:
        0 < N < 1000000*/

public class Task10 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        returnLastNumber(n);
    }

    public static void returnLastNumber(int n) {
        if (n < 10) {
            System.out.print(n);
        } else {
            System.out.print(n % 10 + " ");
            returnLastNumber(n / 10);
        }
    }
}
