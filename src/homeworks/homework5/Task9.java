package homeworks.homework5;

import java.util.*;
/*9.	На вход подается число N. Необходимо вывести цифры числа слева направо. Решить задачу нужно через рекурсию.

        Ограничения:
        0 < N < 1000000*/

public class Task9 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int x = 0;
        int y = 1;

        recursivePrintDigits(n, x, y);
    }

    public static void recursivePrintDigits(int n, int x, int y) {
        System.out.print(Integer.toString(n).substring(x, y) + " ");
        if (y < Integer.toString(n).length()) {
            recursivePrintDigits(n, ++x, ++y);
        }
    }
}
