package fifthweek;

import java.util.Random;
import java.util.Scanner;

/*
Петя со своей подругой хочет купить два билета в кино рядом.
Необходимо вывести на экран предзаполненные места кинотеатра и после этого проверить,
найдутся ли подходящие места для Пети и его подруги.
Подходящими местами являются два свободных места рядом в одном ряду.

На вход передается N — количество мест в одном ряду кинотеатра и M — количество рядов.
Необходимо заполнить кинотеатр размера N на M случайным заполнением (0 — свободное место, 1 — занятое).

Входные данные
3 3
Выходные данные
0 0 1
1 1 0
0 0 1
0 1 0
true

Входные данные
2 2
Выходные данные
1 1
1 1
false
     */
public class Task8 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int m = scan.nextInt();
        int[][] matrix = new int[n][m];
        boolean answer = false;
        Random randomizer = new Random();

        //заполняем массив случайным числом от 0 до 1
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                matrix[i][j] = randomizer.nextInt(2);
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                System.out.print("[" + matrix[i][j] + "]");
            }
            System.out.println();
        }
        for (int i = 0; i < n; i++) {
            if (!answer) {
                for (int j = 0; j < m - 1; j++) {
                    if (matrix[i][j] == 0 && matrix[i][j + 1] == 0) {
                        answer = true;
                        break;
                    }
                }
            }
        }
        System.out.println(answer);
    }
}
