package homeworks.homework3;

import java.util.*;

/*6. В банкомате остались только купюры номиналом 8 4 2 1. Дано положительное
        число n - количество денег для размена. Необходимо найти минимальное
        количество купюр с помощью которых можно разменять это количество денег
        (соблюсти порядок: первым числом вывести количество купюр номиналом 8,
        вторым - 4 и т д)
        Ограничения:
        0 < n < 1000000*/
public class Task6 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        for (int i = 8; i >= 1; i /= 2) {
            System.out.print(n / i + " ");
            n %= i;
        }
    }
}
