package fourthweek;

import java.util.*;

/*
  На вход подается число N - длина массива.
     Затем передается массив строк длины N.
     После этого - число M.

     Сохранить в другом массиве только те элементы, длина строки которых
     не превышает M.

     Входные данные:
     5
     Hello
     good
     to
     see
     you
     4

     Выходные данные:
     good to see you
 */
public class Task6 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        String[] arr = new String[n];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = scan.next();
        }

        int m = scan.nextInt();
        int k = 0;
        String[] result = new String[n];
        for (String s : arr
        ) {
            if (s.length() <= m) {
                result[k++] = s;
            }
        }
        System.out.println(Arrays.toString(result));
    }
}
