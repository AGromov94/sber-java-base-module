package homeworks.homework6.task4;
/*4.	Необходимо реализовать класс TimeUnit с функционалом, описанным ниже (необходимые поля продумать самостоятельно).
        Обязательно должны быть реализованы валидации на входные параметры.
        Конструкторы:
        ●	Возможность создать TimeUnit, задав часы, минуты и секунды.
        ●	Возможность создать TimeUnit, задав часы и минуты. Секунды тогда должны проставиться нулевыми.
        ●	Возможность создать TimeUnit, задав часы. Минуты и секунды тогда должны проставиться нулевыми.
        Публичные методы:
        ●	Вывести на экран установленное в классе время в формате hh:mm:ss
        ●	Вывести на экран установленное в классе время в 12-часовом формате (используя hh:mm:ss am/pm)
        ●	Метод, который прибавляет переданное время к установленному в TimeUnit (на вход передаются только часы, минуты и секунды).*/

public class TimeUnit {
    public int hours;
    public int minutes;
    public int seconds;
    int[] timeArray;

    public TimeUnit(int hours, int minutes, int seconds) {
        this.hours = validationOfInputForHours(hours);
        this.minutes = validationOfInputForMinutesAndSeconds(minutes);
        this.seconds = validationOfInputForMinutesAndSeconds(seconds);
        timeArray = new int[]{hours, minutes, seconds};
    }

    public TimeUnit(int hours, int minutes) {
        this.hours = validationOfInputForHours(hours);
        this.minutes = validationOfInputForMinutesAndSeconds(minutes);
        this.seconds = 0;
        timeArray = new int[]{hours, minutes, seconds};
    }

    public TimeUnit(int hours) {
        this.hours = validationOfInputForHours(hours);
        this.minutes = 0;
        this.seconds = 0;
        timeArray = new int[]{hours, minutes, seconds};
    }

    public void printCurrentTime() {
        for (int i = 0; i < timeArray.length; i++) {
            if (timeArray[i] < 10) {
                System.out.print("0" + timeArray[i]);
            } else {
                System.out.print(timeArray[i]);
            }
            if (i < timeArray.length - 1) {
                System.out.print(":");
            }
        }
    }

    public void printCurrentTimeInTwelveHoursFormat() {
        String halfADay = "AM";
        if (timeArray[0] > 12) {
            timeArray[0] -= 12;
            halfADay = "PM";
        }
        printCurrentTime();
        System.out.print(" " + halfADay);
    }

    public void timeAdditionAndPrint(int inputHours, int inputMinutes, int inputSeconds) {
        timeArray[2] += inputSeconds;
        if (timeArray[2] >= 60) {
            timeArray[2] %= 60;
            timeArray[1]++;
        }
        timeArray[1] += inputMinutes;
        if (timeArray[1] >= 60) {
            timeArray[1] %= 60;
            timeArray[0]++;
        }
        timeArray[0] += inputHours;
        if (timeArray[0] >= 24) {
            timeArray[0] = 0;
        }
    }

    public int validationOfInputForMinutesAndSeconds(int time) throws RuntimeException {
        if (time >= 0 && time < 60) {
            return time;
        } else {
            throw new RuntimeException();
        }
    }

    public int validationOfInputForHours(int time) throws RuntimeException {
        if (time >= 0 && time < 24) {
            return time;
        } else {
            throw new RuntimeException();
        }
    }
}
