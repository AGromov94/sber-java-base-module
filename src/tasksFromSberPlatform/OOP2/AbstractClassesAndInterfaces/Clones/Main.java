package tasksFromSberPlatform.OOP2.AbstractClassesAndInterfaces.Clones;

import java.util.ArrayList;
//клонирование объекта типа ArrayList
public class Main {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("Новосибирск");
        ArrayList<String> list1 = list;
        ArrayList<String> list2 = (ArrayList<String>)(list.clone());
        list.add("Астрахань");
        System.out.println(list == list1);
        System.out.println(list == list2);
        System.out.println("list равно " + list);
        System.out.println("list1 равно " + list1);
        System.out.println("list2.get(0) равно " + list2.get(0));
        System.out.println("list2.size() равно " + list2.size());
    }
}
