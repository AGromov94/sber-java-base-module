package homeworks.homework9.task4;

public class IllegalCreateEvenNumberException extends Exception {
    private int number;

    public IllegalCreateEvenNumberException(int number) {
        this.number = number;
    }

    public IllegalCreateEvenNumberException(String message, int number) {
        super(message);
        this.number = number;
    }

    public int getNumber() {
        return number;
    }
}
