package ninethweek.generics.task4;

public class Main {
    public static void main(String[] args) {
        String str1 = "TopJava";
        String str2 = "TopJava";
        //если строка задана не через конструктор (не через new), а литералом, то она хранится в куче в пуле строк и
        //сравнивается == как примитив.
        System.out.println(str1 == str2); //true

        str1 = "TopJava";
        str2 = "Top" + "Java";
        System.out.println(str1 == str2); //true

        str1 = "TopJava";
        str2 = "Top" + "Java";
        String str3 = "Top" + str2;
        System.out.println(str1 == str3); //false

        str1 = "TopJava";
        str2 = "Top" + "Java";
        str3 = "Top" + str2;
        String str4 = new String("TopJava");
        String str5 = new String("TopJava").intern(); //помещаем строку в стринг пул руками
        System.out.println(str1 == str4); //false
        System.out.println(str1 == str5); //true

    }
}
