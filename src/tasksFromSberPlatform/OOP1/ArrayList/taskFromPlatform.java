package tasksFromSberPlatform.OOP1.ArrayList;

import java.util.*;

/*      Напишите фрагменты кода для выполнения следующих операций:

        1. Создайте ArrayList для хранения значений типа double.

        2. Добавьте в ArrayList новый объект.

        3. Вставьте новый объект в начало ArrayList.

        4. Найдите количество объектов в ArrayList.

        5. Удалите указанный объект из ArrayList.

        6. Удалите последний объект из ArrayList.

        7. Проверьте, находится ли указанный объект в ArrayList.

        8. Извлеките объект с указанным индексом из ArrayList.*/
public class taskFromPlatform {
    public static void main(String[] args) {
        ArrayList<Double> list = new ArrayList<>();
        list.add(2.0);
        list.add(3.0);
        list.add(0, 1.0);
        System.out.println(list.size()); //3
        list.remove(1.0);
        list.remove(list.size()-1);
        System.out.println(list.contains(2.0)); //true
        System.out.println(list.get(0)); // 2.0

        int count = 2;
        int value = switch (count) {
            case 1 -> 12;
            case 2 -> 32;
            case 3 -> 52;
            default -> 0;
        };
        System.out.println(value);
    }
}
