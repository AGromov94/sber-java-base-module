package homeworks.homework8.task1;

public class Main {
    public static void main(String[] args) {
        Eagle eagle = new Eagle();
        eagle.sleep();
        eagle.eat();
        eagle.wayOfBirth();
        eagle.fly();

        GoldFish goldfish = new GoldFish();
        goldfish.sleep();
        goldfish.sleep();
        goldfish.wayOfBirth();
        goldfish.swim();
    }
}
