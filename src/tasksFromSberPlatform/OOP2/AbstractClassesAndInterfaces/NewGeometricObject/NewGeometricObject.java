package tasksFromSberPlatform.OOP2.AbstractClassesAndInterfaces.NewGeometricObject;

import java.util.Date;

public abstract class NewGeometricObject implements Comparable<NewGeometricObject>, ThreeMore {
    private String color = "белый";
    private boolean filled;
    private java.util.Date dateCreated;

    protected NewGeometricObject(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
        this.dateCreated = new Date();
    }

    public NewGeometricObject() {
        this.dateCreated = new Date();
    }

    public static NewGeometricObject max(NewGeometricObject o1, NewGeometricObject o2) {
        return o1.compareTo(o2) > 0 ? o1 : o1.compareTo(o2) < 0 ? o2 : null;
    }

    /**
     * Возвращает цвет
     */
    public String getColor() {
        return color;
    }

    /**
     * Присваивает новый цвет
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * Возвращает заливку. Поскольку filled типа boolean,
     * getter-метод называется isFilled
     */
    public boolean isFilled() {
        return filled;
    }

    /**
     * Присваивает новую заливку
     */
    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    /**
     * Получает dateCreated
     */
    public java.util.Date getDateCreated() {
        return dateCreated;
    }

    public abstract double getArea();

    public abstract double getPerimeter();

    /**
     * Возвращает строковое представление этого объекта
     */
    @Override
    public String toString() {
        return "создан " + dateCreated + ",\nцвет: " + color +
                ", заливка: " + filled + "\n";
    }

    @Override
    public int compareTo(NewGeometricObject o) {
        return Double.compare(this.getArea(), o.getArea());
    }

    @Override
    public boolean equals(Object obj) {
        return this.getArea() == ((NewGeometricObject) obj).getArea();
    }
}
