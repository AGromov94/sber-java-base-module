package tenthweek.reflection.classname;

public class ReflectionClassName {
    public static void main(String[] args) {
        printNameForClass(String.class, "String.class");
        printNameForClass(new java.io.Serializable() {}.getClass(), "new java.io.Serializable() {}.getClass()");
    }

    public static void printNameForClass(Class<?> clazz, String label){
        System.out.println(label + ": ");
        System.out.println("getName(): " + clazz.getName());
        System.out.println("getSimpleName(): " + clazz.getSimpleName());
        System.out.println("getPackage(): " + clazz.getPackage());
        System.out.println("getType(): " + clazz.getTypeName());
        System.out.println();
    }
}
