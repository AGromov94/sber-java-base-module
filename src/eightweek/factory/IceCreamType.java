package eightweek.factory;

public enum IceCreamType {
    CHERRY, CHOLOCATE, VANILLA;
}
