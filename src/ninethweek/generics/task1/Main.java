package ninethweek.generics.task1;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        for (int i = 0; i < -1; i++) {

        }
 /*     Pair<String, Number> pair = new Pair<>();
        pair.first = "string1";
        pair.second = 23;
        pair.first = "1234";
        pair.second = 1;
        pair.print();

        Pair<String, Integer> pair1 = new Pair<>();
        pair1.first = "4321";
        pair1.second = 2;
        pair1.print();*/
    }

    static boolean isAnagram(String a, String b) {
        char[] arr1 = a.toLowerCase().toCharArray();
        char[] arr2 = b.toLowerCase().toCharArray();
        arr1 = arraySort(arr1);
        arr2 = arraySort(arr2);

        if (arr1.length != arr2.length) {
            return false;
        }

        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
                return false;
            }
        }
        return true;
    }

    static char[] arraySort(char[] arr) {
        char[] sortedArr = new char[arr.length];
        for (int i = 0; i < arr.length; i++) {
            sortedArr[i] = arr[i];
        }
        char temp;
        for (int i = sortedArr.length - 1; i <= 1; i--) {
            for (int j = 0; j < i; j++) {
                if (sortedArr[j] > sortedArr[j + 1]) {
                    temp = sortedArr[j];
                    sortedArr[j] = sortedArr[j + 1];
                    sortedArr[j + 1] = temp;
                }
            }
        }
        return sortedArr;
    }
}
