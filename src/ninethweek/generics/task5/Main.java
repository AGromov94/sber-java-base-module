package ninethweek.generics.task5;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

/*с собеседования
подается строка из маленьких латинских символов.
нужно проверить что в строке встречаются все символы латинского алфавита хотя бы раз*/
public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str = scan.nextLine();

        System.out.println(checkString(str));
    }
    /**
    Решение с использованием Set
     */
    public static boolean checkString(String input){
        if(input.length() < 26){
            return false;
        }
        Set<Character> characters = new TreeSet<>(); //TreeSet - отсортированный список!
        for(char  ch: input.toCharArray()){
            characters.add(ch);
        }
        return characters.size() == 26;
    }
    /**
     Старое доброе решение с циклом for
     */
    public static boolean check(String str) {
        if (str.length() < 26) {
            return false;
        }
        char[] arr = str.toCharArray();
        for (char i = 'a'; i <= 'z'; i++) {
            if (str.indexOf(i) < 0) {
                return false;
            }
        }
        return true;
    }
}
