package seventhweek.task3;

import java.io.*;
import java.util.*;

public class FileWorker {
    private static final String FILE_DIRECTORY = "C:\\Users\\User\\IdeaProjects\\SberJava\\src\\seventhweek\\task3\\files";

    private static final String FILE_NAME = "";

    private static final String OUTPUT_FILE_NAME = "output.txt";

    private FileWorker() {

    }

    public static void readAndWriteFromFile(String filePath) throws IOException {
        Scanner scan = new Scanner(new File(filePath));
        String[] days = new String[10];
        int i = 0;
        while (scan.hasNextLine()) {
            days[i++] = scan.nextLine();
        }
        Writer writer = new FileWriter(FILE_DIRECTORY + "\\" + OUTPUT_FILE_NAME);
        for (int j = 0; j < i; j++) {
            String result = "Порядковый номер дня недели " + days[j] + " = " + WeekDays.ofName(days[j]).dayNumber + "\n";
            writer.write(result);
        }
        writer.close();
        scan.close();
    }
}
