package tasksFromSberPlatform.OOP2.Generics;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/*      Создайте кортеж с тремя элементами одного типа Triplet<A>, где кортеж – это неизменяемый упорядоченный набор объектов.
        В зависимости от количества объектов, которые ассоциирует кортеж, он имеет название пара (2 элемента), триплет (3 элемента) и т.д.
        Типы элементов кортежа могут отличаться, но должны быть определены и контролироваться. Контроль типа необходимо реализовать с помощью дженериков.
        Также необходимо определить следующие методы:

        проверки наличия объекта в кортеже;
        определения наименьшего элемента в кортеже;
        сложения элементов кортежа (если это числа, то результатом сложения будет их математическая сумма,
        иначе – строковые представления элементов через пробел).*/
public class Triplet<A extends Comparable<A>> {
    private A firstElement;
    private A secondElement;
    private A thirdElement;
    private List<A> list;

    public Triplet(A firstElement, A secondElement, A thirdElement) {
        this.firstElement = firstElement;
        this.secondElement = secondElement;
        this.thirdElement = thirdElement;
        this.list = Arrays.asList(firstElement, secondElement, thirdElement); //возвращает ArrayList!!!
    }

    //region get/set
    public A getFirstElement() {
        return firstElement;
    }

    public void setFirstElement(A firstElement) {
        this.firstElement = firstElement;
    }

    public A getSecondElement() {
        return secondElement;
    }

    public void setSecondElement(A secondElement) {
        this.secondElement = secondElement;
    }

    public A getThirdElement() {
        return thirdElement;
    }

    public void setThirdElement(A thirdElement) {
        this.thirdElement = thirdElement;
    }

    public List<A> getList() {
        return list;
    }

    public void setList(List<A> list) {
        this.list = list;
    }
    //endregion

    public final boolean containsElement(A a) {
        return list.contains(a);
    }

    public final A min() {
        return Collections.min(list);
    }

    public final String sum() {
        if (firstElement instanceof Number && secondElement instanceof Number && thirdElement instanceof Number) {
            return ((Number) firstElement).doubleValue() + ((Number) secondElement).doubleValue() + ((Number) thirdElement).doubleValue() + "";
        }
        return //firstElement.toString() + " " + secondElement.toString() + " " + thirdElement.toString();
                String.join(" ", firstElement.toString(), secondElement.toString(), thirdElement.toString()); //то же что и строка выше
    }

    @Override
    public String toString() {
        return list.toString();
    }
}
