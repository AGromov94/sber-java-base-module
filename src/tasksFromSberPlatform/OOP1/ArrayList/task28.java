package tasksFromSberPlatform.OOP1.ArrayList;

import java.util.*;

/*      Напишите следующий метод, который возвращает сумму всех чисел в списке типа ArrayList:

        public static double sum(ArrayList<Double> list)
        Напишите тестовую программу, которая запрашивает у пользователя пять чисел, сохраняет их в списке и отображает их сумму.*/
public class task28 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        ArrayList<Double> list = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            System.out.println("Введите чило номер " + (i + 1));
            list.add(scan.nextDouble());
        }

        System.out.println(sum(list));
    }

    public static double sum(ArrayList<Double> list) {
        double sum = 0;
        for (Double a : list) {
            sum += a;
        }
        return sum;
    }
}
