package homeworks.homework10.task3;

/*      Задача: с помощью рефлексии вызвать метод print() и обработать все возможные ошибки
        (в качестве аргумента передавать любое подходящее число).
        При “ловле” исключений выводить на экран краткое описание ошибки.*/
public class APrinter {
    public void print(int a) {
        System.out.println(a);
    }
}

