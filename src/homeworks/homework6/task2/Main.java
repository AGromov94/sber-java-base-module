package homeworks.homework6.task2;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Student petr = new Student();
        int[] grades = {3, 5, 5, 4, 2, 5, 4, 4, 4, 3};
        petr.setName("Petr");
        petr.setSurname("Ivanov");
        petr.setGrades(grades);

        System.out.println((petr.returnAverageOfMarks(petr.getGrades())));
        System.out.println(Arrays.toString(petr.getGrades()));
        petr.setNewMarkIntoGradesArray(1, petr.getGrades());
        System.out.println(Arrays.toString(petr.getGrades()));
    }
}
