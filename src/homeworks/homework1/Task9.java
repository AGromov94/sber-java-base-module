package homeworks.homework1;

import java.util.*;
/*На вход подается бюджет мероприятия – n тугриков. Бюджет на одного гостя – k тугриков. Вычислите и выведите, сколько гостей можно пригласить на мероприятие.

        Ограничения:
        0 < n < 100000
        0 < k < 1000
        k < n*/

public class Task9 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int budget = scan.nextInt();
        int amountOfGuests = scan.nextInt();

        System.out.println(budget / amountOfGuests);
    }
}
