package homeworks.homework1;

import java.util.*;
/*Переведите дюймы в сантиметры (1 дюйм = 2,54 сантиметров). На вход подается количество дюймов, выведите количество сантиметров.

        Ограничения:
        0 < count < 1000*/

public class Task5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int inch = scan.nextInt();

        System.out.println(2.54 * inch);
    }
}
