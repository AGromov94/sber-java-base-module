package tasksFromSberPlatform.OOP2.Generics.task22;

import java.util.ArrayList;

/*      Класс GenericStack из описания предыдущего задания реализован с помощью отношения композиции.
        Определите новый класс стека, который наследуется от ArrayList. Нарисуйте UML-диаграмму этих классов,
        а затем реализуйте новый класс GenericStack. Напишите тестовую программу, которая запросит у пользователя пять строк,
        а отобразит их в обратном порядке.*/
public class NewGenericStack<E> extends ArrayList<E> {

    public int getSize() {
        return super.size();
    }

    @Override
    public boolean isEmpty() {
        return super.isEmpty();
    }

    public E peek() {
        return (E) super.get(size() - 1);
    }

    public void push(E o) {
        super.add(o);
    }

    public E pop() {
        E o = (E) super.get(size() - 1);
        super.remove(size() - 1);
        return o;
    }

    public int search(E o) {
        return super.indexOf(o);
    }

    @Override
    public String toString() {
        return "Наш стек:" + super.toString();
    }
}
