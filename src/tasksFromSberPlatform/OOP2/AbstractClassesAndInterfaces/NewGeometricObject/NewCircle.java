package tasksFromSberPlatform.OOP2.AbstractClassesAndInterfaces.NewGeometricObject;

/*      Определите класс с именем ComparableCircle, который наследуется от Circle и реализует Comparable.
        Нарисуйте UML-диаграмму и реализуйте метод compareTo() для сравнения кругов на основе их площади.
        Напишите тестовую программу, чтобы найти наибольший из двух экземпляров класса ComparableCircle и
        наибольший между кругом и прямоугольником.*/
public class NewCircle extends NewGeometricObject {
    private double radius = 1.0;

    public NewCircle(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public NewCircle(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return Math.PI * Math.pow(radius, 2);
    }

    @Override
    public double getPerimeter() {
        return 4 * Math.PI * radius;
    }

    @Override
    public String toString() {
        return super.toString() + "Радиус круга равен = " + radius;
    }

    @Override
    public boolean equals(Object obj) {
        return this.radius == ((NewCircle) obj).getRadius();
    }
}
