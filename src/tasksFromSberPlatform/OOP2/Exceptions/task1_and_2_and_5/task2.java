package tasksFromSberPlatform.OOP2.Exceptions.task1_and_2_and_5;

import java.util.InputMismatchException;
import java.util.Scanner;

/*Программа из задания №1 нормально работает до тех пор, пока пользователь вводит целое число.
  В противном случае вы можете получить другой тип исключения. Например, если вы используете метод nextInt() класса Scanner,
  то у вас может произойти InputMismatchException. Измените программу таким образом, чтобы предотвратить ввод пользователем любого числа,
  кроме целого.*/
public class task2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[] months = {"январь", "февраль", "март", "апрель", "май",
                "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"};

        int[] dom = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        while (true) {
            System.out.println("Введите число от 1 до 12");
            try {
                int number = scan.nextInt();
                System.out.println("Месяц: " + months[number - 1] + "\n" + "Кол-во дней: " + dom[number - 1]);
            } catch (IndexOutOfBoundsException e) {
                System.out.println("Недопустимое число");
            } catch (InputMismatchException e) {
                System.out.println("Введите целое число!");
                scan.nextLine();
            }
        }
    }
}
