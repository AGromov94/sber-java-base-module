package sixthweek.task1;

public class Main {
    public static void main(String[] args) {
        Bulb bulb = new Bulb(true);
        bulb.turnOff();
        System.out.println(bulb.isShining());

        Chandelier chandelier = new Chandelier(4);
        chandelier.turnOn();
    }
}
