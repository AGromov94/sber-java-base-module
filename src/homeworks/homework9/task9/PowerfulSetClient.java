package homeworks.homework9.task9;

import java.util.HashSet;
import java.util.Set;

public class PowerfulSetClient {
    public static void main(String[] args) {
        PowerfulSet powerfulSet = new PowerfulSet();
        HashSet<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);
        HashSet<Integer> set2 = new HashSet<>();
        set2.add(0);
        set2.add(1);
        set2.add(2);
        set2.add(4);

        System.out.println(powerfulSet.intersection(set1, set2));
        System.out.println(powerfulSet.union(set1, set2));
        System.out.println(powerfulSet.relativeComplement(set1, set2));
    }
}
