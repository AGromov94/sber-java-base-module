package tasksFromSberPlatform.OOP1.project;

import java.util.*;

/*      Используя уже созданный класс Account, смоделируйте работу банкомата. Создайте 10 банковских счетов в массиве с идентификаторами
        (далее — id) 0, 1, …, 9 и начальным балансом 10 000 рублей. Система запрашивает у пользователя ввести id.
        Если введен некорректный id, то попросите пользователя ввести корректный id. После получения корректного id отображается главное меню,
        как показано в примере запуска. Можно ввести пункт меню 1 для просмотра текущего баланса, 2 — для снятия денег со счета,
        3 — для внесения денег на счет и 4 — для выхода из основного меню. После выхода система снова запрашивает id, таким образом,
        после запуска она не останавливается.*/
public class TestAccountATM {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Account[] accounts = new Account[10];
        int id;
        int number;
        for (int i = 0; i < accounts.length; i++) {
            accounts[i] = new Account(i, 10000);
        }

        while (true) {
            do {
                System.out.println("Введите id пользователя (от 1 до 9)");
                id = scan.nextInt();
            } while (id < 0 || id > 9);

            do {
                printMenu();
                System.out.println("Введите пункт меню:");
                number = scan.nextInt();
                while (number < 1 || number > 4) {
                    System.out.println("Введите корректный номер пункта меню");
                    number = scan.nextInt();
                }

                switch (number) {
                    case 1:
                        System.out.println(accounts[id].getBalance());
                        break;
                    case 2:
                        System.out.println("Введите сумму снятия:");
                        int sum = scan.nextInt();
                        accounts[id].withdraw(sum);
                        break;
                    case 3:
                        System.out.println("Введите сумму депозита:");
                        int sum1 = scan.nextInt();
                        accounts[id].deposit(sum1);
                        break;
                }
            } while (number != 4);
        }
    }

    public static void printMenu() {
        System.out.println("Основное меню:" + "\n" +
                "1: проверить баланс счета;" + "\n" +
                "2: снять со счета;" + "\n" +
                "3: положить на счет;" + "\n" +
                "4: выйти.");
    }
}
