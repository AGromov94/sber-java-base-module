package homeworks.homework6.task8;

public class Main {
    public static void main(String[] args) {
        Atm ATM1 = new Atm(CurrencyUnit.DOLLARS, 88.49);
        System.out.println(ATM1.fromDollarsToRoubles(100));
        Atm ATM2 = new Atm(CurrencyUnit.ROUBLES, 0.011);
        //Проверили что класс 2 раза инстанцировался
        System.out.println(Atm.returnCounterOfInstance());
    }
}