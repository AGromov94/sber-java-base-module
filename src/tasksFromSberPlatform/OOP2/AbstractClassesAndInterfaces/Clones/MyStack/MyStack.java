package tasksFromSberPlatform.OOP2.AbstractClassesAndInterfaces.Clones.MyStack;

import java.util.ArrayList;

//Перепишите класс MyStack из раздела «Наследование и полиморфизм» для выполнения глубокой копии поля списка.
public class MyStack implements Cloneable {
    private ArrayList<Object> list = new ArrayList<>();

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public int getSize() {
        return list.size();
    }

    public Object peek() {
        return list.get(getSize() - 1);
    }

    public Object pop() {
        Object o = list.get(getSize() - 1);
        list.remove(getSize() - 1);
        return o;
    }

    public void push(Object o) {
        list.add(o);
    }

    @Override
    /** Переопределяет метод toString класса Object */
    public String toString() {
        return "стек: " + list.toString();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        // Сделать поверхностную копию
        MyStack MyStackClone = (MyStack) super.clone(); //при поверхностной копии объекта, содержащего объектные поля (в данном случае list - типа ArrayList), копируются просто ссылки на них, а не  содержимое
        // Сделать глубокую копию whenBuilt
        MyStackClone.list = (ArrayList<Object>) (list.clone()); //при глубокой копии объекта, содержащего объектные поля (в данном случае list - типа ArrayList), копируется именно содержимое, а не ссылка
        return MyStackClone;

    }
}
