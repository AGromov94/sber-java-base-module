package homeworks.homework9.task6;

public class CheckException extends Exception {
    public CheckException() {
    }

    public CheckException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return "Check failed";
    }
}
