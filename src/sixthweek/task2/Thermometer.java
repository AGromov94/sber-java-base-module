package sixthweek.task2;

/*
Реализовать класс “Термометр”.
Необходимо иметь возможность создавать экземпляр класса с текущей температурой и
получать значение в фаренгейте и в цельсии.

currentTemperature * 1.8 + 32 - Фаренгейты
(currentTemperature - 32) / 1.8 - Цельсии
 */
public class Thermometer {
    private double tempCelsium;
    private double tempFahrenheit;

    public Thermometer(double currentTemperature, TemperatureUnit temperatureUnit) {
        if (temperatureUnit == temperatureUnit) {
            tempCelsium = currentTemperature;
            tempFahrenheit = fromCelsiumToFahrenheit(currentTemperature);
        } else if (temperatureUnit == temperatureUnit) {
            tempFahrenheit = currentTemperature;
            tempCelsium = fromFahrenheitToCelsium(currentTemperature);
        } /*else {
            System.out.println("Температура не распознана, по умолчанию равно Цельсий");
            tempCelsium = currentTemperature;
            tempFahrenheit = fromCelsiumToFahrenheit(currentTemperature);
        }*/
    }

    public Thermometer(double currentTemperature, String temperatureUnit) {
        if (temperatureUnit.equals("C")) {
            tempCelsium = currentTemperature;
            tempFahrenheit = fromCelsiumToFahrenheit(currentTemperature);
        } else if (temperatureUnit.equals("F")) {
            tempFahrenheit = currentTemperature;
            tempCelsium = fromFahrenheitToCelsium(currentTemperature);
        } else {
            System.out.println("Температура не распознана, по умолчанию равно Цельсий");
            tempCelsium = currentTemperature;
            tempFahrenheit = fromCelsiumToFahrenheit(currentTemperature);
        }
    }

    private double fromCelsiumToFahrenheit(double currentTemperature) {
        return (currentTemperature * 1.8) + 32;
    }

    private double fromFahrenheitToCelsium(double currentTemperature) {
        return (currentTemperature - 32) / 1.8;
    }

    public double getCelsium() {
        return this.tempCelsium;
    }

    public double getFahrenheit() {
        return this.tempFahrenheit;
    }
}
