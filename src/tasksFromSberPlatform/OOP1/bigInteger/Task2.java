package tasksFromSberPlatform.OOP1.bigInteger;

import java.math.BigInteger;

/*      Найдите первые 10 квадратных чисел, которые больше Long.MAX_VALUE. Квадратное число — число в виде n^2.
        Например, 4, 9 и 16 являются квадратными числами.*/
public class Task2 {
    public static void main(String[] args) {
        BigInteger x = new BigInteger("" + (long) Math.sqrt(Long.MAX_VALUE));
        x = x.add(BigInteger.ONE);
        //BigInteger y = new BigInteger("" + (int) Math.sqrt(25));

        for (int i = 0; i < 10; i++) {
            System.out.println(x.multiply(x));
            x = x.add(BigInteger.ONE);
        }
    }
}
