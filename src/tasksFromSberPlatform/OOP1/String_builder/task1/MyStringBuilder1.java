package tasksFromSberPlatform.OOP1.String_builder.task1;

/*Java-библиотека предоставляет класс StringBuilder. Предоставьте собственную реализацию следующих методов (назовите новый класс MyStringBuilder1):

public MyStringBuilder1(String s);
public MyStringBuilder1 append(MyStringBuilder1 s);
public MyStringBuilder1 append(int i);
public int length();
public char charAt(int index);
public MyStringBuilder1 toLowerCase();
public MyStringBuilder1 substring(int begin, int end);
public String toString();*/
public class MyStringBuilder1 {
    private String str;

    public MyStringBuilder1(String str) {
        this.str = str;
    }

    public MyStringBuilder1() {
    }

    public MyStringBuilder1 append(MyStringBuilder1 s){
        String newStr = str + s.str;
        return new MyStringBuilder1(newStr);
    }
}
