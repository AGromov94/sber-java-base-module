package secondweek;

import java.util.*;

//Дано число n. Нужно проверить его четность/нечетность
public class Task1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        if (n % 2 == 0) {
            System.out.println("Четное число");
        }
        //тернарный оператор
        String str;
        str = (n % 2 == 0) ? "Число четное" : "Число нечетное";
        System.out.println(str);
    }
}
