package homeworks.homework8.task4;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Dog zhuchka = new Dog("Жучка");
        Dog knopka = new Dog("Кнопка");
        Dog cezar = new Dog("Цезарь");
        Dog dobryash = new Dog("Добряш");

        Participant ivan = new Participant(zhuchka, "Иван", new double[]{7, 6, 7});
        Participant nikolay = new Participant(knopka, "Николай", new double[]{8, 8, 7});
        Participant anna = new Participant(cezar, "Анна", new double[]{4, 5, 6});
        Participant darya = new Participant(dobryash, "Дарья", new double[]{9, 9, 9});

        List<Participant> listOfParticipants = new ArrayList<>();
        listOfParticipants.add(ivan);
        listOfParticipants.add(nikolay);
        listOfParticipants.add(anna);
        listOfParticipants.add(darya);

        Comparator<Participant> compareByAverageOfMarks = new AverageOfMarksComparator();
        listOfParticipants.sort(compareByAverageOfMarks);

        for (int i = listOfParticipants.size() - 1; i > listOfParticipants.size() - 4; i--) {
            System.out.println(listOfParticipants.get(i).getNameOfParticipant() + ": " + listOfParticipants.get(i).getDog().getNameOfDog() + " " + listOfParticipants.get(i).getAverageOfMarks());
        }
    }
}
