package tenthweek.reflection;

import java.lang.reflect.Method;

/*Определение
Рефлексия - знание кода о самом себе
К рефлексии можно отнести возможность проитерироваться по всем полям класса
или найти и создать объект класса, по имени, заданному через текстовую строку
Class - основной класс для рефлексии в Java
*/
public class Reflection {
    public static void main(String[] args) throws ClassNotFoundException {
        //Три способа получить класс
        //1. - псевдо поле .class
        Class<String> str1 = String.class;
        Method[] methods = str1.getDeclaredMethods();
        String name1 = methods[0].getName();
        System.out.println(name1);

        //2 способ - метод getClass(). Он у класса object
        CharSequence sequence = "My String";
        Class<? extends CharSequence> seqInfo = sequence.getClass();

        //3 способ - поиск класса по строковому литералу
        Class<?> integerClass = Class.forName("java.lang.Integer");
    }
}
