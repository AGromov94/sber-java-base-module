package ninethweek.exceptions;

import java.util.*;

public class SimpleExample {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        toDivide(n);
        //можно засунуть блок try-catch в сам метод и это будет правильно
/*        try {
            toDivide(n);
        } catch (ArithmeticException e){
            throw new MyArithmeticalException();
        }
        //будет выполняться всегда после отлова исключения
        finally {
            System.out.println("Последнее слово программы");
        }*/
    }

    public static void toDivide(int n) {
        try {
            System.out.println(100 / n);
        } catch (ArithmeticException e) {
            throw new MyArithmeticalException();
        } finally {
            System.out.println("Last word of program");
        }
    }
}
