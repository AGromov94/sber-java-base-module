package homeworks.homework1;

import java.util.*;
/*На вход подается баланс счета в банке – n. Рассчитайте дневной бюджет на 30 дней.

        Ограничения:
        0 < count < 100000*/

public class Task8 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int balance = scan.nextInt();

        System.out.println(balance / 30.0);
    }
}
