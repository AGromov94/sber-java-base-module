package tasksFromSberPlatform.OOP1.task7;

public class MyTimeClient {
    public static void main(String[] args) {
        MyTime time1 = new MyTime();
        MyTime time2 = new MyTime(555550000);
        MyTime time3 = new MyTime(5, 23, 55);

        time1.printCurrentTime();
        time2.printCurrentTime();
        time3.printCurrentTime();
    }
}
