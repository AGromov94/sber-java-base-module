package secondweek;

import java.util.*;

public class Task3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите 4х значное число");
        int n = scan.nextInt();

        if (n > 999 && n < 10000) {
            int end = n % 10;// первая цифра
            int start = n / 1000; //вторая цифра
            if (end != start) {
                System.out.println("Не палиндром");
            } else {
                end = (n % 100) / 10;
                start = (n / 100) % 10;
                if (end != start) {
                    System.out.println("Не палиндром");
                } else {
                    System.out.println("Это палиндром, Ура!");
                }
            }
        } else {
            System.out.println("Число не 4х значное!");
        }
    }
}

