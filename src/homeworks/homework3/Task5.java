package homeworks.homework3;

import java.util.*;

/*5. Даны положительные натуральные числа m и n. Найти остаток от деления m на
        n, не выполняя операцию взятия остатка.
        Ограничения:
        0 < m, n < 10*/
public class Task5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int m = scan.nextInt();
        int n = scan.nextInt();

        while (m >= n) {
            m -= n;
        }
        System.out.println(m);
    }
}
