package ninethweek.exceptions;

public class MyArithmeticalException extends ArithmeticException {
    public MyArithmeticalException() {
        super("Исключение деления на 0!!!");
    }

    public MyArithmeticalException(String errorMessage) {
        super(errorMessage);
    }
}
