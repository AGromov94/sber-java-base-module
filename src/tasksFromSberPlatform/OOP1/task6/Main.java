package tasksFromSberPlatform.OOP1.task6;

import java.util.Arrays;
//Демонстрация того, как можно изменить private поле, даже если оно
//является приватным и не имеет сеттера, но является ссылочным типом (массивом), а значит может быть изменено
public class Main {
    public static void main(String[] args) {
        A a = new A();
        System.out.println(Arrays.toString(a.getArray()));
        a.getArray()[0] = 1;
        a.getArray()[1] = 1;
        a.getArray()[2] = 1;
        System.out.println(Arrays.toString(a.getArray()));
    }
}
