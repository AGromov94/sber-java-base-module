package tasksFromSberPlatform.OOP2.Generics.task21;

import java.util.Arrays;

/*      Измените класс GenericStack таким образом, чтобы реализовать его с помощью массива, а не ArrayList.
        Перед добавлением нового элемента в стек необходимо проверить размер массива.
        Если массив заполнен, создайте новый массив, который удвоит текущий размер массива и скопирует элементы
        из текущего массива в новый.*/
public class GenericStack<E> {
    private Object[] arr;
    private final int INITIAL_CAPACITY = 16;
    private int size;

    public GenericStack() {
        this.arr = (E[]) new Object[INITIAL_CAPACITY];
        this.size = 0;
    }

    public GenericStack(int capacity) {
        this.arr = (E[]) new Object[capacity];
        this.size = 0;
    }

    public int getSize() {
        return size;
    }

    public E peek() {
        return (E) arr[size - 1];
    }

    public void push(E o) {
        if (size >= arr.length) {
            Object[] newArr = (E[]) new Object[arr.length * 2];
            System.arraycopy(arr, 0, newArr, 0, size);
            arr = newArr;
        }
        arr[size] = o;
        size++;
    }

    public E pop() {
        E o = (E) arr[size - 1];
        Object[] newArr = (E[]) new Object[size - 1];
        System.arraycopy(arr, 0, newArr, 0, size - 1);
        arr = newArr;
        size--;
        return o;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public String toString() {
        return "Наш стек: " + Arrays.toString(arr);
    }
}
