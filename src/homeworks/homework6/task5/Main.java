package homeworks.homework6.task5;

public class Main {
    public static void main(String[] args) {
        DayOfWeek[] daysArray = new DayOfWeek[7];
        daysArray[0] = new DayOfWeek((byte) 1, "Monday");
        daysArray[1] = new DayOfWeek((byte) 2, "Tuesday");
        daysArray[2] = new DayOfWeek((byte) 3, "Wednesday");
        daysArray[3] = new DayOfWeek((byte) 4, "Thursday");
        daysArray[4] = new DayOfWeek((byte) 5, "Friday");
        daysArray[5] = new DayOfWeek((byte) 6, "Saturday");
        daysArray[6] = new DayOfWeek((byte) 7, "Sunday");

        for (int i = 0; i < daysArray.length; i++) {
            System.out.println(daysArray[i].numberOfTheDay + " " + daysArray[i].nameOfTheDay);
        }
    }
}
