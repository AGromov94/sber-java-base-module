package homeworks.homework6.task3;

import homeworks.homework6.task2.Student;

public class Main {
    public static void main(String[] args) {
        int[] gradesForStudent1 = {3, 4, 3, 4, 3, 4, 3, 4, 5, 5};
        int[] gradesForStudent2 = {3, 4, 3, 4, 3, 4, 3, 4, 5, 4};
        int[] gradesForStudent3 = {3, 4, 3, 4, 3, 4, 3, 4, 5, 3};
        Student student1 = new Student("Иван", "Иванов", gradesForStudent1);
        Student student2 = new Student("Петр", "Петров", gradesForStudent2);
        Student student3 = new Student("Михаил", "Михайлов", gradesForStudent3);
        Student[] studentsArray = new Student[3];
        studentsArray[0] = student1;
        studentsArray[1] = student2;
        studentsArray[2] = student3;

        //проверяем получение лучшего студента из массива
        StudentService.printStudentInfo(StudentService.returnBestStudent(studentsArray));

        //проверяем сортировку по фамилии
        StudentService.sortBySurname(studentsArray);
        for (int i = 0; i < 3; i++) {
            StudentService.printStudentInfo(studentsArray[i]);
        }
    }
}
