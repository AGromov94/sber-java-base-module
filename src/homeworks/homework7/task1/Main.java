package homeworks.homework7.task1;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Book book1 = new Book("Евгений Онегин", "Пушкин", 1);
        Book book2 = new Book("Оно", "Кинг", 1);
        Book book3 = new Book("Идиот", "Достоевский", 1);
        Visitor visitor1 = new Visitor("Андрей", 50);
        Library myLibrary = new Library();
        myLibrary.addNewBook(book1);
        myLibrary.addNewBook(book2);
        myLibrary.addNewBook(book3);

        myLibrary.deleteBookByName("Идиот");//удалили книгу из библиотеки

        myLibrary.borrowBookToVisitor("Оно", visitor1);//visitor1 взял книгу

        myLibrary.returnBookToTheLibraryByVisitor(visitor1);//visitor1 вернул книгу и поставил оценку

        myLibrary.borrowBookToVisitor("Оно", visitor1);//visitor1 опять взял книгу

        myLibrary.returnBookToTheLibraryByVisitor(visitor1);//visitor1 вернул книгу и поставил еще оценку

        myLibrary.printBooksTable();//вывели список книг в библиотеке (отметка о наличии равна 1, значит все книги на месте)

        System.out.println(myLibrary.returnRatingOfBook("Оно"));//получили рейтинг книги после двух оставленных оценок
    }
}
