package tasksFromSberPlatform.OOP1.task5;

/*      Создайте класс с именем Location для нахождения наибольшего элемента и его позиции в двумерном массиве.
        Класс должен содержать public-поля row, column и maxValue, в которых будут храниться наибольший элемент типа double
        и его индексы в двумерном массиве со строчками и столбцами типа int.

        Напишите следующий метод, который возвращает позицию наибольшего элемента в двумерном массиве:

        public static Location locateLargest(double[][] a)
        Возвращаемое значение должно быть типа Location. Напишите клиент этого класса — программу, которая предлагает пользователю ввести
        двумерный массив и отображает позицию наибольшего элемента в этом массиве. Пример выполнения программы:

        Введите количество строчек и столбцов массива: 3 4
        Введите массив:
        23.5 35 2 10
        4.5 3 45 3.5
        35 44 5.5 9.6
        Наибольший элемент массива, равный 45.0, находится в позиции (1, 2)*/
public class Location {
    public int row;
    public int column;
    public double maxValue;
    public int  value = 1;

    public static Location locateOfLargestElement(double[][] array) {
        Location location = new Location();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] > location.maxValue) {
                    location.maxValue = array[i][j];
                    location.row = i;
                    location.column = j;
                }
            }
        }
        return location;
    }
}
