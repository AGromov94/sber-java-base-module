package homeworks.homework2;

import java.util.*;
/*За каждый год работы Петя получает на ревью оценку. На вход подаются оценки Пети за последние три года (три целых положительных числа).
        Если последовательность оценок строго монотонно убывает, то вывести "Петя, пора трудиться"
        В остальных случаях вывести "Петя молодец!"

        Ограничения:
        0 < a, b, c < 100*/

public class Task1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int mark1 = scan.nextInt();
        int mark2 = scan.nextInt();
        int mark3 = scan.nextInt();

        if (mark2 < mark1 && mark3 < mark2) {
            System.out.println("Петя, пора трудиться");
        } else {
            System.out.println("Петя молодец!");
        }
    }
}
