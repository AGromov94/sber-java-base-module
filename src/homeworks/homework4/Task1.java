package homeworks.homework4;

import java.util.*;
/*1.	(1 балл) На вход подается число N — длина массива. Затем передается массив вещественных чисел (ai) из N элементов.

        Необходимо реализовать метод, который принимает на вход полученный массив и возвращает среднее арифметическое всех чисел массива.
        Вывести среднее арифметическое на экран.

        Ограничения:
        ●	0 < N < 100
        ●	0 < ai < 1000*/

public class Task1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        double[] arr = new double[n];

        for (int i = 0; i < n; i++) {
            arr[i] = scan.nextDouble();
        }

        System.out.println(averageNumberOfArray(arr));
    }

    static double averageNumberOfArray(double[] inputArray) {
        double sum = 0.0;
        for (double i : inputArray
        ) {
            sum += i;
        }
        return sum / inputArray.length;
    }
}
