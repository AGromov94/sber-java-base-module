package ninethweek.exceptions;

public class MultipleException {
    public static void main(String[] args) {
        try {
            toDivideThrowMyArithmeticalException(100, 0);
            someMethodThrowArrayIndexOutOfBoundException();
            simpleThrowRuntimeException();
            //обрабатываем одновременно все исключения
            //можно написать множество catch блоков и ловить каждое из 3 исключений по отдельности
        } catch (Exception e){
            System.out.println("LOG " + e.getMessage());
        }
    }
    public static void simpleThrowRuntimeException(){
        throw new  RuntimeException();
    }

    public static void someMethodThrowArrayIndexOutOfBoundException(){
        int [] arr = new int[10];
        System.out.println(arr[10]);
    }

    public static void toDivideThrowMyArithmeticalException(int a, int b) throws MyArithmeticalException {
        try{
            System.out.println(a/b);
        } catch (ArithmeticException e){
            throw new MyArithmeticalException();
        }
    }
}
