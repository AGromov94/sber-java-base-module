package fourthweek;

import java.util.*;

/*
 На вход подается число N — длина массива.
 Затем передается массив целых чисел длины N.

  Вывести все четные элементы массива.
  Если таких элементов нет, вывести -1.

5
1 2 3 4 5
->
2 4
 */
public class Task1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[] arr = new int[n];
        boolean flag = false;

        for (int i = 0; i < arr.length; i++) {
            arr[i] = scan.nextInt();
        }
        for (int j = 0; j < arr.length; j++) {
            if (arr[j] % 2 == 0) {
                System.out.println(arr[j]);
                flag = true;
            }
        }
        if (flag == false) {
            System.out.println(-1);
        }
    }
}
