package homeworks.homework7.task1;

/*Должен быть реализован класс Книга, содержащий название и автора. Также должен быть реализован класс Посетитель,
        содержащий имя посетителя и идентификатор (null до тех пор пока не возьмет книгу). Должен быть реализован класс
        Библиотека со следующим функционалом:

        Работа со списком существующих книг в библиотеке.
        Сюда входят все добавленные книги, в том числе и одолженные. Название книги считается уникальным, и в библиотеке не может быть двух книг с одинаковым названием.
        1.	Добавить новую книгу в библиотеку, если книги с таким наименованием ещё нет в библиотеке. Если книга в настоящий момент одолжена, то считается, что она всё равно есть в библиотеке (просто в настоящий момент недоступна).
        2.	Удалить книгу из библиотеки по названию, если такая книга в принципе есть в библиотеке и она в настоящий момент не одолжена.
        3.	Найти и вернуть книгу по названию.
        4.	Найти и вернуть список книг по автору.

        Механизм одалживания книги посетителю. Каждый посетитель в один момент времени может читать только одну книгу.
        5.	Одолжить книгу посетителю по названию, если выполнены все условия:
        a.	Она есть в библиотеке.
        b.	У посетителя сейчас нет книги.
        c.	Она не одолжена.
        Также если посетитель в первый раз обращается за книгой — дополнительно выдать ему идентификатор читателя.
        6.	Вернуть книгу в библиотеку от посетителя, который ранее одалживал книгу. Не принимать книгу от другого посетителя.
        a.	Книга перестает считаться одолженной.
        b.	У посетителя не остается книги.

        Тестирование.
        7.	В методе main написать несколько тестов на реализованный функционал.

        Дополнительная задача*:
        8.	Добавить функционал оценивания книг посетителем при возвращении в библиотеку.
        Оценка книги рассчитывается как среднее арифметическое оценок всех посетителей, кто брал эту книгу.
        Реализовать метод, возвращающий оценку книги по её наименованию.*/

import java.util.Arrays;
import java.util.Scanner;

public class Library {
    private static final int DEFAULT_CAPACITY = 1;
    private Book[] booksArray;
    private int sizeOfBooksArray;
    private int capacityOfBooksArray;

    public Library() {
        booksArray = new Book[DEFAULT_CAPACITY];
        sizeOfBooksArray = 0;
        capacityOfBooksArray = DEFAULT_CAPACITY;
    }

    public void addNewBook(Book book) {
        if (sizeOfBooksArray > 0) {
            //проверка наличия книги в библиотеке
            if (checkContainsBookByName(book.getName())) {
                System.out.println("Такая книга уже есть в библиотеке!");
            } else {
                if (sizeOfBooksArray >= capacityOfBooksArray) {
                    capacityOfBooksArray += 1;
                    booksArray = Arrays.copyOf(booksArray, capacityOfBooksArray);
                }
                booksArray[sizeOfBooksArray] = book;
                sizeOfBooksArray++;
            }
        } else {
            //добавляем первую книгу без проверки
            if (sizeOfBooksArray >= capacityOfBooksArray) {
                capacityOfBooksArray += 1;
                booksArray = Arrays.copyOf(booksArray, capacityOfBooksArray);
            }
            booksArray[sizeOfBooksArray] = book;
            sizeOfBooksArray++;
        }
    }

    public void deleteBookByName(String name) {
        for (int i = 0; i < booksArray.length; i++) {
            if (name.equalsIgnoreCase(booksArray[i].getName()) && booksArray[i].getAvailabilityMark() == 1) {
                Book[] newArray = new Book[capacityOfBooksArray - 1];
                System.arraycopy(booksArray, 0, newArray, 0, i);
                System.arraycopy(booksArray, i + 1, newArray, i, booksArray.length - i - 1);
                booksArray = newArray;
                break;
            } else if (name.equalsIgnoreCase(booksArray[i].getName()) && booksArray[i].getAvailabilityMark() == 0) {
                System.out.println("В настоящий момент книга одолжена посетителю");
                break;
            }
        }
    }

    public Book[] returnArrayOfBooksByAuthor(String author) {
        int size = 0;
        Book[] booksByAuthor = new Book[size];
        for (int i = 0; i < booksArray.length; i++) {
            if (booksArray[i].getAuthor().equalsIgnoreCase(author)) {
                size++;
                booksByAuthor = Arrays.copyOf(booksByAuthor, size);
                booksByAuthor[size - 1] = booksArray[i];
            }
        }
        return booksByAuthor;
    }

    public Book returnBookByName(String name) {
        for (int i = 0; i < booksArray.length; i++) {
            if (booksArray[i].getName().equalsIgnoreCase(name)) {
                return booksArray[i];
            }
        }
        return null;
    }

    //вывод книг на экран для наглядности
    public void printBooksTable() {
        for (int i = 0; i < booksArray.length; i++) {
            System.out.println(booksArray[i].toString());
        }
    }

    //Одалживание книги
    public void borrowBookToVisitor(String nameOfBook, Visitor visitor) {
        if (visitor.getId() == 0) {
            visitor.setId((int) Math.random() * 1000);
        }
        if (checkContainsBookByName(nameOfBook)) {
            if (visitor.getNameOfBook() == null && returnBookByName(nameOfBook).getAvailabilityMark() == 1) {
                returnBookByName(nameOfBook).setAvailabilityMark(0);
                visitor.setNameOfBook(returnBookByName(nameOfBook).getName());
            } else {
                System.out.println("Книга уже одолжена либо у посетителя уже есть книга на руках");
            }
        } else {
            System.out.println("Такой книги нет в библиотеке");
        }
    }

    //Возврат книги посетителем
    public void returnBookToTheLibraryByVisitor(Visitor visitor) {
        Scanner scan = new Scanner(System.in);
        if (visitor.getNameOfBook() == null) {
            System.out.println("У данного посетителя нет книг на руках");
        } else {
            returnBookByName(visitor.getNameOfBook()).setAvailabilityMark(1);
            System.out.println("Введите оценку - число от 0.0 до 5.0");
            returnBookByName(visitor.getNameOfBook()).rating += scan.nextDouble();
            returnBookByName(visitor.getNameOfBook()).reviewers++;
            visitor.setNameOfBook(null);
        }
    }

    //Проверка наличия книги в библиотеке по названию (не зависимо одолжена или нет)
    public boolean checkContainsBookByName(String nameOfBook) {
        boolean answer = false;
        for (int i = 0; i < booksArray.length; i++) {
            if (booksArray[i].getName().equalsIgnoreCase(nameOfBook)) {
                answer = true;
                break;
            }
        }
        return answer;
    }

    //Возвращает оценку книги по ее наименованию
    public double returnRatingOfBook(String nameOfBook) {
        return returnBookByName(nameOfBook).rating / returnBookByName(nameOfBook).reviewers;
    }
}
