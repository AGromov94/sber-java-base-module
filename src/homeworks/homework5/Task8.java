package homeworks.homework5;

import java.util.*;
/*8.	На вход подается число N. Необходимо посчитать и вывести на экран сумму его цифр. Решить задачу нужно через рекурсию.

        Ограничения:
        0 < N < 1000000*/

public class Task8 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int sum = 0;
        System.out.println(recursiveSummary(n, sum));
    }

    private static int recursiveSummary(int n, int sum) {
        if (n >= 0 && n <= 9 ) {
            sum += n;
            return sum;
        } else{
            sum += n%10;
            return recursiveSummary(n/10, sum);
        }
    }
}
