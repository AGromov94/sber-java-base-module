package tasksFromSberPlatform.OOP2.AbstractClassesAndInterfaces.NewGeometricObject;

import tasksFromSberPlatform.OOP1.task10.IllegalTriangleException;

public class NewGeometricObjectTest {
    public static void main(String[] args) {
        try {
            NewGeometricObject triangle1 = new NewTriangle("blue", true, 5, 5, 5);
            NewGeometricObject triangle2 = new NewTriangle("blue", true, 5, 5, 5);
            NewGeometricObject triangle3 = new NewTriangle("blue", true, 2, 2, 2);
            NewGeometricObject circle1 = new NewCircle(1);
            NewGeometricObject circle2 = new NewCircle(1);
            NewGeometricObject square1 = new NewSquare("red", false, 10);
            NewGeometricObject octagon1 = new NewOctagon(1);
            NewGeometricObject octagon2 = new NewOctagon(1);
            NewGeometricObject octagon3 = (NewGeometricObject) ((NewOctagon) octagon1).clone();

            /*System.out.println(triangle1.toString());
            System.out.println(triangle1.getArea());
            System.out.println(triangle1.getPerimeter());
            System.out.println(triangle1.equals(triangle2));
            System.out.println(triangle1.compareTo(triangle2));
            NewGeometricObject[] arr = {triangle1, triangle2, triangle3};
            Arrays.sort(arr);
            System.out.println(Arrays.toString(arr));*/

            NewGeometricObject[] arr = {triangle1, triangle2, triangle3, circle1, square1};
            System.out.println(sumArea(arr));
            System.out.println(circle1.equals(circle2));
            System.out.println(octagon1.compareTo(triangle2));
            /*for (int i = 0; i < arr.length; i++) {
                if (arr[i] instanceof Colorable) {
                    System.out.println(arr[i].getClass());
                    System.out.println(arr[i].toString());
                    ((Colorable) arr[i]).howToColor();
                }
            }*/
            //System.out.println(NewGeometricObject.max(triangle2, circle1));
        } catch (IllegalTriangleException | CloneNotSupportedException e) {
            e.printStackTrace();
        }

    }

    /**Напишите метод, который суммирует площадь всех геометрических объектов в массиве. Сигнатура этого метода следующая:
    public static double sumArea(GeometricObject[] a)
    Напишите тестовую программу, которая создает массив из четырех объектов (два круга и два прямоугольника) и
    вычисляет их общую площадь с помощью метода sumArea().*/
    public static double sumArea(NewGeometricObject[] a) {
        double sum = 0;
        for (int i = 0; i < a.length; i++) {
            sum += a[i].getArea();
        }
        return sum;
    }
}
