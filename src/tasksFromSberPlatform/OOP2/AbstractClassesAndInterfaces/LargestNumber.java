package tasksFromSberPlatform.OOP2.AbstractClassesAndInterfaces;

import java.util.ArrayList;
import java.math.*;

public class LargestNumber {
    public static void main(String[] args) {
        ArrayList<Number> list = new ArrayList<>();
        list.add(45); // прибавляет целое число
        list.add(3445.53); // прибавляет число с плавающей точкой
        // Прибавляет BigInteger
        list.add(new BigInteger("3432323234344343101"));
        list.add(new BigInteger("3432323234344343102"));
        // Прибавляет BigDecimal
        list.add(new BigDecimal("2.0909090989091343433344343"));

        System.out.println("Наибольшее число равно " +
                getLargestNumber(list));
    }

    public static Number getLargestNumber(ArrayList<Number> list) {
        if (list == null || list.size() == 0)
            return null;

        BigDecimal firstNumber = new BigDecimal(list.get(0) + "");
        for (int i = 1; i < list.size(); i++) {
            if (firstNumber.compareTo(new BigDecimal(list.get(i) + "")) < 0) {
                firstNumber = new BigDecimal(list.get(i) + "");
            }
        }
        return firstNumber;
    }
}
