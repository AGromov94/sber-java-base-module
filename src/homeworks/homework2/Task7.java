package homeworks.homework2;
import java.util.*;
/*Петя недавно изучил строки в джаве и решил попрактиковаться с ними.
        Ему хочется уметь разделять строку по первому пробелу. Для этого он может воспользоваться методами indexOf() и substring().

        На вход подается строка. Нужно вывести две строки, полученные из входной разделением по первому пробелу.


        Ограничения:
        В строке гарантированно есть хотя бы один пробел
        Первый и последний символ строки гарантированно не пробел
        2 < s.length() < 100*/

public class Task7 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str = scan.nextLine();
        int indexOfSpace = str.indexOf(" ");

        System.out.println(str.substring(0,indexOfSpace));
        System.out.println(str.substring(indexOfSpace+1));

    }
}
