package homeworks.homework9.task1_and_2;

//2.	Создать собственное исключение MyUncheckedException, являющееся непроверяемым.
public class MyUncheckedException extends RuntimeException {
    public MyUncheckedException() {
    }

    public MyUncheckedException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return "This is my unchecked Exception";
    }
}
