package thirdweek;

import java.util.*;

/*Дана последовательность из n целых числел, которая может начинаться с отрицательного числа.
Определить, какое количество отрицательных чисел записано в начале последовательности и прекратить
выполнение программы при получении первого положительного числа на вход.*/
public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int result = 0;
        while (true) {
            if (scanner.nextInt() < 0) {
                result++;
            } else {
                break;
            }
        }
        System.out.println("Количество отрицательных чисел: " + result);
    }
}

