package tasksFromSberPlatform.OOP1.ArrayList;
        /*Напишите программу, которая создает список типа ArrayList и добавляет в него объект типа Loan, объект типа Date,
        строку и объект типа Circle, а также используйте цикл для отображения всех элементов в списке путем вызова
        метода toString() этих объектов.*/

import tasksFromSberPlatform.OOP1.project.Account;
import tasksFromSberPlatform.OOP1.task10.GeometricObject;
import tasksFromSberPlatform.OOP1.task10.Triangle;
import tasksFromSberPlatform.OOP1.task9.MyDate;

import java.util.ArrayList;
import java.util.Date;

public class task25 {
    public static void main(String[] args) {
        ArrayList<Object> list = new ArrayList<>();
        list.add(new Triangle());
        list.add(new GeometricObject());

        for (Object o : list) {
            System.out.println(o.toString());
        }
    }
}
