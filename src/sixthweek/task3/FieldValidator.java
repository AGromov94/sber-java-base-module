package sixthweek.task3;

import java.util.regex.Pattern;

public class FieldValidator {

    private static final Pattern EMAIL_PATTERN = Pattern.compile("(^[a-z0-9\\_\\-\\*\\.]+@[a-z0-9]+\\.(com|ru)$)");
    private static final Pattern NAME_PATTERN = Pattern.compile("[A-Z][a-z]{1,19}");
    private static final Pattern PHONE_PATTERN = Pattern.compile("\\+[0-9]{11}");
    private static final Pattern DATE_BIRTH_PATTERN = Pattern.compile("[0-9]{2}\\.[0-9]{2}\\.[0-9]{4}");

    public static boolean validateEmail(final String email){
        return EMAIL_PATTERN.matcher(email).matches();
    }

    public static boolean validatePhone(final String phone){
        return PHONE_PATTERN.matcher(phone).matches();
    }

    public static boolean validateBirthDate(final String birthDate){
        return DATE_BIRTH_PATTERN.matcher(birthDate).matches();
    }

    public static boolean validateName(final String name){
        return  NAME_PATTERN.matcher(name).matches();
    }

    private FieldValidator(){

    }
}
