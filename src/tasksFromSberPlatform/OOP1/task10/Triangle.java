package tasksFromSberPlatform.OOP1.task10;

/*      Создайте класс Triangle для представления треугольников, который порождается от класса GeometricObject. Класс Triangle должен содержать:

        Три поля данных типа double с именами side1, side2 и side3 и с заданными по умолчанию значениями, равными 1.0,
        для обозначения трех сторон треугольника.
        Безаргументный конструктор, который создает треугольник с заданными по умолчанию значениями.
        Конструктор, который создает треугольник с side1, side2 и side3.
        Getter-методы для всех трех полей данных.
        Метод с именем getArea(), который возвращает площадь этого треугольника.
        Метод с именем getPerimeter(), который возвращает периметр этого треугольника.
        Метод с именем toString(), который возвращает строковое описание треугольника.*/
public class Triangle extends GeometricObject {
    private double side1 = 1.0;
    private double side2 = 1.0;
    private double side3 = 1.0;

    public Triangle() {
    }

    public Triangle(double side1, double side2, double side3) throws IllegalTriangleException {
        if (side1 + side2 > side3 && side2 + side3 > side2 && side1 + side3 > side2) {
            this.side1 = side1;
            this.side2 = side2;
            this.side3 = side3;
        } else {
            throw new IllegalTriangleException(side1, side2, side3, "Сумма двух сторон меньше третьей");
        }
    }

    //region getters
    public double getSide1() {
        return side1;
    }

    public double getSide2() {
        return side2;
    }

    public double getSide3() {
        return side3;
    }
    //endregion

    public double getArea() {
        double halfOfPerimeter = getPerimeter() / 2;
        return Math.pow(halfOfPerimeter * (halfOfPerimeter - side1) * (halfOfPerimeter - side2) * (halfOfPerimeter - side3), 0.5);
    }

    public double getPerimeter() {
        return side1 + side2 + side3;
    }

    public String toString() {
        return "Треугольник: сторона1 = " + side1 + " сторона2 = " + side2 +
                " сторона3 = " + side3;
    }
}
