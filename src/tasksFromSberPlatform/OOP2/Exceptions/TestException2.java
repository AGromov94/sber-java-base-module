package tasksFromSberPlatform.OOP2.Exceptions;

public class TestException2 {
    public static void main(String[] args) {
        try {
            method();
            System.out.println("После вызова метода");
        }
        catch (ArithmeticException ex) {
            System.out.println("ArithmeticException");
        }
        catch (RuntimeException ex) {
            System.out.println("RuntimeException");
        }
        catch (Exception e) {
            System.out.println("Exception");
        }
    }
    static void method() throws Exception {
        System.out.println(1 / 0);
    }
}
