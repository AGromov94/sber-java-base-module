package tasksFromSberPlatform.OOP2.AbstractClassesAndInterfaces;

import java.util.ArrayList;
import java.util.Collections;

public class NumberArrayListSortOrShuffle {
    public static void main(String[] args) {
        ArrayList<Number> list = new ArrayList<>();
        list.add(3);
        list.add(5.5);
        list.add(1.5);
        list.add(10);
        list.add(0.8);
        list.add(2);
        System.out.println("Начальный порядок чисел:" + "\n" + list);
        sort(list);
        System.out.println("После сортировки:" + "\n" + list);
        shuffle(list);
        System.out.println("После перемешивания:" + "\n" + list);
    }

    public static void sort(ArrayList<Number> list) {
        for (int i = 0; i < list.size() - 1; i++) {
            // Найти наименьшее значение в list[i..list.length-1]
            double currentMin = list.get(i).doubleValue();
            int currentMinIndex = i;

            for (int j = i + 1; j < list.size(); j++) {
                if (currentMin > list.get(j).doubleValue()) {
                    currentMin = list.get(j).doubleValue();
                    currentMinIndex = j;
                }
            }

            // Переставить list[i] и list[currentMinIndex], если необходимо
            if (currentMinIndex != i) {
                list.set(currentMinIndex, list.get(i));
                list.set(i, currentMin);
            }
        }
    }

    public static void shuffle(ArrayList<Number> list) {
        for (int i = 0; i < list.size(); i++) {
            int randomIndex = (int) (Math.random() * list.size());
            Number temp = list.get(i);
            list.set(i, list.get(randomIndex));
            list.set(randomIndex, temp);
        }
    }
}
