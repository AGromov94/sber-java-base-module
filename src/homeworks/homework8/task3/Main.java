package homeworks.homework8.task3;
/*3.	На вход передается N — количество столбцов в двумерном массиве и M —
        количество строк. Необходимо вывести матрицу на экран, каждый элемент
        которой состоит из суммы индекса столбца и строки этого же элемента. Решить необходимо используя ArrayList.

        Ограничения:
        ●	0 < N < 100
        ●	0 < M < 100

        Пример:
        Входные данные	Выходные данные
        2 2	                0 1
                            1 2

        3 5	                0 1 2
                            1 2 3
                            2 3 4
                            3 4 5
                            4 5 6
*/

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int m = scan.nextInt();

        ArrayList<ArrayList> columns = new ArrayList<>();
        for (int i = 0; i < m; i++) {
            ArrayList<Integer> rows = new ArrayList<>();
            for (int j = 0; j < n; j++) {
                rows.add(i + j);
            }
            columns.add(rows);
        }

        for (ArrayList element : columns) {
            for (Object number : element) {
                System.out.print(number + " ");
            }
            System.out.println();
        }
    }
}
