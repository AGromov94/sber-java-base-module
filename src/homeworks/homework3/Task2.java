package homeworks.homework3;

import java.util.*;

/*2. На вход подается два положительных числа m и n. Найти сумму чисел между m
        и n включительно.
        Ограничения:
        0 < m, n < 10
        m < n*/
public class Task2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int m = scan.nextInt();
        int n = scan.nextInt();
        int sum = 0;

        for (int i = m; i < n + 1; i++) {
            sum += i;
        }
        System.out.println(sum);
    }
}
