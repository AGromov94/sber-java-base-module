package tasksFromSberPlatform.OOP2.Exceptions.Practicum;

import java.util.*;

/*Создайте класс Fraction с двумя целочисленными final-полями:числителем и знаменателем.
  Конструктор этого класса создает дроби при заданных значениях,
  но выбрасывает исключение NullDenominatorException при нулевом знаменателе.
  Для этого дополнительно создайте класс проверяемого исключения NullDenominatorException,объекты которого выбрасываются,
  если знаменатель объекта типа Fraction равен 0,а также напишите тестовый класс,который проверяет эти классы.*/

public class Fraction {
    private final int numerator;
    private final int denominator;

    public Fraction() {
        this.numerator = 1;
        this.denominator = 1;
    }

    public Fraction(int numerator, int denominator) throws NullDenominatorException {
        if(denominator == 0){
            throw new NullDenominatorException(numerator, denominator, "Знаменатель не может быть равен 0");
        }
        this.numerator = numerator;
        this.denominator = denominator;
    }

    @Override
    public String toString() {
        return numerator + "/" + denominator;
    }
}
