package tasksFromSberPlatform.OOP1.ArrayList;

import java.util.*;

/*      Напишите следующий метод сортировки числового списка типа ArrayList:

        public static void sort(ArrayList<Integer> list)
        Напишите тестовую программу, которая запрашивает у пользователя пять чисел,
        сохраняет их в списке и отображает в порядке возрастания.*/
public class task27 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        ArrayList<Integer> list = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            System.out.println("Введите чило номер " + (i + 1));
            list.add(scan.nextInt());
        }

        sort(list);
        System.out.println(list);
    }

    //сортировка списка методом выбора
    public static void sort(ArrayList<Integer> list) {
        for (int i = 0; i < list.size() - 1; i++) {
            // Найти наименьшее значение в list[i..list.size-1]
            int currentMin = list.get(i);
            int currentMinIndex = i;

            for (int j = i + 1; j < list.size(); j++) {
                if (currentMin > list.get(j)) {
                    currentMin = list.get(j);
                    currentMinIndex = j;
                }
            }
            // Переставить list(i) и list(currentMinIndex), если необходимо
            if (currentMinIndex != i) {
                list.set(currentMinIndex, list.get(i));
                list.set(i, currentMin);
            }
        }
    }
}
