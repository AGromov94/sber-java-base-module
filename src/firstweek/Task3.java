package firstweek;

import java.util.*;

/*
    Напишите программу, которая получает два числа с плавающей точкой х и у в аргументах
    командной строки и выводит евклидово расстояние от точки (х, у) до точки (0, 0)

    Входные данные
    i = 7 j = 5

    Евклидово расстояние - это расстояние между двумя точками,
    вычисляемое по теореме Пифагора.
     (сумма квадратов длин катетов равна квадрату длины гипотенузы.)
     корень квадратный из суммы квадратов x2-x1 и y2-y1
    d = sqrt((x2-x1)^2 + (y2-y1)^2)

    y
    |
  4 |_____(3,4)
    |    /|
    |   / |
    |  /  |
    | /   |
    |/____|________________x
          3
     */
public class Task3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double a = scan.nextDouble();
        double b = scan.nextDouble();

        double result = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
        System.out.println(result);
    }
}
