package homeworks.homework9.task10;

import java.util.*;

/*10.	В некоторой организации хранятся документы (см. класс Document). Сейчас все документы лежат в ArrayList,
        из-за чего поиск по id документа выполняется неэффективно. Для оптимизации поиска по id,
        необходимо помочь сотрудникам перевести хранение документов из ArrayList в HashMap.*/
public class DocumentClient {
    public static void main(String[] args) {
        Document doc1 = new Document(1, "Отчет №1", 18);
        Document doc2 = new Document(4, "Отчет №4", 10);
        Document doc3 = new Document(2, "Отчет №2", 8);
        Document doc4 = new Document(6, "Отчет №6", 12);
        Document doc5 = new Document(3, "Отчет №3", 5);
        Document doc6 = new Document(5, "Отчет №5", 1);

        ArrayList<Document> list = new ArrayList<>();
        list.add(doc1);
        list.add(doc2);
        list.add(doc3);
        list.add(doc4);
        list.add(doc5);
        list.add(doc6);

        //Отсортируем документы по id
        list.sort(Document.COMPARE_BY_ID);

        HashMap<Integer, Document> map = new HashMap<>();
        for (int i = 0; i < list.size();i++) {
            map.put(i, list.get(i));
        }

        map.forEach((key, value) -> System.out.println(key + " " + value.toString()));
    }
}
