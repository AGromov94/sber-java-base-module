package homeworks.homework6.task3;
/*3.	Необходимо реализовать класс StudentService.
        У класса должны быть реализованы следующие публичные методы:
        ●	bestStudent() — принимает массив студентов (класс Student из предыдущего задания), возвращает лучшего студента
        (т.е. который имеет самый высокий средний балл). Если таких несколько — вывести любого.
        ●	sortBySurname() — принимает массив студентов (класс Student из предыдущего задания) и сортирует его по фамилии.*/

import homeworks.homework6.task2.Student;

import java.util.Arrays;
import java.util.Comparator;

public class StudentService {
    public static Student returnBestStudent(Student[] studentsArray) {
        Student bestStudent = studentsArray[0];
        for (int i = 1; i < studentsArray.length; i++) {
            if (bestStudent.returnAverageOfMarks(bestStudent.getGrades()) <= studentsArray[i].returnAverageOfMarks(studentsArray[i].getGrades())) {
                bestStudent = studentsArray[i];
            }
        }
        return bestStudent;
    }

    public static void sortBySurname(Student[] studentsArray) {
        Arrays.sort(studentsArray, Comparator.comparing(Student::getSurname));
    }

    public static void printStudentInfo(Student student) {
        System.out.println("Имя студента: " + student.getName());
        System.out.println("Фамилия студента: " + student.getSurname());
    }
}
