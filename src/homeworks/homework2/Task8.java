package homeworks.homework2;

import java.util.*;
/*Раз так легко получается разделять по первому пробелу, Петя решил немного изменить предыдущую программу и теперь
        разделять строку по последнему пробелу.

        Ограничения:
        В строке гарантированно есть хотя бы один пробел
        Первый и последний символ строки гарантированно не пробел
        2 < s.length() < 100*/

public class Task8 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str = scan.nextLine();
        int indexOfSpace = str.lastIndexOf(" ");

        System.out.println(str.substring(0, indexOfSpace));
        System.out.println(str.substring(indexOfSpace + 1));
    }
}
