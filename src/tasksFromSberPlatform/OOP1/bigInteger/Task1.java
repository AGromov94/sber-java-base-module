package tasksFromSberPlatform.OOP1.bigInteger;

import java.math.BigInteger;

//Найдите первые 10 чисел с 50 десятичными цифрами, которые кратны 2 или 3
public class Task1 {
    public static void main(String[] args) {
        int counter = 0;
        BigInteger x = new BigInteger("10000000000000000000000000000000000000000000000000");
        while(counter < 10){
            if(x.remainder(new BigInteger("2")).equals(BigInteger.ZERO) || x.remainder(new BigInteger("3")).equals(BigInteger.ZERO)){
                System.out.println(x);
                counter++;
            }
            x = x.add(BigInteger.ONE);
        }
    }
}
