package homeworks.homework3;

import java.util.*;

/*9. На вход последовательно подается возрастающая последовательность из n
        целых чисел, которая может начинаться с отрицательного числа.
        Посчитать и вывести на экран, какое количество отрицательных чисел было
        введено в начале последовательности. Помимо этого нужно прекратить
        выполнение цикла при получении первого неотрицательного числа на вход.
        Ограничения:
        0 < n < 1000
        -1000 < ai < 1000*/
public class Task9 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int counter = 0;
        while (scan.hasNext()) {
            int k = scan.nextInt();
            if (k > 0) {
                break;
            } else {
                counter++;
            }
        }
        System.out.println(counter);
    }
}
