package homeworks.homework10.task1;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface IsReallyLike {
    String message() default "Really Like!";
}
