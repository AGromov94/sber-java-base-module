package eightweek.factory;

public class CherryIceCream implements IceCream{
    @Override
    public void printIngredients() {
        System.out.println("Крем, лед, вишня, молоко");
    }
}
