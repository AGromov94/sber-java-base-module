package homeworks.homework9.task7;

import java.util.*;

/*7.	Реализовать метод, который на вход принимает ArrayList<T>, а возвращает набор уникальных элементов этого массива.
        Решить используя коллекции.*/
public class Main {
    public static void main(String[] args) {
        Integer[] array = {1, 1, 2, 2, 3, 3, 3, 3, 4, 5, 5, 5, 5, 5};
        String[] array1 = {"Moscow", "Moscow", "Yalta", "Yalta", "Kazan", "Kazan"};

        List<Object> listOfNumbers = new ArrayList<>();
        List<Object> listOfCities = new ArrayList<>();

        Collections.addAll(listOfNumbers, array);
        Collections.addAll(listOfCities, array1);

        printUniqueElementsFromArrayList(listOfNumbers);
        System.out.println();
        printUniqueElementsFromArrayList(listOfCities);
    }

    public static void printUniqueElementsFromArrayList(List<Object> list) {
        HashSet<Object> set = new HashSet<>(list);
        list.clear();
        list.addAll(set);
        print(list);
    }

    public static void print(List<Object> list) {
        for (Object o : list) {
            System.out.print(o + " ");
        }
    }
}
