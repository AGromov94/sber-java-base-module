package secondweek;

import java.util.*;

public class Task4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int number = scan.nextInt();
        switch (number) {
            case 1:
                System.out.println(125);
                break;
            case 2:
                System.out.println(110);
                break;
            case 3:
                System.out.println(100);
                break;
            default:
                System.out.println("Некорректный ввод");
                break;
        }
        /*Новый вариант switch case
        switch (number){
            case 1 -> System.out.println(125);
            case 2 -> System.out.println(110);
            case 3 -> System.out.println(100);
            default -> System.out.println("Некорректный ввод");
        }*/
    }
}
