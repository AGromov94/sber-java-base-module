package seventhweek.task2;
/*Ленивая инициализация

-
Не потокобезопасна*/
public class LazySingletone {
    private static LazySingletone INSTANCE;

    private LazySingletone() {

    }

    public static LazySingletone getInstance() {
        if(INSTANCE == null){
            INSTANCE = new LazySingletone();
        }
        return INSTANCE;
    }
}
