package homeworks.homework5;

import java.util.*;
/*4.	На вход подается число N — количество строк и столбцов матрицы. Затем передается сама матрица, состоящая из натуральных чисел.
        После этого передается натуральное число P.

        Необходимо найти элемент P в матрице и удалить столбец и строку его содержащий (т.е. сохранить и вывести на экран массив меньшей размерности).
        Гарантируется, что искомый элемент единственный в массиве.

        Ограничения:
        ●	0 < N < 100
        ●	0 < ai < 1000*/

public class Task4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[][] matrix = new int[n][n];
        int[][] newMatrix = new int[n - 1][n - 1];
        String str = "";
        int counter1 = 0;
        int counter2 = 1;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = scan.nextInt();
            }
        }
        final int P = scan.nextInt();
        //зануление всех элементов матрицы в координатах искомого числа
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == P) {
                    for (int k = 0; k < n; k++) {
                        matrix[i][k] = 0;
                        matrix[k][j] = 0;
                    }
                    break;
                }
            }
        }
        //запись не 0 элементов во вспомогательную строку
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] != 0) {
                    str += matrix[i][j];
                }
            }
        }
        //запись нового массива из вспомогательной строки
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - 1; j++) {
                newMatrix[i][j] = Integer.parseInt(str.substring(counter1,counter2));
                counter1++;
                counter2++;
            }
        }
        //вывод на экран новой матрицы без пробелов в конце строк
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - 1; j++) {
                if (j == n - 2) {
                    System.out.print(newMatrix[i][j]);
                } else {
                    System.out.print(newMatrix[i][j] + " ");
                }
            }
            System.out.println();
        }
    }
}
