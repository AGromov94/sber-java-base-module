package firstweek;

import java.util.*;
/*
   Даны числа a, b, c. Нужно перенести значения
   из a -> b, из b -> с, и из c -> а.

   Входные данные:
   a = 3, b = 2, c = 1.
*/

public class Task1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a, b, c, temp;

        a = scan.nextInt();
        b = scan.nextInt();
        c = scan.nextInt();

        System.out.println("Результат ввода: а = " + a + "; b = " + b + "; c = " + c);

        temp = c;
        c = b;
        b = a;
        a = temp;

        System.out.println("Результат работы программы: а = " + a + "; b = " + b + "; c = " + c);
    }
}
