package homeworks.homework6.task7;

public class Main {
    public static void main(String[] args) {
        //тесты
        System.out.println(TriangleChecker.checkingExistingOfTriangle(3,3,3));//равносторонний
        System.out.println(TriangleChecker.checkingExistingOfTriangle(3,3,0));//с 0 стороной
        System.out.println(TriangleChecker.checkingExistingOfTriangle(3,8,2));//без 0, но с НЕ соблюденным условием существования
    }
}
