package tasksFromSberPlatform.OOP1.task7;

/*      Создайте класс MyTime для представления времени. Класс MyTime должен содержать:

        Поля данных hour, minute и second, которые представляют время.
        Безаргументный конструктор, который создает объект типа MyTime для текущего времени. (Значения полей данных этого объекта
        представляют текущее время.)
        Конструктор, который создает объект типа MyTime с указанным временем в миллисекундах, прошедших с 00:00, 1 января 1970 г.
        (Значения полей данных этого объекта будут представлять это время.)
        Конструктор, который создает объект типа MyTime с указанными часами, минутами и секундами.
        Три getter-метода для полей данных hour, minute и second соответственно.
        Метод с именем setTime(long elapseTime), который присваивает объекту новое время с помощью прошедшего времени. Например,
        если прошедшее время равно 555550000 миллисекундам, то hour равно 10, minute равно 19, а second равно 10.
        Нарисуйте UML-диаграмму класса MyTime, а затем реализуйте этот класс. Напишите клиент этого класса — программу,
        которая создает три объекта типа MyTime (с помощью new MyTime(), new MyTime(555550000) и new MyTime(5, 23, 55)) и отображает
        значениях их полей данных hour, minute и second в формате часы:минуты:секунды.

        (Подсказка: первые два конструктора извлекут значения hour, minute и second из прошедшего времени. Для безаргументного конструктора
        текущее время может быть получено с помощью метода System.currentTimeMillis(), как было показано в программе ShowCurrentTime из
        курса «Основы Java-программирования». Пусть время будет GMT.)*/
public class MyTime {
    private long hour;
    private long minute;
    private long second;

    public MyTime() {
        this.second = System.currentTimeMillis() / 1000 % 60;
        this.minute = System.currentTimeMillis() / 1000 / 60 % 60;
        this.hour = System.currentTimeMillis() / 1000 / 3600 % 24;
    }

    public MyTime(long milliseconds) {
        this.second = milliseconds / 1000 % 60;
        this.minute = milliseconds / 1000 / 60 % 60;
        this.hour = milliseconds / 1000 / 3600 % 24;
    }

    public MyTime(long hour, long minute, long second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public void setTime(long elapseTime) {
        this.second = elapseTime / 1000 % 60;
        this.minute = elapseTime / 1000 / 60 % 60;
        this.hour = elapseTime / 1000 / 3600 % 24;
    }

    public long getHour() {
        return hour;
    }

    public long getMinute() {
        return minute;
    }

    public long getSecond() {
        return second;
    }

    public void printCurrentTime() {
        if (hour < 10) {
            System.out.print(0 + "" + hour + ":");
        } else System.out.print(hour + ":");
        if (minute < 10) {
            System.out.print(0 + "" + minute + ":");
        } else System.out.print(minute + ":");
        if (second < 10) {
            System.out.print(0 + "" + second);
        } else System.out.print(second);
        System.out.println();
    }
}
