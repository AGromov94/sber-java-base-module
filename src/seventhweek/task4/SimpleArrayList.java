package seventhweek.task4;

import homeworks.homework6.task5.DayOfWeek;

import java.util.Arrays;

public class SimpleArrayList {
    private static int DEFAULT_CAPACITY = 3;
    private int[] array;
    private int size;
    private int capacity;

    public SimpleArrayList() {
        array = new int[DEFAULT_CAPACITY];
        capacity = DEFAULT_CAPACITY;
        size = 0;
    }

    public SimpleArrayList(int capacity) {
        array = new int[capacity];
        this.capacity = capacity;
        size = 0;
    }

    public void add(int element) {
        if (size >= capacity) {
            capacity = 2 * capacity;
            array = Arrays.copyOf(array, capacity);
        }
        array[size] = element;
        size++;
    }

    public int get(int index) {
        if (index < 0 || index > size) {
            System.out.println("Impossible action");
            return -1;
        } else {
            return array[index];
        }
    }

    public int size() {
        return size;
    }
}
