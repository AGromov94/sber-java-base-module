package firstweek;

import java.util.*;
/*

Дана площадь круга, нужно найти диаметр окружности и длину окружности.
S = PI * (D^2 / 4) - это через диаметр => d = sqrt(S * 4 / PI)
S = PI * r^2 - радиус
S = L^2 / (4 *PI) - площадь через длину
Отношение длины окружности к диаметру является постоянным числом.
π = L : d
Входные данные:
91
 */

public class Task4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int area = scan.nextInt();

        double diam = Math.sqrt((area * 4 / Math.PI));
        double length = diam * Math.PI;

        System.out.println("Длина окружности площади " + area + " равна " + length);
        System.out.println("Диаметр окружности площади " + area + " равен " + diam);
    }
}
