package homeworks.homework5;

import java.util.*;
/*6.	Петя решил начать следить за своей фигурой. Но все существующие приложения для подсчета калорий ему не понравились и он решил написать свое.
        Петя хочет каждый день записывать сколько белков, жиров, углеводов и калорий он съел, а в конце недели приложение должно его уведомлять, \
        вписался ли он в свою норму или нет.

        На вход подаются числа C — недельная норма белков, B — недельная норма жиров, C — недельная норма углеводов и K — недельная норма калорий.
        Затем передаются 7 строк, в которых в том же порядке указаны сколько было съедено Петей нутриентов в каждый день недели.
        Если за неделю в сумме по каждому нутриенту не превышена недельная норма, то вывести “Отлично”, иначе вывести “Нужно есть поменьше”.

        Ограничения:
        ●	0 < C, B, C < 2000
        ●	0 < ai, bi, ci < 2000
        ●	0 < K < 20000
        ●	0 < ki < 20000*/

public class Task6 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        final int A = scan.nextInt();
        final int B = scan.nextInt();
        final int C = scan.nextInt();
        final int K = scan.nextInt();
        int sum = 0;
        final int[] NORMAL_CALORIES = {A, B, C, K};

        int[][] tableOfCalories = new int[7][4];
        String answer = "Отлично";

        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 4; j++) {
                tableOfCalories[i][j] = scan.nextInt();
            }
        }

/*        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print(tableOfCalories[i][j] + " ");
            }
            System.out.println();
        }*/

        for (int i = 0; i < 4; i++) {
            if (answer.equals("Нужно есть поменьше")) {
                break;
            }
            sum = 0;
            for (int j = 0; j < 7; j++) {
                sum += tableOfCalories[j][i];
                if (sum > NORMAL_CALORIES[i]) {
                    answer = "Нужно есть поменьше";
                    break;
                }
            }
        }
        System.out.println(answer);
    }
}
