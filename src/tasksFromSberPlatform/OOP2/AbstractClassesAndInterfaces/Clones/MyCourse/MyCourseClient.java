package tasksFromSberPlatform.OOP2.AbstractClassesAndInterfaces.Clones.MyCourse;

public class MyCourseClient {
    public static void main(String[] args) {
        MyCourse course1 = new MyCourse("«Основы программирования»");
        MyCourse course2 = new MyCourse("«Объектно-ориентированное программирование»");

        course1.addStudent("Петр Иванович Н.");
        course1.addStudent("Борис Сергеевич П.");
        course1.addStudent("Анна Дмитриевна К.");

        course2.addStudent("Петр Иванович Н.");
        course2.addStudent("Анна Сергеевна Д.");
        course2.dropStudent("Анна Сергеевна Д.");

        try {
            MyCourse course3 = (MyCourse) course1.clone();
            course1.addStudent("Андрей Анатольевич Г."); //после добавление в курс1 содержтвое курса 3 (копии курса 1) не меняется

        System.out.println("Количество студентов по дисциплине\n"
                + course1.getCourseName() + ": " + course1.getNumberOfStudents());
        String[] students1 = course1.getStudents();
        for (int i = 0; i < course1.getNumberOfStudents(); i++)
            System.out.print(students1[i] + ", ");

        System.out.println();
        System.out.print("Количество студентов по дисциплине\n"
                + course2.getCourseName() + ": " + course2.getNumberOfStudents() + "\n");
            String[] students2 = course2.getStudents();
        for (int i = 0; i < course2.getNumberOfStudents(); i++)
            System.out.print(students2[i] + ", ");

            System.out.println();
            System.out.print("Количество студентов по дисциплине\n"
                    + course3.getCourseName() + ": " + course3.getNumberOfStudents() + "\n");
            String[] students3 = course3.getStudents();
            for (int i = 0; i < course3.getNumberOfStudents(); i++)
                System.out.print(students3[i] + ", ");

        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}
