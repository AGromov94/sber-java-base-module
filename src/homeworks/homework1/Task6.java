package homeworks.homework1;

import java.util.*;
/*На вход подается количество километров count. Переведите километры в мили (1 миля = 1,60934 км) и выведите количество миль.

        Ограничения:
        0 < count < 1000*/

public class Task6 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int kilometres = scan.nextInt();

        System.out.println(kilometres / 1.60934);
    }
}
