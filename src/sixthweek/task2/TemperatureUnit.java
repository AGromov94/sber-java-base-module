package sixthweek.task2;

public enum TemperatureUnit {
    CELSIUS, FAHRENHEIT
}
