package homeworks.homework2;

import java.util.*;
/*Петя снова пошел на работу. С сегодняшнего дня он решил ходить на обед строго после полудня.
Периодически он посматривает на часы (x - час, который он увидел). Помогите Пете решить, пора ли ему на обед или нет.
Если время больше полудня, то вывести "Пора". Иначе - “Рано”.

        Ограничения:
        0 <= n <= 23*/

public class Task3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int time = scan.nextInt();

        if (time > 12) {
            System.out.println("Пора");
        } else {
            System.out.println("Рано");
        }
    }
}
