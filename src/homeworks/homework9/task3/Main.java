package homeworks.homework9.task3;

import java.io.*;
import java.util.*;

/*3.	Реализовать метод, открывающий файл ./input.txt и сохраняющий в файл ./output.txt текст из input,
        где каждый латинский строчный символ заменен на соответствующий заглавный. Обязательно использование try с ресурсами.*/
public class Main {
    final static String INPUT_FILE_DIRECTORY = "C:\\Users\\User\\IdeaProjects\\SberJava\\src\\homeworks\\homework9\\task3\\files\\input.txt";
    final static String OUTPUT_FILE_DIRECTORY = "C:\\Users\\User\\IdeaProjects\\SberJava\\src\\homeworks\\homework9\\task3\\files\\";
    final static String OUTPUT_FILE_NAME = "output.txt";

    public static void main(String[] args) throws IOException {
        readFileAndWriteNewFileInUpperCase(INPUT_FILE_DIRECTORY);
    }

    public static void readFileAndWriteNewFileInUpperCase(String filePath) throws IOException {
        Scanner scan = new Scanner(new File(filePath));
        String inputText = scan.nextLine();
        StringBuilder result = new StringBuilder();
        Writer writer = new FileWriter(OUTPUT_FILE_DIRECTORY + "\\" + OUTPUT_FILE_NAME);
        try (scan; writer) {
            for (int i = 0; i < inputText.length(); i++) {
                if (Character.isLetter(inputText.charAt(i)) && Character.isLowerCase(inputText.charAt(i))) {
                    result.append(Character.toUpperCase(inputText.charAt(i)));
                } else {
                    result.append(inputText.charAt(i));
                }
            }
            writer.write(result.toString());
        }
    }
}
