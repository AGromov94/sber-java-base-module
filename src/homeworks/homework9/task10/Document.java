package homeworks.homework9.task10;

import java.util.Comparator;

public class Document {
    public int id;
    public String name;
    public int pageCount;

    public Document(int id, String name, int pageCount) {
        this.id = id;
        this.name = name;
        this.pageCount = pageCount;
    }

    public Document() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPageCount() {
        return pageCount;
    }

    public static final Comparator<Document> COMPARE_BY_ID = new Comparator<>() {
        @Override
        public int compare(Document doc1, Document doc2) {
            return doc1.getId() - doc2.getId();
        }
    };

    @Override
    public String toString() {
        return "id: " + id + " name: " + name + " pageCount: " + pageCount;
    }
}
