package ninethweek.generics.task5;

import java.util.Scanner;

//с собеседования
//        подается строка из маленьких латинских символов.
//        нужно проверить что в строке встречаются все символы латинского алфавита хотя бы раз
public class B {
    public static void main(String[] args) {
        Integer[] arr1 = {1,2,3};
        String[] arr2 = {"Zoo", "Foo", "Boo"};
        printArray(arr1);
        printArray(arr2);
//        Scanner scan = new Scanner(System.in);
//        int num = scan.nextInt();
//
//        System.out.println(getFact(num));
//    }
//
//    public static int getFact(int num) {
//        int res = 1;
//        while (num > 1) {
//            res *= num;
//            num--;
//        }
//        return res;
//    }
//    public static boolean check(String str) {
//        if (str.length() < 26) {
//            return false;
//        }
//        char[] arr = str.toCharArray();
//        for (char i = 'a'; i <= 'z'; i++) {
//            if (str.indexOf(i) < 0) {
//                return false;
//            }
//        }
//        return true;
   }

    public static <T> void printArray(T[] array){
        for (T t : array) {
            System.out.print(t+" ");
        }
    }
}
