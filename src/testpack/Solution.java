package testpack;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public class Solution {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {
        Class<Inner.Private> c = Inner.Private.class;
        Constructor<Inner.Private> f = c.getDeclaredConstructor();
        f.setAccessible(true);
        Object o = f.newInstance();
        f.setAccessible(false);
       // System.out.println(Arrays.toString(f));
//        Method powerOf2Method = c.getDeclaredMethod("powerof2", int.class);
//        powerOf2Method.setAccessible(true);
//        powerOf2Method.invoke(new Inner(),8);
//        powerOf2Method.setAccessible(false);
    }

    static class Inner {
        private  int a;
        private class Private {
            private String powerof2(int num) {
                return ((num & num - 1) == 0) ? "power of 2" : "not a power of 2";
            }
        }
    }
}
