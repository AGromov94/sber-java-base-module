package firstweek;

import java.util.*;

/*
Дано целое число n.
Выведите следующее за ним четное число.
При решении этой задачи нельзя использовать условную инструкцию if и циклы.

5 -> 6
10 -> 12
 */
public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        System.out.println((number/2 + 1) *2);
    }

}