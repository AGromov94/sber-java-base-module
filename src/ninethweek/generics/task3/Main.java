package ninethweek.generics.task3;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>();
        set1.add(0);
        set1.add(0);
        set1.add(1);
        set1.add(2);
        set1.add(3);
        set1.add(4);

        Set<Integer> set2 = new HashSet<>();
        set2.add(4);
        set2.add(3);
        set2.add(7);
        set2.add(8);

        set1.retainAll(set2); //удалит из set1 все элементы, которые не содержатся в set2

        //вернет true, если нет общих элементов в 2 коллекциях
        System.out.println(Collections.disjoint(set1,set2));
    }
}
