package tasksFromSberPlatform.OOP1.bigInteger;

import java.math.BigInteger;

//Найдите первые 10 чисел, больших Long.MAX_VALUE, которые кратны 5 или 6
public class Task5 {
    public static void main(String[] args) {
        int counter = 0;
        BigInteger x = new BigInteger(String.valueOf(Long.MAX_VALUE)).add(BigInteger.ONE);

        /* эксперименты что все верно
        System.out.println(Long.MAX_VALUE);
        System.out.println(x);*/

        while (counter < 10) {
            if (x.remainder(new BigInteger("5")).equals(BigInteger.ZERO) || x.remainder(new BigInteger("6")).equals(BigInteger.ZERO)) {
                System.out.println(x);
                counter++;
            }
            x = x.add(BigInteger.ONE);
        }
    }
}
