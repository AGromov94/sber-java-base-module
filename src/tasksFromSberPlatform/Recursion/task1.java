package tasksFromSberPlatform.Recursion;

import java.util.Scanner;

//Напишите рекурсивный метод, который вычисляет 1 + 2 + 3 + … + n для положительного целого числа n.
public class task1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите целое цисло n");
        int n = scan.nextInt();

        System.out.println("Сумма чисел от 1 до n равна: " + recursivelySum(n));
        System.out.println("Факториал числа " + n + " равен " + factorial(n));
        System.out.println("Число n в степени 4 равно " + power_raiser(n, 4));
    }

    public static int recursivelySum(int n) {
        if (n == 1) {
            return 1;
        } else {
            return n + recursivelySum(n - 1);
        }
    }

    //получение факториала числа рекурсивно
    public static int factorial(int n) {
        if (n == 2) {
            return 2;
        } else {
            return n * factorial(n - 1);
        }
    }

    //возведение в степень рекурсивно
    public static int power_raiser(int number, int power) {
        if (power == 1) {
            return (number);
        } else {
            return (number * power_raiser(number, power - 1));
        }
    }

    //получение числа Фибоначчи по индексу с помощью цикла
    public static long fib(long index) {
        int fib0 = 0;
        int fib1 = 1;
        int currentFib = 0;
        for (int i = 2; i <= index; i++) {
            currentFib = fib0 + fib1;
            fib0 = fib1;
            fib1 = currentFib;
        }
        return currentFib;
    }

    //получение числа Фибоначчи по индексу с помощью рекурсии
/*    public static long fib(long n) {
        if(n == 0){
            return 0;
        }
        else if(n == 1){
            return 1;
        }
        else {
            return fib(n-1) + fib(n-2);
        }
    }*/
}
