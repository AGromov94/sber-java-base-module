package homeworks.homework8.task1;

public abstract class Animals {
    public void eat() {
        System.out.println("Я животное и я ем");
    }

    public void sleep() {
        System.out.println("Я животное и я сплю");
    }

    public abstract void wayOfBirth();
}
