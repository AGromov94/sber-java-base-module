package tasksFromSberPlatform.OOP2.Generics;

public class TripletTest {
    public static void main(String[] args) {
        Triplet<Integer> triplet = new Triplet<Integer>(2, 3, 1);
        System.out.println(triplet.containsElement(1));
        System.out.println(triplet.min());
        System.out.println(triplet.sum());
        System.out.println(triplet.toString());

        System.out.println();

        Triplet<Double> triplet1 = new Triplet<Double>(2.0, 3.0, 1.0);
        System.out.println(triplet1.containsElement(10.2));
        System.out.println(triplet1.min());
        System.out.println(triplet1.sum());
        System.out.println(triplet1.toString());
    }
}
