package homeworks.homework1;
import java.util.*;
/*Вычислите и выведите на экран объем шара, получив его радиус r с консоли.

        Подсказка: считать по формуле V  =  4/3 * pi * r^3. Значение числа pi взять из Math.

        Ограничения:
        0 < r < 100*/

public class Task1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        double radius = scan.nextInt();
        double size = (4.0 / 3) * Math.PI * Math.pow(radius, 3);

        System.out.println(size);
    }
}
