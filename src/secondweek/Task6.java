package secondweek;

import java.util.*;
/*
Дан символ, поменять со строчного на заглавный или с заглавного на строчный

Входные данные
d
Выходные данные
D
Входные данные
C
Выходные данные
a
 */
//используем ASKI код
public class Task6 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String symbol = scan.next();
        char c = symbol.charAt(0);

        if (c >= 'a' && c <= 'z') {
            System.out.println((char)(c+('A'-'a')));
        }
        else {
            System.out.println((char)(c-('A'-'a')));
        }
    }
}
