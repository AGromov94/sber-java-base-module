package homeworks.homework10.task2;

import homeworks.homework10.task1.IsLike;
import homeworks.homework10.task1.IsReallyLike;
import jdk.jfr.Period;

import java.lang.annotation.Annotation;
import java.util.Arrays;

/*2.	Написать метод, который рефлексивно проверит наличие аннотации @IsLike на любом переданном классе и выведет значение,
        хранящееся в аннотации, на экран.*/
@IsLike(false)
@IsReallyLike // просто потестить добавление нескольких аннотаций для класса
public class Task2 {
    public static void main(String[] args) {
        checkAnnotationInClass(Task2.class);
    }

    public static void checkAnnotationInClass(Class<?> c) {
        if (!c.isAnnotationPresent(IsLike.class)) {
            System.out.println("У данного класса нет аннотации \"IsLike\"");
        } else {
            System.out.println("У класса присутствует аннотация \"IsLike\"");
            IsLike isLike = c.getAnnotation(IsLike.class);
            System.out.println("Значение value в аннотации равно: " + isLike.value());
        }
    }
}
