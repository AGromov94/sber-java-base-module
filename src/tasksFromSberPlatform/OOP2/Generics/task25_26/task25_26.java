package tasksFromSberPlatform.OOP2.Generics.task25_26;

import tasksFromSberPlatform.OOP2.AbstractClassesAndInterfaces.NewGeometricObject.NewCircle;

/*      Реализуйте поиск наибольшего элемента в массиве с помощью метода compareTo() интерфейса Comparable.
        Определите класс Circle с полем radius и найдите наибольший элемент в массиве экземпляров этого класса.*/

//      Реализуйте поиск наибольшего элемента в двумерном массиве с помощью метода compareTo() интерфейса Comparable.
public class task25_26 {
    public static void main(String[] args) {
        NewCircle[] array = {new NewCircle(1.0), new NewCircle(4.0), new NewCircle(10.0)};
        System.out.println(max(array));

        NewCircle[][] array1 = {{new NewCircle(1.0), new NewCircle(4.0)}, {new NewCircle(10.0), new NewCircle(12.0)}};
        System.out.println(max(array1));
    }

    public static <E extends Comparable<E>> E max(E[] array) {
        E max = array[0];
        for (int i = 1; i < array.length; i++) {
            if (max.compareTo(array[i]) < 0) {
                max = array[i];
            }
        }
        return max;
    }

    public static <E extends Comparable<E>> E max(E[][] array1) {
        E max = array1[0][0];
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array1[i].length; j++) {
                if (max.compareTo(array1[i][j]) < 0) {
                    max = array1[i][j];
                }
            }
        }
        return max;
    }
}
