package homeworks.homework9.task8;

import java.util.*;

/*8.	С консоли на вход подается две строки s и t. Необходимо вывести true,
        если одна строка является валидной анаграммой другой строки и false иначе. Анаграмма — это слово или фраза,
        образованная путем перестановки букв другого слова или фразы, обычно с использованием всех исходных букв ровно один раз.*/
public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s1 = scan.nextLine();
        String s2 = scan.nextLine();

        System.out.println(isAnagram(s1, s2));
    }

    public static boolean isAnagram(String s1, String s2) {
        if (s1.length() != s2.length()) {
            return false;
        }
        char[] array1 = s1.replace(" ", "").toCharArray();
        char[] array2 = s2.replace(" ", "").toCharArray();

        Arrays.sort(array1);
        Arrays.sort(array2);

        for (int i = 0; i < array1.length; i++) {
            if (array1[i] != array2[i]) {
                return false;
            }
        }
        return true;
    }
}