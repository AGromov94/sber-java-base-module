package tasksFromSberPlatform.OOP1.String_and_Character.task4;

import java.util.ArrayList;
import java.util.Arrays;

/*Метод split() класса String возвращает массив строк, который содержит подстроки, отделенные разделителями.
        Однако сами разделители не возвращаются. Реализуйте следующий новый метод split(), который возвращает массив строк,
        содержащий подстроки, отделенные совпадающими разделителями, включая сами разделители.

public static String[] split(String s, String regex)
        Например, split("ab#12#453", "#") возвращает ab, #, 12, # и 453 в массиве строк, а split("a?b?gf#e", "[?#]")
        возвращает a, ?, b, ?, gf, # и e в массиве строк.*/
public class Task4 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(mySplit("a?b?gf#e", "[?#]")));
    }

    public static String[] mySplit(String s, String regex) {
        int counter = 0;
        String[] array = s.split(regex);
        ArrayList<String> list = new ArrayList();
        for (int i = 0; i < array.length; i++) {
            list.add(array[i]);
            list.add(regex);
        }
        if (list.get(list.size() - 1).equals(regex)) {
            list.remove(list.size() - 1);
        }
        return list.toArray(new String[0]);
    }
}
