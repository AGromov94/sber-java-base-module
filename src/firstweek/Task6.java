package firstweek;
import java.util.*;
/*
    Дано двузначное число. Вывести сначала левую цифру (единицы), затем правую (десятки)
 */
public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int c = scanner.nextInt();

        String str = Integer.toString(c);
        System.out.println(str.substring(0,1));
        System.out.println(str.substring(1,2));
    }
}
