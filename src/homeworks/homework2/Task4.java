package homeworks.homework2;

import java.util.*;
/*После вкусного обеда Петя принимается за подсчет дней до выходных. Календаря под рукой не оказалось, а если спросить у коллеги Феди,
то тот называет только порядковый номер дня недели, что не очень удобно. Поэтому Петя решил написать программу,
которая по порядковому номеру дня недели выводит сколько осталось дней до субботы. А если же сегодня шестой (суббота) или седьмой (воскресенье) день,
то программа выводит "Ура, выходные!"
        Ограничения:
        1 <= n <= 7*/

public class Task4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int numberOfDay = scan.nextInt();

        if (numberOfDay >= 1 && numberOfDay <= 5) {
            System.out.println(6 - numberOfDay);
        } else {
            System.out.println("Ура, выходные!");
        }
    }
}
