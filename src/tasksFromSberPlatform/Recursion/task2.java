package tasksFromSberPlatform.Recursion;

import java.util.*;

public class task2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.nextLine();
        System.out.print(safeCardNumber(s));
    }

    public static String safeCardNumber(String s) {
        if (s.length() <= 4) {
            return s;
        } else {
            return "*" + safeCardNumber(s.substring(1, s.length()));
        }
    }
}
