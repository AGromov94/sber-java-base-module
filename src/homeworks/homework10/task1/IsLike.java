package homeworks.homework10.task1;
//1.	Создать аннотацию @IsLike, применимую к классу во время выполнения программы. Аннотация может хранить boolean значение.

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface IsLike {
    boolean value();
}
