package seventhweek.task3;

public enum WeekDays {
    MONDAY(1, "Понедельник"),
    TUESDAY(2, "Вторник"),
    WEDNESDAY(3, "Среда"),
    THURSDAY(4, "Четверг"),
    FRIDAY(5, "Пятница"),
    SATURDAY(6, "Суббота"),
    SUNDAY(7, "Воскресенье"),
    NOT_A_DAY(-1, "Такого дня нет");


    public final int dayNumber;
    public final String name;
    private static final WeekDays[] ALL_DAYS = values();

    WeekDays(int dayNumber, String name) {
        this.dayNumber = dayNumber;
        this.name = name;
    }

    public static WeekDays ofNumber(int dayNumber) {
        for (WeekDays day : ALL_DAYS) {
            if (day.dayNumber == dayNumber) {
                return day;
            }
        }
        return NOT_A_DAY;
    }

    public static WeekDays ofName(String name) {
        for (WeekDays day : ALL_DAYS) {
            if (day.name.equalsIgnoreCase(name)) {
                return day;
            }
        }
        return NOT_A_DAY;
    }
}
