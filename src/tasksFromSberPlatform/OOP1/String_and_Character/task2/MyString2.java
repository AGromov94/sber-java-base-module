package tasksFromSberPlatform.OOP1.String_and_Character.task2;
/*Java-библиотека предоставляет класс String. Предоставьте собственную реализацию следующих методов (назовите новый класс MyString2):

public MyString2(char[] chars);
public MyString2 substring(int begin);
public MyString2 toUpperCase();
public char[] toChars();
public static MyString2 valueOf(boolean b);*/

public class MyString2 {
    private char arr[];

    public MyString2(char[] chars) {
        this.arr = new char[chars.length];
        System.arraycopy(chars, 0, arr, 0, chars.length);
    }

    public MyString2 substring(int begin) {
        char[] newArr = new char[arr.length - begin];
        System.arraycopy(arr, begin, newArr, 0, arr.length - begin);
        return new MyString2(newArr);
    }

    public MyString2 toUpperCase() {
        char[] newArr = new char[arr.length];
        for (int i = 0; i < arr.length; i++) {
            newArr[i] = Character.toUpperCase(arr[i]);
        }
        return new MyString2(newArr);
    }

    public char[] toChars() {
        return arr;
    }

    public static MyString2 valueOf(boolean b) {
        if (b) {
            char[] newArr = new char[]{'t', 'r', 'u', 'e'};
            return new MyString2(newArr);
        } else {
            char[] newArr = new char[]{'f', 'a', 'l', 's', 'e'};
            return new MyString2(newArr);
        }
    }
}
