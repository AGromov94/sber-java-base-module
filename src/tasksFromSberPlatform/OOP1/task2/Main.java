package tasksFromSberPlatform.OOP1.task2;

public class Main {
    public static void main(String[] args) {
        Stock sber = new Stock("SBER", "ПАО Сбербанк", 281.5, 282.87);
        System.out.println("Процент изменения стоимости акций равен " + sber.getChangePercent() + "%");
    }
}
