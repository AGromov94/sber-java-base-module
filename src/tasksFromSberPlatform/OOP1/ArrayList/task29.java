package tasksFromSberPlatform.OOP1.ArrayList;

import java.util.*;

/*      Напишите метод, который удаляет повторяющиеся элементы из целочисленного списка типа ArrayList, используя следующий заголовок:

        public static void removeDuplicate(ArrayList<Integer> list)
        Напишите тестовую программу, которая запрашивает у пользователя 10 целых чисел для списка,
        отображает несовпадающие целые числа в порядке их ввода и отделяет их друг от друга только одним пробелом.

        Пример выполнения программы:

        Введите десять целых чисел: 34 5 3 5 6 4 33 2 2 4
        Несовпадающие целые числа равны 34 5 3 6 4 33 2*/
public class task29 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        ArrayList<Integer> list = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            System.out.println("Введите чило номер " + (i + 1));
            list.add(scan.nextInt());
        }

        removeDuplicate(list);
        print(list);
    }

    public static void removeDuplicate(ArrayList<Integer> list) {
        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = i + 1; j < list.size(); j++) {
                if (list.get(i) == list.get(j)) {
                    list.remove(j);
                    j--;
                }
            }
        }
    }

    public static void print(ArrayList<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i) + " ");
        }
    }
}
