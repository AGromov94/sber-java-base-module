package homeworks.homework6.task4;

public class Main {
    public static void main(String[] args) {
        TimeUnit time = new TimeUnit(23,50,0);
        time.printCurrentTime();
        System.out.println();
        time.timeAdditionAndPrint(1,30,30);
        time.printCurrentTimeInTwelveHoursFormat();
    }
}
