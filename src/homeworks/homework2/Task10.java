package homeworks.homework2;

import java.util.*;
/*"А логарифмическое?" - не унималась дочь.

        Напишите программу, которая проверяет, что log(e^n) == n для любого вещественного n.

        Ограничения:
        -500 < n < 500*/

public class Task10 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double n = scan.nextDouble();
        System.out.println(Math.log(Math.pow(Math.E, n)) == n);
    }
}
