package homeworks.homework2;

import java.util.*;
/*Петя пришел домой и помогает дочке решать математику. Ей нужно определить, принадлежит ли точка с указанными координатами первому квадранту.
 Недолго думая, Петя решил автоматизировать процесс и написать программу:
 на вход нужно принимать два целых числа (координаты точки), выводить true, когда точка попала в квадрант и false иначе.
 Но сначала Петя вспомнил, что точка лежит в первом квадранте тогда, когда её координаты удовлетворяют условию: x > 0 и y > 0.

        Ограничения:
        -100 < x, y < 100*/

public class Task2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int x = scan.nextInt();
        int y = scan.nextInt();

        System.out.println(checkingCoordinates(x, y));
    }

    public static boolean checkingCoordinates(int x, int y) {
        if (x > 0 && y > 0) {
            return true;
        }
        return false;
    }
}
