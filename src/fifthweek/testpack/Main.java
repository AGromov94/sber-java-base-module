package fifthweek.testpack;

import java.util.Scanner;

//На вход передается N — высота двумерного массива и M — его ширина.
//        Затем передается сам массив.
//        Необходимо сохранить в одномерном массиве суммы чисел каждого столбца и вывести их на экран.
//        Пример:
//        Входные данные
//        2 2
//        10 20
//        5 7
//        Выходные данные
//        15 27
//        Входные данные
//        3 1
//        30
//        42
//        15
//        Выходные данные
//        87
public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int N = scan.nextInt();
        int M = scan.nextInt();
        int[][] arr = new int[N][M];
        int[] resultArr = new int[M];

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                arr[i][j] = scan.nextInt();
            }
        }

        for (int i = 0; i < M; i++) {
            int sum = 0;
            for (int j = 0; j < N; j++) {
                sum += arr[j][i];
            }
            resultArr[i] = sum;
        }

        for (int j : resultArr) {
            System.out.print(j + " ");
        }
    }
}
