package tasksFromSberPlatform.OOP2.Generics.task23;
/*      Напишите метод, которому передается коллекция объектов типа ArrayList, а возвращается коллекция ArrayList, но уже без дубликатов.
        Необходимо использовать метод contains() интерфейса List.*/

import java.util.ArrayList;
import java.util.List;

public class task23 {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(2);
        list.add(2);
        list.add(3);
        list.add(3);

        System.out.println(removeDuplicates(list).toString());

    }

    public static <E> ArrayList<E> removeDuplicates(ArrayList<E> list) {
        ArrayList<E> newList = new ArrayList<E>();
        for (E e : list) {
            if (!newList.contains(e)) {
                newList.add(e);
            }
        }
        return newList;
    }
}
