package fifthweek;

import java.util.Scanner;
/*
Транспонировать матрицу.
Транспонирование — в линейной алгебре это операция над матрицами в результате которой
матрица поворачивается относительно своей главной диагонали.
При этом столбцы исходной матрицы становятся строками результирующей.

Входные данные
4 2 - размерность массива
Сам массив:
1 2
3 4
5 6
7 8
Выходные данные
1 3 5 7
2 4 6 8

Входные данные
2 2 - размерность массива
Сам массив:
2 3
4 5
Выходные данные
2 4
3 5
     */
public class Task9 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int m = scan.nextInt();
        int[][] matrix = new int[n][m];
        int[][] transpondMatrix = new int[m][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                matrix[i][j] = scan.nextInt();
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                System.out.print("[" + matrix[i][j] + "]");
            }
            System.out.println();
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                transpondMatrix[i][j] = matrix[j][i];
            }
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print("[" + transpondMatrix[i][j] + "]");
            }
            System.out.println();
        }
    }
}
