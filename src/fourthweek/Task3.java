package fourthweek;

import java.util.*;

/*
На вход подается число N — длина массива.
Затем передается массив целых чисел длины N.

Проверить, является ли он отсортированным массивом строго по убыванию.
Если да, вывести true, иначе вывести false.

5
5 4 3 2 1
->
true

2
43 46
->
false

3
5 5 5
->
false
 */
public class Task3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[] arr = new int[n];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = scan.nextInt();
        }
        System.out.println(checkArr(arr));

    }

    static boolean checkArr(int[] inputArray, int n) {
        for (int i = 0; i < n - 1; i++) {
            if (inputArray[i] <= inputArray[i + 1]) {
                return false;
            }
        }
        return true;
    }

    static boolean checkArr(int[] inputArray) {
        return checkArr(inputArray, inputArray.length);
    }
}
