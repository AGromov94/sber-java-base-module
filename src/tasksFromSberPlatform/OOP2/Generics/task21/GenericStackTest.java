package tasksFromSberPlatform.OOP2.Generics.task21;

public class GenericStackTest {
    public static void main(String[] args) {
        GenericStack<Integer> stack = new GenericStack<>(2);
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        stack.pop();
        stack.pop();
        stack.push(9);
        System.out.println(stack.peek());
        System.out.println(stack.isEmpty());
        System.out.println(stack.getSize());
        System.out.println(stack.toString());
    }
}
