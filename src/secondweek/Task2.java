package secondweek;

import java.util.*;

/*
Дано число n.
Если оно четное и больше либо равно 0, то вывести “Четное больше или равно 0”.
Если четное и меньше 0, то вывести “Четное меньше 0”.
 */
public class Task2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        if (n % 2 == 0 && n >= 0) {
            System.out.println("Число четное и больше или равно 0");
        } else if (n % 2 == 0 && n < 0) {
            System.out.println("Число четное и меньше 0");
        }
//тот же код, но без повторений и со вложенным циклом
        if (n % 2 == 0) {
            if (n >= 0) {
                System.out.println("Число четное и больше или равно 0");
            } else {
                System.out.println("Число четное и меньше 0");
            }
        }
    }
}
