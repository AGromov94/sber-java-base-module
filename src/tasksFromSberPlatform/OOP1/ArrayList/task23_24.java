package tasksFromSberPlatform.OOP1.ArrayList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;
/*
1. Напишите следующий метод, который возвращает наибольшее значение в целочисленном списке типа ArrayList.
   Этот метод должен возвращать значение null, если список пустой или его размер равен 0.
2. Напишите следующий метод, который перетасовывает элементы целочисленного списка типа ArrayList:
*/

public class task23_24 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        ArrayList<Integer> list = new ArrayList<>();
        System.out.println("Введите следующий элемент списка, закончив список 0");

        int number;
        do {
            number = scan.nextInt();
            if (number != 0) {
                list.add(number);
            }
        } while (number != 0);

        System.out.println(max(list));
        shuffle(list);
        System.out.println(list);
    }

    public static Integer max(ArrayList<Integer> list) {
        if (list.isEmpty()) {
            return null;
        } else {
            int max = list.get(0);
            for (int i = 1; i < list.size(); i++) {
                if (list.get(i) > max) {
                    max = list.get(i);
                }
            }
            return max;
        }
    }

    public static void shuffle(ArrayList<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            int randomIndex = (int) (Math.random() * list.size());
            int temp = list.get(i);
            list.set(i, list.get(randomIndex));
            list.set(randomIndex, temp);
        }
    }
}
