package tasksFromSberPlatform.OOP1.task4;

/*Создайте класс с именем StopWatch для представления секундомера. Класс StopWatch должен содержать:

        Скрытые поля данных startTime и endTime с getter-методами.
        Безаргументный конструктор, который инициализирует startTime с текущим временем.
        Метод с именем start(), который сбрасывает startTime до текущего времени.
        Метод с именем stop(), который присваивает endTime текущее время.
        Метод с именем getElapsedTime(), который возвращает прошедшее время на секундомере в миллисекундах.
        Нарисуйте UML-диаграмму класса StopWatch, а затем реализуйте этот класс. Напишите клиент этого класса – программу,
        которая вычисляет время выполнения сортировки 100 000 чисел методом выбора.*/
public class StopWatch {
    private long startTime;
    private long endTime;

    public StopWatch() {
        this.startTime = System.currentTimeMillis();
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void start() {
        startTime = System.currentTimeMillis();
    }

    public void stop() {
        endTime = System.currentTimeMillis();
    }

    public long getElapsedTime() {
        return endTime - startTime;
    }
}
