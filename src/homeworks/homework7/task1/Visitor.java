package homeworks.homework7.task1;

public class Visitor {
    private String name;
    private Integer id;
    private String nameOfBook;

    public Visitor(String name, Integer id) {
        this.name = name;
        this.id = id;
        this.nameOfBook = null;
    }

    public Visitor(String name, Integer id, String nameOfBook) {
        this.name = name;
        this.id = id;
        this.nameOfBook = nameOfBook;
    }

    //region get/set
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameOfBook() {
        return nameOfBook;
    }

    public void setNameOfBook(String nameOfBook) {
        this.nameOfBook = nameOfBook;
    }
    //endregion
}
