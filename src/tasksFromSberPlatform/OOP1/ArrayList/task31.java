package tasksFromSberPlatform.OOP1.ArrayList;

import java.util.*;

/*      Напишите метод, который возвращает из строки символьный массив типа ArrayList, используя следующий заголовок:

        public static ArrayList<Character> toCharacterArray(String s)
        Например, toCharacterArray("abc") возвращает список, содержащий символы 'a', 'b' и 'c'.*/
public class task31 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите строку");
        String str = scan.nextLine();

        print(toCharacterArray(str));
    }

    public static ArrayList<Character> toCharacterArray(String s) {
        ArrayList<Character> list = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {
            list.add(s.charAt(i));
        }
        return list;
    }

    public static void print(ArrayList<Character> list) {
        for (Character character : list) {
            System.out.print("'" + character + "'" + " ");
        }
    }
}
