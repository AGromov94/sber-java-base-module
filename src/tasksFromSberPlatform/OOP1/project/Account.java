package tasksFromSberPlatform.OOP1.project;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/*      Создайте класс с именем Account для представления банковского счета. Класс Account должен содержать:

        Скрытое поле данных типа int с именем id (идентификатор) счета (по умолчанию равное 0).
        Скрытое поле данных типа double с именем balance (остаток, баланс) счета (по умолчанию равное 0).
        Скрытое поле данных типа double с именем annualInterestRate, в котором хранится годовая процентная ставка (по умолчанию равная 0).
        Пусть у всех счетов она будет одинаковая.
        Скрытое поле данных типа Date с именем dateCreated, в котором хранится дата создания счета.
        Безаргументный конструктор, который создает счет с заданными по умолчанию значениями.
        Конструктор, который создает счет с указанными id и balance.
        Getter и setter-методы для id, balance и annualInterestRate.
        Getter-метод для dateCreated.
        Метод с именем getMonthlyInterest(), который возвращает ежемесячный процент.
        Метод с именем withdraw(), который снимает со счета указанную сумму.
        Метод с именем deposit(), который пополняет счет на указанную сумму.
        Нарисуйте UML-диаграмму класса Account, а затем реализуйте этот класс. (Подсказка: метод getMonthlyInterest() предназначен для
        возврата ежемесячных процентов, а не процентной ставки. Ежемесячные проценты = balance * ежемесячная процентная ставка.
        Ежемесячная процентная ставка = annualInterestRate / 12. Обратите внимание, что значение annualInterestRate измеряется в
        процентах, например, 4.5%, поэтому необходимо делить его на 100.)

        Напишите клиент этого класса — программу, которая создает объект типа Account с ID счета, равным 1122, балансом,
        равным 20 000 руб., и годовой процентной ставкой, равной 4.5%. Используйте метод withdraw(), чтобы снять 2 500 руб.,
        метод deposit(), чтобы положить 3 000 руб. и отобразите в консоли баланс, ежемесячные проценты и дату создания этого счета.*/
public class Account {
    private int id = 0;
    private String name;
    private double balance = 0;
    private static double annualInterestRate = 0;
    private Date dateCreated;
    private ArrayList<Transaction> transactions = new ArrayList<>();


    public Account() {
        this.dateCreated = new Date();
    }

    public Account(int id, double balance) {
        this.id = id;
        this.balance = balance;
        this.dateCreated = new Date();
    }

    public Account(String name, int id, double balance) {
        this.name = name;
        this.id = id;
        this.balance = balance;
        this.dateCreated = new Date();
    }

    //region get/set
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public static double getAnnualInterestRate() {
        return annualInterestRate;
    }

    public static void setAnnualInterestRate(double annualInterestRate) {
        Account.annualInterestRate = annualInterestRate;
    }

    public Date getDateCreated() {
        return dateCreated;
    }
    //endregion

    public double getMonthlyInterest() {
        return balance * (annualInterestRate / 100 / 12);
    }

    public void withdraw(double amount) {
        balance -= amount;
        transactions.add(new Transaction('-', amount, balance, "Произведено снятие со счета " + amount + " рублей"));
    }

    public void deposit(double amount) {
        balance += amount;
        transactions.add(new Transaction('+', amount, balance, "Произведено пополнение счета на " + amount + " рублей"));
    }

    public void printHistoryOfOperations() {
        int counter = 1;
        for (Transaction t : transactions
        ) {
            System.out.println(counter + "." + " " + t.toString());
            counter++;
        }
    }
}
