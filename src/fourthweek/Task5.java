package fourthweek;

import java.util.*;

/*
   На вход подается число N - длина массива.
   Затем подается массив целых чисел из N элементов.

   Нужно циклически сдвинуть элементы на 1 влево.

   Входные данные:
   5
   1 2 3 4 7
   Выходные данные:
   2 3 4 7 1
   */
public class Task5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[] arr = new int[n];
        int[] newArr = new int[n];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = scan.nextInt();
        }
        for (int i = 0; i < arr.length - 1; i++) {
            newArr[i] = arr[i + 1];
        }
        newArr[n - 1] = arr[0];
        System.out.println(Arrays.toString(newArr));

        //решение через arraycopy
        int first = arr[0];
        System.arraycopy(arr, 1, arr, 0, arr.length - 1);
        arr[arr.length - 1] = first;
        System.out.println(Arrays.toString(arr));
    }
}
