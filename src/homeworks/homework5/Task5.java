package homeworks.homework5;

import java.util.*;
/*5.	На вход подается число N — количество строк и столбцов матрицы.
        Затем передается сама матрица, состоящая из натуральных чисел.

        Необходимо вывести true, если она является симметричной относительно побочной диагонали, false иначе.

        Побочной диагональю называется диагональ, проходящая из верхнего правого угла в левый нижний.

        Ограничения:
        ●	0 < N < 100
        ●	0 < ai < 1000
        Пример входных данных:
        3
        57 190 160 71 42
        141 79 187 19 71
        141 16 7 187 160
        100 42 16 79 190
        15 100 141 141 57
*/

public class Task5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[][] matrix = new int[n][n];
        String str1 = "";
        String str2 = "";

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = scan.nextInt();
            }
        }

        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                str1 += matrix[i][j] + " ";
            }
        }

        for (int i = n - 1; i > 0; i--) {
            for (int j = n - 1; j > n - i - 1; j--) {
                str2 += matrix[j][i] + " ";
            }
        }
        System.out.println(str1.equals(str2));
    }
}
