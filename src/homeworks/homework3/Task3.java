package homeworks.homework3;

import java.util.*;

/*3. На вход подается два положительных числа m и n. Необходимо вычислить m^1
        + m^2 + ... + m^n
        Ограничения:
        0 < m, n < 10*/
public class Task3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int m = scan.nextInt();
        int n = scan.nextInt();
        int sum = 0;

        for (int i = 1; i <= n; i++) {
            sum += Math.pow(m,i);
        }
        System.out.println(sum);
    }
}
