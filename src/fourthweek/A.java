package fourthweek;

import java.util.Scanner;

//На вход подается число N - длина массива.
//        Затем подается массив целых чисел из N элементов.
//
//        Нужно циклически сдвинуть элементы на 1 влево.
//
//        Входные данные:
//        5
//        1 2 3 4 7
//        Выходные данные:
//        2 3 4 7 1
public class A {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scan.nextInt();
        }

        int[] resultArr = new int[arr.length];

//        for (int i = 0; i < arr.length - 1; i++) {
//            resultArr[i] = arr[i + 1];
//        }
//        resultArr[resultArr.length-1] = arr[0];

        System.arraycopy(arr, 1, resultArr, 0, arr.length - 1);
        System.arraycopy(arr, 0, resultArr, arr.length - 1, 1);

        System.out.println("Было:");
        for (int j : arr) {
            System.out.print(j + " ");
        }

        System.out.println("Стало:");
        for (int j : resultArr) {
            System.out.print(j + " ");
        }
    }
}
