package tasksFromSberPlatform.OOP2.Exceptions.task1_and_2_and_5;

import java.util.*;

/*Напишите программу, которая запрашивает у пользователя два целых числа и отображает их сумму.
  В случае некорректного ввода необходимо его обработать и повторно запросить у пользователя числа.*/
public class task5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int num1 = 0, num2 = 0;
        boolean marker = true;

        while (marker) {
            System.out.println("Введите значения двух чисел");
            try {
                num1 = scan.nextInt();
                num2 = scan.nextInt();
                marker = false;
            } catch (InputMismatchException e) {
                e.printStackTrace();
                System.out.println("Некорректный ввод!");
                scan.nextLine();
            }
            System.out.println(num1 + num2);
        }
    }
}
