package tasksFromSberPlatform.OOP1.String_and_Character.task2;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        MyString2 testString1 = new MyString2(new char[]{'W', 'e', 'l', 'c', 'o', 'm', 'e'});

        System.out.println(Arrays.toString(testString1.substring(1).toChars()));
        System.out.println(Arrays.toString(MyString2.valueOf(false).toChars()));
    }
}
