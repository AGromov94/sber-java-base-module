package homeworks.homework11;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

//4.	На вход подается список вещественных чисел. Необходимо отсортировать их по убыванию.
public class Task4 {
    public static void main(String[] args) {
        List<Double> list = List.of(1.12, 0.7656, 0.0002, 14.0, 5.5, 2.5);
        List<Double> sortedList = list.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
        sortedList.forEach(i -> System.out.print(i + " "));
    }
}
