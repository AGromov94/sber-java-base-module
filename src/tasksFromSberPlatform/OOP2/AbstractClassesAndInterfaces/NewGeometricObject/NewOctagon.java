package tasksFromSberPlatform.OOP2.AbstractClassesAndInterfaces.NewGeometricObject;

import static java.lang.StrictMath.sqrt;
import static java.lang.StrictMath.subtractExact;

/*      Напишите класс с именем Octagon (восьмиугольник),
        который наследуется от GeometricObject и реализует интерфейсы Comparable и Cloneable.
        Предположим, что все восемь сторон восьмиугольника имеют одинаковую длину.
        Площадь (area) можно вычислить по следующей формуле:

        У класса Octagon есть private-поле типа double с именем side и соответствующие ему getter- и setter-методы.
        Также у этого класса есть безаргументный конструктор для создания восьмиугольника со стороной,
        равной 0, и конструктор для создания восьмиугольника с указанной стороной.

        Нарисуйте UML-диаграмму, включающую Octagon, GeometricObject, Comparable и Cloneable.
        Напишите тестовую программу, которая создает объект типа Octagon со значением side, равным 5,
        и отображает его площадь и периметр. Создайте новый объект с помощью метода clone() и сравните два объекта
        с помощью метода compareTo().*/
public class NewOctagon extends NewGeometricObject implements Cloneable {
    private double side;

    public NewOctagon(String color, boolean filled, double side) {
        super(color, filled);
        this.side = side;
    }

    public NewOctagon(double side) {
        this.side = side;
    }

    public NewOctagon() {
        this.side = 0;
    }

    public double getSide() {
        return side;
    }

    @Override
    public double getArea() {
        return (2 + 4 / sqrt(2)) * side * side;
    }

    @Override
    public double getPerimeter() {
        return side * 8;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public int compareTo(NewGeometricObject o) {
        if (o instanceof NewOctagon) {
            return Double.compare(this.side, ((NewOctagon) o).side);
        } else {
            return super.compareTo(o);
        }
    }
}
