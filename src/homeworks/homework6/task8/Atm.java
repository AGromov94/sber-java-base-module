package homeworks.homework6.task8;
/*8.	Реализовать класс “банкомат” Atm.
        Класс должен:
        ●	Содержать конструктор, позволяющий задать курс валют перевода долларов в рубли и курс валют перевода рублей в доллары
        (можно выбрать и задать любые положительные значения)
        ●	Содержать два публичных метода, которые позволяют переводить переданную сумму рублей в доллары и долларов в рубли
        ●	Хранить приватную переменную счетчик — количество созданных инстансов класса Atm и публичный метод, возвращающий этот счетчик
        (подсказка: реализуется через static)*/

public class Atm {
    private double roublesPerDollars;
    private double dollarsPerRoubles;
    private static int counter = 0;

    public Atm(CurrencyUnit currencyUnit, double currentCourse) {
        if (currencyUnit == currencyUnit.DOLLARS) {
            roublesPerDollars = currentCourse;
        } else if (currencyUnit == currencyUnit.ROUBLES) {
            dollarsPerRoubles = currentCourse;
        }
        if (currentCourse <= 0) {
            System.out.println("Вы ввели не верное значение курса");
        }
        counter++;
    }

    public double fromDollarsToRoubles(double amountOfDollars) {
        return amountOfDollars * roublesPerDollars;
    }

    public double fromRoublesToDollars(double amountOfRoubles) {
        return amountOfRoubles * dollarsPerRoubles;
    }

    public static int returnCounterOfInstance() {
        return counter;
    }
}
