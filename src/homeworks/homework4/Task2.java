package homeworks.homework4;

import java.util.*;
/*2.	(1 балл) На вход подается число N — длина массива. Затем передается массив целых чисел (ai) из N элементов.
        После этого аналогично передается второй массив (aj) длины M.

        Необходимо вывести на экран true, если два массива одинаковы (то есть содержат одинаковое количество элементов и
        для каждого i == j элемент ai == aj). Иначе вывести false.

        Ограничения:
        ●	0 < N < 100
        ●	0 < ai < 1000
        ●	0 < M < 100
        ●	0 < aj < 1000*/

public class Task2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        boolean flag = true;
        int n = scan.nextInt();
        int[] arrI = new int[n];

        for (int i = 0; i < n; i++) {
            arrI[i] = scan.nextInt();
        }

        int m = scan.nextInt();
        int[] arrJ = new int[m];

        for (int i = 0; i < m; i++) {
            arrJ[i] = scan.nextInt();
        }

        if (n != m) {
            flag = false;
        } else {
            for (int i = 0; i < n; i++) {
                if (arrI[i] != arrJ[i]) {
                    flag = false;
                    break;
                }
            }
        }
        System.out.println(flag);
    }
}
