package tasksFromSberPlatform.OOP2.Generics.task27_28_29;

import java.util.ArrayList;

/*1. Напишите следующий метод, который перетасовывает ArrayList:
     public static <E> void shuffle(ArrayList<E> list)

  2. Напишите следующий метод, который сортирует ArrayList:
     public static <E extends Comparable<E>> void sort(ArrayList<E> list)

  3. Напишите следующий метод, который возвращает наибольший элемент в ArrayList:
     public static <E extends Comparable<E>> E max(ArrayList<E> list)
     */
public class task27_28_29 {
    public static void main(String[] args) {
        ArrayList<Double> list = new ArrayList<>();
        list.add(1.0);
        list.add(2.2);
        list.add(4.0);
        list.add(7.5);
        list.add(18.5);

        System.out.println("Начальный порядок элементов:" + list);
        shuffle(list);
        System.out.println("Порядок элементов после перемешивания:" + list);
        sort(list);
        System.out.println("Порядок элементов после сортировки:" + list);
        System.out.println(max(list));
    }

    public static <E> void shuffle(ArrayList<E> list) {
        for (int i = 0; i < list.size(); i++) {
            int randomIndex = (int) (Math.random() * list.size());
            E temp = list.get(i);
            list.set(i, list.get(randomIndex));
            list.set(randomIndex, temp);
        }
    }

    public static <E extends Comparable<E>> void sort(ArrayList<E> list) {
        for (int i = 0; i < list.size() - 1; i++) {
            // Найти наименьшее значение в list[i..list.length-1]
            E currentMin = list.get(i);
            int currentMinIndex = i;

            for (int j = i + 1; j < list.size(); j++) {
                if (currentMin.compareTo(list.get(j)) > 0) {
                    currentMin = list.get(j);
                    currentMinIndex = j;
                }
            }

            // Переставить list[i] и list[currentMinIndex], если необходимо
            if (currentMinIndex != i) {
                list.set(currentMinIndex, list.get(i));
                list.set(i, currentMin);
            }
        }
    }

    public static <E extends Comparable<E>> E max(ArrayList<E> list) {
        E maxElement = list.get(0);
        for (int i = 1; i < list.size(); i++) {
            if (maxElement.compareTo(list.get(i)) < 0) {
                maxElement = list.get(i);
            }
        }
        return maxElement;
    }
}