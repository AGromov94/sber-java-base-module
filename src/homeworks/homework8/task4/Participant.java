package homeworks.homework8.task4;

public class Participant {
    private Dog dog;
    private String nameOfParticipant;
    private double[] marks = new double[3];
    private double averageOfMarks;

    public Participant(Dog dog, String nameOfParticipant, double[] marks) {
        this.dog = dog;
        this.nameOfParticipant = nameOfParticipant;
        this.marks = marks;
        this.averageOfMarks = returnAverageOfMarks();
    }

    public Participant() {
    }

    public Dog getDog() {
        return dog;
    }

    public void setDog(Dog dog) {
        this.dog = dog;
    }

    public String getNameOfParticipant() {
        return nameOfParticipant;
    }

    public void setNameOfParticipant(String nameOfParticipant) {
        this.nameOfParticipant = nameOfParticipant;
    }

    public double[] getMarks() {
        return marks;
    }

    public void setMarks(double[] marks) {
        this.marks = marks;
    }

    public double getAverageOfMarks() {
        return averageOfMarks;
    }

    public void setAverageOfMarks(double averageOfMarks) {
        this.averageOfMarks = averageOfMarks;
    }

    public double returnAverageOfMarks() {
        double sum = 0.0;
        for (int i = 0; i < 3; i++) {
            sum += marks[i];
        }
        return (int) (sum / 3 * 10) / 10.0;
    }
}
