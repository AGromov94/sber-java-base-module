package eightweek.factory;

public class VanillaIceCream implements IceCream{
    @Override
    public void printIngredients() {
        System.out.println("Ванилин, лед, молоко");
    }
}
