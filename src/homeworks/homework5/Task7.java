package homeworks.homework5;

import java.util.*;
/*7.	Раз в год Петя проводит конкурс красоты для собак. К сожалению, система хранения участников и оценок неудобная, а победителя определить надо.
        В первой таблице в системе хранятся имена хозяев, во второй - клички животных, в третьей — оценки трех судей за выступление каждой собаки.
        Таблицы связаны между собой только по индексу. То есть хозяин i-ой собаки указан в i-ой строке первой таблицы,
        а ее оценки — в i-ой строке третьей таблицы. Нужно помочь Пете определить топ 3 победителей конкурса.

        На вход подается число N — количество участников конкурса. Затем в N строках переданы имена хозяев.
        После этого в N строках переданы клички собак. Затем передается матрица с N строк, 3 вещественных числа в каждой — оценки судей.
        Победителями являются три участника, набравшие максимальное среднее арифметическое по оценкам 3 судей.
        Необходимо вывести трех победителей в формате “Имя хозяина: кличка, средняя оценка”.

        Гарантируется, что среднее арифметическое для всех участников будет различным.

        Входные данные:
        4
        Иван
        Николай
        Анна
        Дарья
        Жучка
        Кнопка
        Цезарь
        Добряш
        7 6 7
        8 8 7
        4 5 6
        9 9 9

        Ограничения:
        0 < N < 100

        Выходные данные:
        Дарья: Добряш, 9.0
        Николай: Кнопка, 7.6
        Иван: Жучка, 6.6
        */

public class Task7 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        scan.nextLine();
        String[] namesOfOwners = new String[n];
        String[] namesOfDogs = new String[n];
        int[][] marks = new int[n][3];
        double[] averagesOfmarks = new double[n];
        double[] averagesOfmarksCopy = new double[n];
        int[] indexOfmarksOfWinners = new int[3];
        double max = 0;

        for (int i = 0; i < n; i++) {
            namesOfOwners[i] = scan.nextLine();
        }

        for (int i = 0; i < n; i++) {
            namesOfDogs[i] = scan.nextLine();
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                marks[i][j] = scan.nextInt();
            }
        }

        for (int i = 0; i < n; i++) {
            int sum = 0;
            for (int j = 0; j < 3; j++) {
                sum += marks[i][j];
            }
            averagesOfmarks[i] = (int) ((sum / 3.0) * 10) / 10.0;
        }

        System.arraycopy(averagesOfmarks, 0, averagesOfmarksCopy, 0, n);

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                if (averagesOfmarks[j] > max) {
                    max = averagesOfmarks[j];
                }
            }

            for (int k = 0; k < n; k++) {
                if (max == averagesOfmarks[k]) {
                    averagesOfmarks[k] = 0;
                    indexOfmarksOfWinners[i] = k;
                    break;
                }
            }
            max = 0;
        }

        for (int i = 0; i < 3; i++) {
            System.out.println(namesOfOwners[indexOfmarksOfWinners[i]] + ":" + " " + namesOfDogs[indexOfmarksOfWinners[i]] + "," + " " + averagesOfmarksCopy[indexOfmarksOfWinners[i]]);
        }
    }
}
