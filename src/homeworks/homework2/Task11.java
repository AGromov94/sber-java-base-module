package homeworks.homework2;

import java.util.*;
/*      Разобравшись со своими (и не только) задачками, Петя уже собирался лечь спать и отдохнуть перед очередным тяжелым рабочим днем,
        но вдруг в тишине раздается детский шепот: "Паааапааа, мы забыли решить ещё одну задачку! " +
        "Давай проверим, можно ли из трех сторон составить треугольник?". Что ж, придется написать еще одну программу,
        связанную со школьной математикой.

        На вход подается три целых положительных числа – длины сторон треугольника. Нужно вывести true,
        если можно составить треугольник из этих сторон и false иначе.

        Ограничения:
        0 < a, b, c < 100*/

public class Task11 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();
        int c = scan.nextInt();

        System.out.println(checkTriangle(a, b, c));
    }

    public static boolean checkTriangle(int a, int b, int c) {
        if ((a + b > c) && (a + c > b) && (b + c > a)) {
            return true;
        }
        return false;
    }
}
