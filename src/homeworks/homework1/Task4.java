package homeworks.homework1;

import java.util.*;
/*На вход подается количество секунд, прошедших с начала текущего дня – count. Выведите в консоль текущее время в формате: часы и минуты.

        Ограничения:
        0 < count < 86400*/

public class Task4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int count = scan.nextInt();
        int currentHours = count / 3600;
        int currentMinutes = count % 3600 / 60;

        System.out.println(currentHours + " " + currentMinutes);
    }
}
