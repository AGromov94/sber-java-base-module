package sixthweek.task4;

public class Robot {
    private int x;
    private int y;
    private int direction; //0 - UP, 1 - RIGHT, 2 - BOTTOM, 3 - LEFT

    public Robot() {
        this.x = 0;
        this.y = 0;
        this.direction = 0;
    }

    public Robot(int x, int y) {
        this.x = x;
        this.y = y;
        this.direction = 0;
    }

    public Robot(int x, int y, int direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public void go() {
        System.out.println("Initial Y: " + y);
        System.out.println("Initial X: " + x);
        System.out.println("Initial direction: " + direction);
        System.out.println("--------------------------------");
        System.out.println("GO! GO! GO!");
        System.out.println("--------------------------------");

        switch (direction) {
            case 0:
                y++;
            case 1:
                x++;
            case 2:
                y--;
            case 3:
                x--;
        }
        System.out.println("STOP WALKING");
        System.out.println("--------------------------------");
        System.out.println("After walk Y: " + y);
        System.out.println("After walk X: " + x);
        System.out.println("After walk direction: " + direction);
    }

    public void turnLeft() {
        System.out.println("!!!Turning left!!!");
        System.out.println("--------------------------------");
        this.direction = (direction - 1) % 4;
    }

    public void turnRight() {
        System.out.println("!!!Turning right!!!");
        System.out.println("--------------------------------");
        this.direction = (direction + 1) % 4;
    }
}
