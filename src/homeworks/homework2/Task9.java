package homeworks.homework2;

import java.util.*;
/*Пока Петя практиковался в работе со строками, к нему подбежала его дочь и спросила:
        "А правда ли, что тригонометрическое тождество (sin^2(x)+ cos^2(x) - 1 == 0) всегда-всегда выполняется?"

        Напишите программу, которая проверяет, что при любом x на входе
        тригонометрическое тождество будет выполняться (то есть будет выводить true при любом x).

        Ограничения:
        -1000 < x < 1000*/

public class Task9 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int x = scan.nextInt();

        System.out.println((Math.pow(Math.sin(Math.toRadians(x)), 2) + Math.pow(Math.cos(Math.toRadians(x)), 2) - 1) == 0);
    }
}
