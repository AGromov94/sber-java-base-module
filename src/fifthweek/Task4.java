package fifthweek;

import java.util.*;

/*
    Найти сумму чисел а и в
    использовать в явном виде a + b - нельзя

 */
public class Task4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();
        System.out.println("Сумма введенных чисел равна " + sumRecursively(a, b));
    }

    private static int sumRecursively(int a, int b) {
        if (b == 0) {
            return a;
        }
        return sumRecursively(a + 1, b - 1);
    }
}
