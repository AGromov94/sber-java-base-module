package tasksFromSberPlatform.OOP2.Generics.task24;

/*      Реализуйте алгоритм линейного поиска элемента в массиве. При нахождении элемента необходимо вернуть его позицию в массиве.
        Если элемент не найден, то вернуть -1.*/
public class task24 {
    public static void main(String[] args) {
        String[] array = {"One", "Two", "Three"};
        System.out.println(lineSearch(array,"Two"));

        Integer[] array1 = {1, 2, 3};
        System.out.println(lineSearch(array1,2));
    }

    public static <E extends Comparable<E>> int lineSearch(E[] array, E element) { //Если убрать extends Comparable<E> то компилятор не будет проверять совместимость типов, но код будет работать
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(element)) {
                return i;
            }
        }
        return -1;
    }
}
