package homeworks.homework8.task4;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class AverageOfMarksComparator implements Comparator<Participant> {

    @Override
    public int compare(Participant o1, Participant o2) {
        if (o1.getAverageOfMarks() < o2.getAverageOfMarks()) return -1;
        if (o1.getAverageOfMarks() > o2.getAverageOfMarks()) return 1;
        return 0;
    }
}

    /**
     * Задачка с хаккерранка. Нужно написать собственный компаратор для сортировки массива Студентов
     * You are given a list of student information: ID, FirstName, and CGPA.
     * Your task is to rearrange them according to their CGPA in decreasing order.
     * If two student have the same CGPA, then arrange them according to their first name in alphabetical order.
     * If those two students also have the same first name, then order them according to their ID.
     * No two students have the same ID.
     * Input1:
     * 5
     * 3
     * akashs
     * 3,69
     * 30
     * shayla
     * 3,80
     * 32
     * anik
     * 3,68
     * 2
     * swapnil
     * 3,69
     * 300
     * towhid
     * 3,9
     * Output1:
     * towhid
     * shayla
     * akashs
     * swapnil
     * anik
     * =============================================================
     *Input2:
     * 5
     * 33
     * Rumpa
     * 3,68
     * 85
     * Ashis
     * 3,85
     * 56
     * Samiha
     * 3,75
     * 19
     * Samara
     * 3,75
     * 22
     * Fahim
     * 3,76
     * Output2:
     * Ashis
     * Fahim
     * Samara
     * Samiha
     * Rumpa
     */
/*
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int testCases = Integer.parseInt(in.nextLine());

        List<tenthweek.reflection.superclass.Student> studentList = new ArrayList<>();
        while (testCases > 0) {
            int id = in.nextInt();
            String fname = in.next();
            double cgpa = in.nextDouble();

            tenthweek.reflection.superclass.Student st = new tenthweek.reflection.superclass.Student(id, fname, cgpa);
            studentList.add(st);

            testCases--;
        }

        Comparator<tenthweek.reflection.superclass.Student> studentComparator = new tenthweek.reflection.superclass.StudentComparator();
        studentList.sort(studentComparator);

        for (tenthweek.reflection.superclass.Student st : studentList) {
            System.out.println(st.getFname());
        }
    }
}

  class Student {
    private int id;
    private String fname;
    private double cgpa;

    public Student(int id, String fname, double cgpa) {
        super();
        this.id = id;
        this.fname = fname;
        this.cgpa = cgpa;
    }

    public int getId() {
        return id;
    }

    public String getFname() {
        return fname;
    }

    public double getCgpa() {
        return cgpa;
    }
}

class StudentComparator implements Comparator<tenthweek.reflection.superclass.Student> {
    @Override
    public int compare(tenthweek.reflection.superclass.Student s1, tenthweek.reflection.superclass.Student s2) {
        if (s2.getCgpa() != s1.getCgpa()) {
            return Double.compare(s2.getCgpa(), s1.getCgpa());
        } else {
            if (s1.getFname().compareTo(s2.getFname()) != 0) {
                return s1.getFname().compareTo(s2.getFname());
            } else {
                return Integer.compare(s1.getId(), s2.getId());
            }
        }
    }*/