package tasksFromSberPlatform.OOP1.task3;

import java.util.Date;

/*      Напишите программу, которая создает объект Date, устанавливает у него прошедшее время, равным
        10000, 100000, 1000000, 10000000, 100000000, 1000000000, 10000000000 и 100000000000,
        и отображает дату и время с помощью метода toString(), соответственно.*/
public class Task3_1 {
    public static void main(String[] args) {
        long milliseconds = 10000;
        for (int i = 0; i < 8; i++) {
            Date date = new Date(milliseconds);
            System.out.println(date.toString());
            milliseconds *= 10;
        }
    }
}
