package homeworks.homework5;

import java.util.*;
/*1.	На вход передается N — количество столбцов в двумерном массиве и M — количество строк. Затем сам передается двумерный массив,
        состоящий из натуральных чисел.

        Необходимо сохранить в одномерном массиве и вывести на экран минимальный элемент каждой строки.

        Ограничения:
        ●	0 < N < 100
        ●	0 < M < 100
        ●	0 < ai < 1000*/

public class Task1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int m = scan.nextInt();
        int[][] arr = new int[m][n];
        int[] resultArr = new int[m];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = scan.nextInt();
            }
        }
        for (int i = 0; i < m; i++) {
            int min = arr[i][0];
            for (int j = 1; j < n; j++) {
                if (min > arr[i][j]) {
                    min = arr[i][j];
                }
            }
            resultArr[i] = min;
        }
        for (int i = 0; i < m; i++) {
            System.out.print(resultArr[i] + " ");
        }
    }
}
