package homeworks.homework7.task1;

public class Book {

    private String name;
    private String author;
    private int availabilityMark; //Если книга в данный момент находится в библиотеке - 1, если нет - 0 (одолжена)
    public double rating = 0; //0-5
    public int reviewers = 0;

    public Book(String name, String author, int availabilityMark) {
        this.name = name;
        this.author = author;
        this.availabilityMark = availabilityMark;
    }

    @Override
    public String toString() {
        return "Book{" +
                "Название книги: '" + name + '\'' +
                ", Автор: '" + author + '\'' +
                ", Отметка о наличии: " + availabilityMark +
                '}';
    }

    //region get/set
    public void setName(String name) {
        this.name = name;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setAvailabilityMark(int availabilityMark) {
        this.availabilityMark = availabilityMark;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public int getAvailabilityMark() {
        return availabilityMark;
    }
    //endregion
}
