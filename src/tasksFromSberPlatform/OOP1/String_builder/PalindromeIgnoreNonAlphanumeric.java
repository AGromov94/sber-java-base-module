package tasksFromSberPlatform.OOP1.String_builder;

import java.util.*;

/*      Напишите новую программу, которая игнорирует не буквенно-цифровые символы при проверке строки на палиндром.

        Очистите строку от не буквенно-цифровых символов. Это можно сделать, создав пустую строку типа StringBuilder,
        добавив каждый буквенно-цифровой символ строки в строку типа StringBuilder и вернув строку из нее. Чтобы проверить,
        является ли символ ch буквой или цифрой, можно использовать метод isLetterOrDigit(ch) класса Character.
        Получите новую строку, которая является обратной (реверсированной) к очищенной.
        Сравните обратную строку с очищенной с помощью метода equals().*/
public class PalindromeIgnoreNonAlphanumeric {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str = scan.nextLine();

        System.out.println(isPalindrome(str));
    }

    public static boolean isPalindrome(String s) {
        StringBuilder builder1 = new StringBuilder(filter(s));
        return builder1.toString().equals(builder1.reverse().toString());
    }

    public static String filter(String s) {
        char[] charArray = s.toCharArray();
        String filterStr = "";
        for (char c : charArray) {
            if (Character.isLetterOrDigit(c)) {
                filterStr += c;
            }
        }
        return filterStr;
    }
}
