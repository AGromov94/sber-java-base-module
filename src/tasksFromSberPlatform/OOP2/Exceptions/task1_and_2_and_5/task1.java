package tasksFromSberPlatform.OOP2.Exceptions.task1_and_2_and_5;

/*С помощью двух массивов, как показано ниже, напишите программу, которая предложит пользователю ввести целое число от 1 до 12,
а затем отобразит месяц и количество дней, соответствующие этому целому числу. Если пользователь вводит недопустимое число,
то программа должна отображать Недопустимое число с помощью перехвата ArrayIndexOutOfBoundsException.*/

import java.util.*;

public class task1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[] months = {"январь", "февраль", "март", "апрель", "май",
                "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"};

        int[] dom = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        while (true) {
            System.out.println("Введите число от 1 до 12");
            int number = scan.nextInt();

            try {
                System.out.println("Месяц: " + months[number - 1] + "\n" + "Кол-во дней: " + dom[number - 1]);
            } catch (IndexOutOfBoundsException e) {
                System.out.println("Недопустимое число");
            }
        }
    }
}
