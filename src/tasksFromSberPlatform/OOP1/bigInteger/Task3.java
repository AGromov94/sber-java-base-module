package tasksFromSberPlatform.OOP1.bigInteger;

import java.math.BigInteger;

//Напишите программу, которая находит 5 простых чисел, которые больше Long.MAX_VALUE
public class Task3 {
    public static void main(String[] args) {
        int counter = 0;
        BigInteger x = new BigInteger(String.valueOf(Long.MAX_VALUE)).add(BigInteger.ONE);

        while (counter < 5) {
            //определяем простое ли число
            if (x.isProbablePrime(1)) {
                System.out.println(x);
                counter++;
            }
            x = x.add(BigInteger.ONE);
        }
        //эксперимент)
        //System.out.println(new BigInteger("30").isProbablePrime(1));
    }
}
