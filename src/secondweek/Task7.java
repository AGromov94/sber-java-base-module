package secondweek;

import java.util.*;

/*
   Дана строка и паттерн, заменить паттерн на паттерн, состоящий из заглавных символов
   Входные данные
   Hello
   o
   Выходные данные
   HellO

   Входные данные
   Hello world
   ld
   Выходные данные
   Hello worLD
    */
public class Task7 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str = scan.nextLine();
        String pattern = scan.next();
        String upperPattern = pattern.toUpperCase();

        System.out.println(str.replaceAll(pattern,upperPattern));
    }
}
