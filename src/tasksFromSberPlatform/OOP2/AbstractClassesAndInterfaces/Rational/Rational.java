package tasksFromSberPlatform.OOP2.AbstractClassesAndInterfaces.Rational;

public class Rational extends Number implements Comparable<Rational> {
    // Поля данных для числителя и знаменателя
    private final long[] arr = {0, 1};
    //private long numerator = 0;
    //private long denominator = 1;

    /**
     * Создает рациональное число с заданными по умолчанию свойствами
     */
    public Rational() {
        this(0, 1);
    }

    /**
     * Создает рациональное число с указанным числителем и знаменателем
     */
    public Rational(long numerator, long denominator) {
        long gcd = gcd(numerator, denominator);
        this.arr[0] = (denominator > 0 ? 1 : -1) * numerator / gcd;
        this.arr[1] = Math.abs(denominator) / gcd;
    }

    /**
     * Находит НОД двух чисел
     */
    private static long gcd(long n, long d) {
        long n1 = Math.abs(n);
        long n2 = Math.abs(d);
        int gcd = 1;

        for (int k = 1; k <= n1 && k <= n2; k++) {
            if (n1 % k == 0 && n2 % k == 0)
                gcd = k;
        }

        return gcd;
    }

    /**
     * Возвращает числитель
     */
    public long getNumerator() {
        return arr[0];
    }

    /**
     * Возвращает знаменатель
     */
    public long getDenominator() {
        return arr[1];
    }

    /**
     * Прибавляет рациональное число к текущему
     */
    public Rational add(Rational secondRational) {
        long n = arr[0] * secondRational.getDenominator() +
                arr[1] * secondRational.getNumerator();
        long d = arr[1] * secondRational.getDenominator();
        return new Rational(n, d);
    }

    /**
     * Вычитает рациональное число из текущего
     */
    public Rational subtract(Rational secondRational) {
        long n = arr[0] * secondRational.getDenominator()
                - arr[1] * secondRational.getNumerator();
        long d = arr[1] * secondRational.getDenominator();
        return new Rational(n, d);
    }

    /**
     * Умножает рациональное число на текущее
     */
    public Rational multiply(Rational secondRational) {
        long n = arr[0] * secondRational.getNumerator();
        long d = arr[1] * secondRational.getDenominator();
        return new Rational(n, d);
    }

    /**
     * Делит на рациональное число текущее
     */
    public Rational divide(Rational secondRational) {
        long n = arr[0] * secondRational.getDenominator();
        long d = arr[1] * secondRational.arr[0];
        return new Rational(n, d);
    }

    @Override // Переопределяет метод toString класса Object
    public String toString() {
        if (arr[1] == 1)
            return arr[0] + "";
        else
            return arr[0] + "/" + arr[1];
    }

    @Override // Реализует абстрактный метод intValue класса Number
    public int intValue() {
        return (int) doubleValue();
    }

    @Override // Реализует абстрактный метод floatValue класса Number
    public float floatValue() {
        return (float) doubleValue();
    }

    @Override // Реализует абстрактный метод doubleValue класса Number
    public double doubleValue() {
        return arr[0] * 1.0 / arr[1];
    }

    @Override // Реализует абстрактный метод longValue класса Number
    public long longValue() {
        return (long) doubleValue();
    }

    @Override // Реализует метод compareTo интерфейса Comparable
    public int compareTo(Rational o) {
        return this.subtract(o).getNumerator() > 0 ? 1 : this.subtract(o).getNumerator() < 0 ? -1 : 0;
/*    if (this.subtract(o).getNumerator() > 0)
      return 1;
    else if (this.subtract(o).getNumerator() < 0)
      return -1;
 else
      return 0;*/
    }
}
