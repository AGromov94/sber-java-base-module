package tenthweek.reflection.superclass;

import java.util.*;

/*Получение родителей
 - метод getSuperClass() - возвращает Class родителя текущего класса
 - метод getInterfaces() - возвращает список Classов интерфейсов, реализуемых текущим классом*/
public class ReflectionSuperClass {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int testCases = Integer.parseInt(in.nextLine());

        List<Student> studentList = new ArrayList<>();
        while (testCases > 0) {
            int id = in.nextInt();
            String fname = in.next();
            double cgpa = in.nextDouble();

            Student st = new Student(id, fname, cgpa);
            studentList.add(st);

            testCases--;
        }

        Comparator<Student> studentComparator = new StudentComparator();
        studentList.sort(studentComparator);

        for (Student st : studentList) {
            System.out.println(st.getFname());
        }
    }
}

class Student {
    private int id;
    private String fname;
    private double cgpa;

    public Student(int id, String fname, double cgpa) {
        super();
        this.id = id;
        this.fname = fname;
        this.cgpa = cgpa;
    }

    public int getId() {
        return id;
    }

    public String getFname() {
        return fname;
    }

    public double getCgpa() {
        return cgpa;
    }
}

class StudentComparator implements Comparator<Student> {
    @Override
    public int compare(Student s1, Student s2) {
        if (s2.getCgpa() != s1.getCgpa()) {
            return Double.compare(s2.getCgpa(), s1.getCgpa());
        } else {
            if (s1.getFname().compareTo(s2.getFname()) != 0) {
                return s1.getFname().compareTo(s2.getFname());
            } else {
                return Integer.compare(s1.getId(), s2.getId());
            }
        }
    }
}
