package homeworks.homework6.task6;
/*6.	Необходимо реализовать класс AmazingString, который хранит внутри себя строку как массив char и предоставляет следующий функционал:
        Конструкторы:
        ●	Создание AmazingString, принимая на вход массив char
        ●	Создание AmazingString, принимая на вход String
        Публичные методы (названия методов, входные и выходные параметры продумать самостоятельно).
        Все методы ниже нужно реализовать “руками”, т.е. не прибегая к переводу массива char в String и без использования
        стандартных методов класса String.
        ●	Вернуть i-ый символ строки
        ●	Вернуть длину строки
        ●	Вывести строку на экран
        ●	Проверить, есть ли переданная подстрока в AmazingString (на вход подается массив char). Вернуть true, если найдена и false иначе
        ●	Проверить, есть ли переданная подстрока в AmazingString (на вход подается String). Вернуть true, если найдена и false иначе
        ●	Удалить из строки AmazingString ведущие пробельные символы, если они есть
        ●	Развернуть строку (первый символ должен стать последним, а последний первым и т.д.)*/

import java.util.Arrays;

public class AmazingString {
    private int length;
    private char characterArray[];

    public AmazingString(char[] inputArray) {
        this.characterArray = inputArray;
        this.length = inputArray.length;
    }

    public AmazingString(String inputString) {
        this.characterArray = inputString.toCharArray();
        this.length = inputString.length();
    }

    public char returnFirstCharacterOfString() {
        return characterArray[0];
    }

    public int returnLengthOfString() {
        return length;
    }

    public void printString() {
        for (int i = 0; i < length; i++) {
            System.out.print(characterArray[i]);
        }
    }

    public void reverseString() {
        char[] reverseArray = new char[length];
        for (int i = 0; i < length; i++) {
            reverseArray[i] = characterArray[length - 1 - i];
            characterArray = reverseArray;
        }
    }

    public boolean isContainsInputString(String inputString) {
        char[] inputArray = inputString.toCharArray();
        int beginIndex = 0;
        boolean answer = true;
        for (int i = 0; i < length; i++) {
            if (characterArray[i] == inputArray[0]) {
                beginIndex = i;
                break;
            }
        }
        for (int i = 0; i < inputArray.length; i++) {
            if (characterArray[beginIndex + i] != inputArray[i]) {
                answer = false;
                break;
            }
        }
        return answer;
    }

    public boolean isContainsInputCharacterArray(char[] inputArray) {
        int beginIndex = 0;
        boolean answer = true;
        for (int i = 0; i < length; i++) {
            if (characterArray[i] == inputArray[0]) {
                beginIndex = i;
                break;
            }
        }
        for (int i = 0; i < inputArray.length; i++) {
            if (characterArray[beginIndex + i] != inputArray[i]) {
                answer = false;
                break;
            }
        }
        return answer;
    }

    public void deleteSpacesFromString() {
        int spaceCounter = 0;
        for (int i = 0; i < length; i++) {
            if (characterArray[i] == ' ') {
                spaceCounter++;
            }
        }
        char[] characterArrayWithoutSpaces = new char[length - spaceCounter];
        for (int i = 0, j = 0; i < length; i++) {
            if (characterArray[i] != ' ') {
                characterArrayWithoutSpaces[j] = characterArray[i];
                j++;
            }
        }
        characterArray = characterArrayWithoutSpaces;
    }
}
