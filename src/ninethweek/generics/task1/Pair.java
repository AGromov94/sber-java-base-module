package ninethweek.generics.task1;

/*Класс, который умеет хранить:
1) два значения любого типа Pair<T, U>
T, E, (K, V)

2) Два значения одинакового типа

3) Класс который умеет хранить 1 параметр только стринг, второй - только число
*/
public class Pair<T extends String, U extends Number> {
    public T first;
    public U second;

    public void print() {
        System.out.println("First: " + first + "; Second: " + second);
    }
}
