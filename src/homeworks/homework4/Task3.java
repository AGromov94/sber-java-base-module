package homeworks.homework4;

import java.util.*;
/*3.(1балл)На вход подается число N — длина массива.Затем передается массив целых чисел(ai)из N элементов,отсортированный по возрастанию.
        После этого вводится число X — элемент,который нужно добавить в массив,чтобы сортировка в массиве сохранилась.

        Необходимо вывести на экран индекс элемента массива,куда нужно добавить X.Если в массиве уже есть число равное X,
        то X нужно поставить после уже существующего.

        Ограничения:
        ●    0<N< 100
        ●    -1000<ai< 1000
        ●    -1000<X< 1000*/

public class Task3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[] arr = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = scan.nextInt();
        }
        int number = scan.nextInt();

        for (int i = 0; i < n; i++) {
            if (arr[i] > number) {
                System.out.println(i);
                break;
            }
        }
    }
}
