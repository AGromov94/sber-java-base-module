package secondweek;

import java.util.*;

/*
Дана последовательность символов,
конкатенировать их в одну строку и вывести эту строку, исключая цифры.
На вход подаются заглавные или строчные символы английского алфавита или цифры.
Scanner input = new Scanner(System.in);
String a1 = input.next();
String a2 = input.next();
String a3 = input.next();
String a4 = input.next();
String a5 = input.next();

Входные данные
H 1 9 i 4
Выходные данные
Hi
 */
public class Task8 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        char a1 = scan.next().charAt(0);
        char a2 = scan.next().charAt(0);
        char a3 = scan.next().charAt(0);
        char a4 = scan.next().charAt(0);
        char a5 = scan.next().charAt(0);

        System.out.print(checkAndFilter(a1));
        System.out.print(checkAndFilter(a2));
        System.out.print(checkAndFilter(a3));
        System.out.print(checkAndFilter(a4));
        System.out.print(checkAndFilter(a5));
    }

    public static String checkAndFilter(char input) {
        String res = "";
        if ((input >= 'a' && input <= 'z') || (input >= 'A' && input <= 'Z')) {
            res += input;
        }
        return res;
    }
}
