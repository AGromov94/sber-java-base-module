package homeworks.homework4;

import java.util.*;
/*5.	(1 балл) На вход подается число N — длина массива. Затем передается массив целых чисел (ai) из N элементов.
        После этого передается число M — величина сдвига.

        Необходимо циклически сдвинуть элементы массива на M элементов вправо.

        Ограничения:
        ●	0 < N < 100
        ●	-1000 < ai < 1000
        ●	0 <= M < 100*/

public class Task5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[] arr = new int[n];
        int[] copyArr = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = scan.nextInt();
        }
        int m = scan.nextInt();

        System.arraycopy(arr, 0, copyArr, m, n - m);
        System.arraycopy(arr, n - m, copyArr, 0, m);

        for (int i : copyArr
        ) {
            System.out.print(i + " ");
        }
    }
}
