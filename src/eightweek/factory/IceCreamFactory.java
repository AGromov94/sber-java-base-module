package eightweek.factory;

/**
 * реализация паттерна "Фабрика"
 */
public class IceCreamFactory {
    public IceCream getIceCream(IceCreamType iceCreamType){

/*        switch (iceCreamType){
            case CHERRY -> {
                return new CherryIceCream();
            }
            case VANILLA -> {
                return new VanillaIceCream();
            }
            case CHOLOCATE -> {
                return new ChocolateIceCream();
            }
        }*/
        return null;
    }
    //вторая реализация фабрики
    public IceCream getIceCream(Class object){
        IceCream iceCream = null;
        if(object.equals(ChocolateIceCream.class)){
            iceCream = new ChocolateIceCream();
        }
        if(object.equals(VanillaIceCream.class)){
            iceCream = new VanillaIceCream();
        }
        if(object.equals(CherryIceCream.class)){
            iceCream = new CherryIceCream();
        }
        return iceCream;
    }
}
