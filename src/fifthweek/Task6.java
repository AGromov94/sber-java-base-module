package fifthweek;

import java.util.*;
/*
На вход подается число N — ширина и высота матрицы.
Необходимо заполнить матрицу 1 и 0 в виде шахматной доски.
Нулевой элемент должен быть 0.

Входные данные
3
Выходные данные
0 1 0
1 0 1
0 1 0

Входные данные
4
Выходные данные
0 1 0 1
1 0 1 0
0 1 0 1
1 0 1 0
*/
public class Task6 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[][] matrix = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if((i + j) %2 != 0){
                    matrix[i][j] = 1;
                }
            }
        }

    }
}
