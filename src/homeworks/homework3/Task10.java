package homeworks.homework3;

import java.util.*;

/*10. Вывести на экран “ёлочку” из символа решетки (#) заданной высоты N. На N + 1
        строке у “ёлочки” должен быть отображен ствол из символа |
        Ограничения:
        2 < n < 10*/
public class Task10 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        for (int i = 0; i < n; i++) {
            for (int j = n - i - 1; j > 0; j--) {
                System.out.print(" ");
            }
            for (int k = 0; k <= i; k++) {
                System.out.print("#");
            }
            for (int z = 0; z <= i - 1; z++) {
                System.out.print("#");
            }
            System.out.println();
        }
        for (int x = 0; x < n-1; x++) {
            System.out.print(" ");
        }
        System.out.print("|");
    }
}
