package homeworks.homework2;

import java.util.*;
/*Дома дочери Пети опять нужна помощь с математикой! В этот раз ей нужно проверить, имеет ли предложенное квадратное уравнение решение или нет.
       На вход подаются три числа — коэффициенты квадратного уравнения a, b, c. Нужно вывести "Решение есть", если оно есть и "Решения нет",
       если нет.

       Ограничения:
       -100 < a, b, c < 100*/

public class Task5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();
        int c = scan.nextInt();

        int d = (int) ((Math.pow(b, 2)) - 4 * a * c);
        if (d < 0) {
            System.out.println("Решения нет");
        } else {
            System.out.println("Решение есть");
        }
    }
}
