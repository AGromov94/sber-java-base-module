package tasksFromSberPlatform.OOP2.AbstractClassesAndInterfaces;

import tasksFromSberPlatform.OOP2.AbstractClassesAndInterfaces.Rational.Rational;

import javax.naming.InsufficientResourcesException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;

public class Test {
    public static void main(String[] args) {
        Number[] numbers = {new Rational(1, 2), new Integer(4), new Double(5.6)};
        Arrays.sort(numbers);
    }

    public static Object max(Object o1, Object o2) {
        if (((Comparable) o1).compareTo(o2) > 0) {
            return o1;
        } else {
            return o2;
        }
    }
}

class C implements Comparable<C> {
    private double weight;

    public C(double weight) {
        this.weight = weight;
    }

    @Override
    public int compareTo(C o) {
        return 0;
    }
}

